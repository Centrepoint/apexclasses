global class sendEmailToUSer implements Schedulable {
    global void execute(SchedulableContext sc) {
        list<User> lstUser = [Select ID from User where username = 'd.speller@centrepoint.org.lightning3' ];
        for(User iterator : lstUser) {
            EmailTemplate objTemp = [SELECT Id FROM EmailTemplate where DeveloperName = 'Change_Request_Monthly_Update' limit 1];
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTemplateId(objTemp.Id);
            mail.setTargetObjectId(iterator.Id);
            mail.setSaveAsActivity(false);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
}