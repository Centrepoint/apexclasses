@IsTest
global class KyperaRetrievePendingController_test {

    global static testMethod void testKyperaRetrievePendingController() {
        
        Test.StartTest();
        
        // set up the test response variable
        String responseStub1 = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body>';
        responseStub1 += '<ChangeStatus>Completed</ChangeStatus></soap:Body></soap:Envelope>';
    
        // Get record type Ids
        List<RecordType> rTypeList = [SELECT Id, Name, SobjectType FROM RecordType WHERE SobjectType IN ('Account', 'Contact')];
        Map<String, Id> accTypeMap = new Map<String, Id>();
        Map<String, Id> conTypeMap = new Map<String, Id>();
        for (RecordType rt : rTypeList) {
            if (rt.SobjectType == 'Contact') {
                conTypeMap.put(rt.Name, rt.Id);
            }
            if (rt.SobjectType == 'Account') {
                accTypeMap.put(rt.Name, rt.Id);
            }
        }

     
        // Create Kypera Metadata in the Custom Setting
        List<Kypera_Metadata__c> kms = new List<Kypera_Metadata__c>();
        
        kms.add(new Kypera_Metadata__c(Name = 'GetCurrentTransactionsBegin', Metadata__c = '<GetCurrentTransactions xmlns="http://mobile.kypera.com/InformWebService/">'));
        kms.add(new Kypera_Metadata__c(Name = 'GetCurrentTransactionsEnd', Metadata__c = '</GetCurrentTransactions>'));
        kms.add(new Kypera_Metadata__c(Name = 'GetCurrentTransactionsEndPoint', Metadata__c = 'http://mobile.kypera.com/InformWebService/GetCurrentTransactions'));
        
        kms.add(new Kypera_Metadata__c(Name = 'createPersonBegin', Metadata__c = '<CreatePerson xmlns="http://mobile.kypera.com/InformWebService/">'));
        kms.add(new Kypera_Metadata__c(Name = 'createPersonEnd', Metadata__c = '</CreatePerson>'));
        kms.add(new Kypera_Metadata__c(Name = 'soapBodyBegin', Metadata__c = '<soap:Body>'));
        kms.add(new Kypera_Metadata__c(Name = 'soapBodyEnd', Metadata__c = '</soap:Body>'));
        kms.add(new Kypera_Metadata__c(Name = 'soapEnvelopeBegin', Metadata__c = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'));
        kms.add(new Kypera_Metadata__c(Name = 'soapEnvelopeEnd', Metadata__c = '</soap:Envelope>'));
        kms.add(new Kypera_Metadata__c(Name = 'xmlTag', Metadata__c = '<?xml version="1.0" encoding="utf-8"?>'));
        kms.add(new Kypera_Metadata__c(Name = 'ServiceHandler',  Metadata__c = 'http://mobile.kypera.com/InformWebService/ServiceHandler.asmx'));
        kms.add(new Kypera_Metadata__c(Name = 'CreatePersonEndPoint',  Metadata__c = 'http://mobile.kypera.com/InformWebService/CreatePerson'));
        kms.add(new Kypera_Metadata__c(Name = 'UpdatePersonEndPoint',  Metadata__c = 'http://mobile.kypera.com/InformWebService/UpdatePerson'));
        kms.add(new Kypera_Metadata__c(Name = 'updatePersonBegin',  Metadata__c = '<UpdatePerson xmlns="http://mobile.kypera.com/InformWebService/">'));
        kms.add(new Kypera_Metadata__c(Name = 'updatePersonEnd',  Metadata__c = '</UpdatePerson>'));
        kms.add(new Kypera_Metadata__c(Name = 'checkPendingChangeBegin', Metadata__c = '<CheckPendingChange xmlns="https://kypera.changing-lives.org.uk/InformWebService/">'));
        kms.add(new Kypera_Metadata__c(Name = 'checkPendingChangeEnd', Metadata__c = '</CheckPendingChange>'));
        kms.add(new Kypera_Metadata__c(Name = 'checkPendingChangeEndPoint', Metadata__c = 'https://kypera.changing-lives.org.uk/InformWebService/CheckPendingChange'));
        
        insert kms;

        
        // put these in a map for easy retrieval later on
        Map<String, String> kyperaMetaData = new Map<String, String>();
        for (Kypera_Metadata__c kmsMapItem: kms) {
            kyperaMetaData.put(kmsMapItem.Name, kmsMapItem.Metadata__c);
        }
        
    
        // Create an Account
        Account acc = new Account(Name = 'Test Account 1', RecordTypeId = accTypeMap.get('Project'));
        insert acc;        

        // Create a Client with the correct Record Type
        Contact con = new Contact(FirstName = 'Test', LastName = 'Contact 1', infrm__Gender__c = 'Male', Salutation = 'Mr', RecordTypeId = conTypeMap.get('Client'), infrm__Disabled_person__c = 'Yes', BirthDate = Date.newInstance(1980, 10, 10), Kypera_Reference__c = '99999');
        con.put('infrm__NI_number_known__c', 'No');
        con.put('infrm__Client_Group_P__c', 'Rough sleeper');
        con.put('infrm__Sexuality__c', 'Does not wish to disclose');
        con.put('infrm__ex_Armed_forces_personnel_SP__c', 'No');
        con.put('infrm__Source_of_income__c', 'Don\'t know');
        con.put('infrm__Qualify_for_HB__c', 'don\'t know');
        con.put('infrm__Immigration_Status__c', 'Not known');
        insert con;
        
        Kypera_Tenancy__c kt = new Kypera_Tenancy__c(Client__c = con.Id, Kypera_Start_Date__c = '2020-01-01', Unit_Reference__c = 'TEST001');
        insert kt;
        
        
        // set up the page and its controller
    KyperaRetrievePendingController.responseStub = responseStub1;
        PageReference pageRef = Page.KyperaTransactionStatus;
        Test.setCurrentPage(pageRef);        
        ApexPages.currentPage().getParameters().put('id', kt.id);      
        System.debug('con.id: ' + con.id);
        ApexPages.StandardController stdController = new ApexPages.StandardController(kt);
        stdController.getRecord();
        KyperaRetrievePendingController controller = new KyperaRetrievePendingController(stdController);
        controller.retrieveTransactionData();   
        system.assertEquals(responseStub1, KyperaRetrievePendingController.responseStub);      

        Test.StopTest();
    
    
    }
}