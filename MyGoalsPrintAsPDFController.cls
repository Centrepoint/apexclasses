public class MyGoalsPrintAsPDFController {
    // Declare variables used within the class to hold key bits of information
    private String SMSigUrl; // Staff Member Signature
    private String YPSigUrl; // Young Person Signature
    private String Title;    // Page Title (Comes from Record Type label)

    // Set the class as a StandardController (this enables it to be used on Visualforce pages)
    public MyGoalsPrintAsPDFController(ApexPages.StandardController controller) {
		My_Goal__c mg = (My_Goal__c)controller.getRecord();
        
        /* Please note ----------------------------------------------------------------------------------------
        / Due to the fields required, this Controller will only work with the My Goals record
        / type. This has not been hardcoded to give flexibility within the system
		------------------------------------------------------------------------------------------------------*/     
        
        // Set the variable title to the hardcoded variable 'My Goals'
        Title = 'My Goals';
       
        // Query E-Signature records which are related to the record id of this My Goals record, via the Lookup field 
        // My_Goal_Record__c
        List<Id> esigids = new List<Id>();
        for(E_Signature__C e : [SELECT Id, My_Goal_Record__c  FROM E_Signature__C WHERE My_Goal_Record__c  = :mg.Id]){
                esigids.add(e.Id);
        }
        
        // Check to make sure the E-Signature records exist
        List<Id> cdids = new List<Id>();
        if(esigids.isEmpty()){
            return;
        }
        
        // Query related ContentDocuments
        for(ContentDocumentLink cdl : [SELECT ContentDocumentId, LinkedEntityId  FROM ContentDocumentLink where LinkedEntityId IN :esigids]){
            if (cdl.ContentDocumentId != null) {
                cdids.add(cdl.ContentDocumentId);
            }
        }
        
        // Query related ContentVersions of the files linked to the E-Signature records
        for(ContentVersion cv : [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN :cdids]){
            // The staff signature image file name is always saved as CPStaffSignature.png during the appropriate Flow using the BrightGen
            // lightning signature component
            if(cv.Title == 'CPStaffSignature.png'){
                // This is the salesforce factory url to access files
                SMSigUrl = '/sfc/servlet.shepherd/version/download/' + cv.Id;
            } 
            // The yp signature image file name is always saved as YPSignature.png during the appropriate Flow using the BrightGen
            // lightning signature component
            if (cv.Title == 'YPSignature.png'){
                // This is the salesforce factory url to access files
                YPSigUrl = '/sfc/servlet.shepherd/version/download/' + cv.Id;
            }
        }
    }
    // Enable the variables created within the Class to be accessed outside of the Class
    public String getTitle(){
        return Title;
    }
    public String getSMSigUrl(){
        return SMSigUrl;
    }
    public String getYPSigUrl(){
        return YPSigUrl;
    }
}