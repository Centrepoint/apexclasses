// Declare that this is a test class
@isTest
// Declare the test class name. This is for the RecentSystemChangesController
public class RecentSystemChangesControllerTest {

    @testSetup static void setupTestData(){
        // Fetch the recordTypeID 'Suggest a System Improvement' from the Request Support Custom Object
    	Id SuggestASystemImprovementRecordTypeId = Schema.SObjectType.Request_Support__c.getRecordTypeInfosByName().get('Suggest a System Improvement').getRecordTypeId();

        // Setup a list to hold the test System Improvement records
        List<Request_Support__c> testSystemImprovements = new List<Request_Support__c>();
        // Create a test contact
        
        // Create 2 System Improvement records
        Request_Support__c testSystemImprovement1 = new Request_Support__c(	What_System_does_this_relate_to__c = 'In-Form',
                                                             				Support_Requestor_Name__c = '0051p000009QPAKAA4',
                                                                            Support_Requester_Team__c = 'Policy',
                                                                            Change_Req_Details__c = 'Test Change Request',
                                                                            Change_Req_Justification__c = 'Test Justification',
                                                                            Change_Req_Actual_Completion_Date__c = Date.Today().addDays(-20),
                                                                            Change_Req_Level_of_Change__c = 'Major',
                                                                            RecordTypeId = SuggestASystemImprovementRecordTypeId);
        
        testSystemImprovements.add(testSystemImprovement1);
        
        insert testSystemImprovements;
    }
    
    private static testMethod void testGetSystemImprovements(){
        Test.startTest();
        Request_Support__c test = [SELECT Id fROM Request_Support__c LIMIT 1];
        // Set the test to run on the c Visualforce page as this is
        // the one which RecentSystemChangesController (the class tested by this script)
        // runs on  
        PageReference pageRef = Page.RecentSystemChanges;

        // Assign the RecentSystemChangesController as the controller
        ApexPages.StandardController sc = new ApexPages.StandardController(test);
        RecentSystemChangesController controller = new RecentSystemChangesController(sc);
        controller.getSystemImprovements();
    }
}