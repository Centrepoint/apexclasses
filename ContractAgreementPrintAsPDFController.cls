public class ContractAgreementPrintAsPDFController {
    private String SMSigUrl; // Staff Member Signature
    private String YPSigUrl; // Young Person Signature
    private String Title;    // Page Title (Comes from Record Type label)

    public ContractAgreementPrintAsPDFController(ApexPages.StandardController controller) {
        Contract_Agreement__c ca = (Contract_Agreement__c)controller.getRecord();
        Title = Schema.getGlobalDescribe().get('Contract_Agreement__c').getDescribe().getRecordTypeInfosById().get(ca.RecordTypeId).getName();
        // Query related E-Signature records
        List<Id> esigids = new List<Id>();
        for(E_Signature__C e : [SELECT Id, Contract_Agreement__c FROM E_Signature__C WHERE Contract_Agreement__c = :ca.Id]){
                esigids.add(e.Id);
        }
        List<Id> cdids = new List<Id>();
        if(esigids.isEmpty()){
            return;
        }
        // Query related ContentDocuments
        for(ContentDocumentLink cdl : [SELECT ContentDocumentId, LinkedEntityId  FROM ContentDocumentLink where LinkedEntityId IN :esigids]){
            if (cdl.ContentDocumentId != null) {
                cdids.add(cdl.ContentDocumentId);
            }
        }
        // Query related ContentVersions
        for(ContentVersion cv : [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN :cdids]){
            if(cv.Title == 'CPStaffSignature.png'){
                SMSigUrl = '/sfc/servlet.shepherd/version/download/' + cv.Id;
            } 
            if (cv.Title == 'YPSignature.png'){
                YPSigUrl = '/sfc/servlet.shepherd/version/download/' + cv.Id;
            }
        }
    }
    public String getTitle(){
        return Title;
    }
    public String getSMSigUrl(){
        return SMSigUrl;
    }
    public String getYPSigUrl(){
        return YPSigUrl;
    }
}