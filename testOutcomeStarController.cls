/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class testOutcomeStarController {

    static testMethod void myUnitTest() {
      PageReference pageRef = Page.outcomeStar;
        Test.setCurrentPage(pageRef);
        
      Contact client = new Contact(FirstName = 'Arthur', LastName = 'TestAskey');
      insert client;
      client = [select id from Contact where LastName = 'TestAskey'];
      
      Account project = new Account(name = 'TestStarTest');
      insert project;
      project = [select id from Account where name = 'TestStarTest'];
      
      Outcome_Star__c outcomeStar = new Outcome_Star__c(Client__c = client.Id, Project_Lookup__c = project.Id);
      insert outcomeStar;
  
      ApexPages.currentPage().getParameters().put('id', outcomeStar.id);      
      ApexPages.StandardController stdController = new ApexPages.StandardController(outcomeStar);
      stdController.getRecord();
      ApexPages.currentPage().getParameters().put('id', outcomeStar.id);
      outcomeStarController controller = new outcomeStarController(stdController);
      
      controller.getFilterDates();
      controller.getoutcomeStar();
      controller.getPlanData();
      controller.getChartTypes();
      controller.action();
      String dd = controller.dateOfPlan;
      String cc = controller.chartType;
      controller.GetAddedPlans();
      ApexPages.currentPage().getParameters().put('addId', outcomeStar.id);
      controller.AddPlan();
      controller.getPlanData();
      controller.GetStarDates();
      controller.GetValidDate(System.now());
      //controller.toInteger(outcomeStar.MOTIVATION__c);
      controller.formatDate(System.now());
      
      Map<Id, Outcome_Star__c> filteredPlans = new Map<Id, Outcome_Star__c>();
      filteredPlans.put(outcomeStar.Id, outcomeStar);
      controller.DefaultStarData(filteredPlans);
      
      ApexPages.currentPage().getParameters().put('removeId', outcomeStar.id);
      controller.RemovePlan();
      controller.GetPlanSize();
    }
}