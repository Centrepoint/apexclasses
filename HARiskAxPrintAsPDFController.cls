public with sharing class HARiskAxPrintAsPDFController {
	private List<Individual_Risk__c> irsFromOthers {get; set;} 
    private List<Individual_Risk__c> irsToOthers {get; set;} 
    private List<Individual_Risk__c> irsToSelf {get; set;} 
    private Housing_Assessment__c HA; 
    
    // Set the class as a StandardController (this enables it to be used on Visualforce pages)
    public HARiskAxPrintAsPDFController(ApexPages.StandardController controller) {
        this.HA = (Housing_Assessment__c)controller.getRecord();
        this.HA = [SELECT Id, Young_Persons_Name__c, Date_Submitted_for_Approval__c, CreatedById FROM Housing_Assessment__c WHERE Id = :HA.Id];
    }
 
    public List<Individual_Risk__c> getIrsFromOthers() {
        irsFromOthers = new List<Individual_Risk__c>();
        Date submittedDate; 
        if(HA.Date_Submitted_for_Approval__c == null){ // If Date_Submitted_for_Approval__c is blank, return current date, else return Date_Submitted_for_Approval__c
            submittedDate = Date.today();
        } else {
            submittedDate = HA.Date_Submitted_for_Approval__c;
        }
        List<Individual_Risk__c> irsFromOthersTemp = [SELECT id, Name,Date_Risk_Became_No_longer_current__c, Action_Plan_Area__c, Identified_Risk_And_Evidence__c, Protective_Factors__c, Level_of_Risk__c, Date_Risk_Assessed__c, Current_Risk__c FROM Individual_Risk__c WHERE 
                            CreatedById = :HA.CreatedById 
                            AND Client__c = :HA.Young_Persons_Name__c 
                            AND Type_Of_Risk__c = 'Harm from Others'];
        // Now further filter the results by date and status
        for(Individual_Risk__c ir : irsFromOthersTemp){
            if(ir.Current_Risk__c == 'Yes' && ir.Date_Risk_Assessed__c <= submittedDate){
                irsFromOthers.add(ir);
            }
            if(ir.Current_Risk__c == 'No' && ir.Date_Risk_Became_No_Longer_Current__c > submittedDate){
                irsFromOthers.add(ir);
            }
        }
        return irsFromOthers;
    }

    public List<Individual_Risk__c> getIrsToOthers() {
        irsToOthers = new List<Individual_Risk__c>();
        Date submittedDate; 
        if(HA.Date_Submitted_for_Approval__c == null){ // If Date_Submitted_for_Approval__c is blank, return current date, else return Date_Submitted_for_Approval__c
            submittedDate = Date.today();
        } else {
            submittedDate = HA.Date_Submitted_for_Approval__c;
        }
        List<Individual_Risk__c> irsToOthersTemp = [SELECT id, Name,Date_Risk_Became_No_longer_current__c, Action_Plan_Area__c, Identified_Risk_And_Evidence__c, Protective_Factors__c, Level_of_Risk__c, Date_Risk_Assessed__c, Current_Risk__c FROM Individual_Risk__c WHERE 
                            CreatedById = :HA.CreatedById 
                            AND Client__c = :HA.Young_Persons_Name__c 
                            AND Type_Of_Risk__c = 'Harm to Others'];
        // Now further filter the results by date and status
        for(Individual_Risk__c ir : irsToOthersTemp){
            if(ir.Current_Risk__c == 'Yes' && ir.Date_Risk_Assessed__c <= submittedDate){
                irsToOthers.add(ir);
            }
            if(ir.Current_Risk__c == 'No' && ir.Date_Risk_Became_No_Longer_Current__c > submittedDate){
                irsToOthers.add(ir);
            }
        }
        return irsToOthers;
    }

    public List<Individual_Risk__c> getIrsToSelf() {
        irsToSelf = new List<Individual_Risk__c>();
        Date submittedDate; 
        if(HA.Date_Submitted_for_Approval__c == null){ // If Date_Submitted_for_Approval__c is blank, return current date, else return Date_Submitted_for_Approval__c
            submittedDate = Date.today();
        } else {
            submittedDate = HA.Date_Submitted_for_Approval__c;
        }
        List<Individual_Risk__c> irsToSelfTemp = [SELECT id, Name,Date_Risk_Became_No_longer_current__c, Action_Plan_Area__c, Identified_Risk_And_Evidence__c, Protective_Factors__c, Level_of_Risk__c, Date_Risk_Assessed__c, Current_Risk__c FROM Individual_Risk__c WHERE 
                            CreatedById = :HA.CreatedById 
                            AND Client__c = :HA.Young_Persons_Name__c 
                            AND Type_Of_Risk__c = 'Harm to Self'];
        // Now further filter the results by date and status
        for(Individual_Risk__c ir : irsToSelfTemp){
            if(ir.Current_Risk__c == 'Yes' && ir.Date_Risk_Assessed__c <= submittedDate){
                irsToSelf.add(ir);
            }
            if(ir.Current_Risk__c == 'No' && ir.Date_Risk_Became_No_Longer_Current__c > submittedDate){
                irsToSelf.add(ir);
            }
        }
        return irsToSelf;
    }
}