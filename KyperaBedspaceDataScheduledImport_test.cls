@IsTest
global without sharing class KyperaBedspaceDataScheduledImport_test {

    static testmethod void m1() {
        Test.StartTest();

        // Create Kypera Metadata in the Custom Setting
        List<Kypera_Metadata__c> kms = new List<Kypera_Metadata__c>();
        
        kms.add(new Kypera_Metadata__c(Name = 'GetCurrentTransactionsBegin', Metadata__c = '<GetCurrentTransactions xmlns="http://mobile.kypera.com/InformWebService/">'));
        kms.add(new Kypera_Metadata__c(Name = 'GetCurrentTransactionsEnd', Metadata__c = '</GetCurrentTransactions>'));
        kms.add(new Kypera_Metadata__c(Name = 'GetCurrentTransactionsEndPoint', Metadata__c = 'http://mobile.kypera.com/InformWebService/GetCurrentTransactions'));
        
        kms.add(new Kypera_Metadata__c(Name = 'createPersonBegin', Metadata__c = '<CreatePerson xmlns="http://mobile.kypera.com/InformWebService/">'));
        kms.add(new Kypera_Metadata__c(Name = 'createPersonEnd', Metadata__c = '</CreatePerson>'));
        kms.add(new Kypera_Metadata__c(Name = 'soapBodyBegin', Metadata__c = '<soap:Body>'));
        kms.add(new Kypera_Metadata__c(Name = 'soapBodyEnd', Metadata__c = '</soap:Body>'));
        kms.add(new Kypera_Metadata__c(Name = 'soapEnvelopeBegin', Metadata__c = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'));
        kms.add(new Kypera_Metadata__c(Name = 'soapEnvelopeEnd', Metadata__c = '</soap:Envelope>'));
        kms.add(new Kypera_Metadata__c(Name = 'xmlTag', Metadata__c = '<?xml version="1.0" encoding="utf-8"?>'));
        kms.add(new Kypera_Metadata__c(Name = 'ServiceHandler',  Metadata__c = 'http://mobile.kypera.com/InformWebService/ServiceHandler.asmx'));
        kms.add(new Kypera_Metadata__c(Name = 'CreatePersonEndPoint',  Metadata__c = 'http://mobile.kypera.com/InformWebService/CreatePerson'));
        kms.add(new Kypera_Metadata__c(Name = 'UpdatePersonEndPoint',  Metadata__c = 'http://mobile.kypera.com/InformWebService/UpdatePerson'));
        kms.add(new Kypera_Metadata__c(Name = 'updatePersonBegin',  Metadata__c = '<UpdatePerson xmlns="http://mobile.kypera.com/InformWebService/">'));
        kms.add(new Kypera_Metadata__c(Name = 'updatePersonEnd',  Metadata__c = '</UpdatePerson>'));
        kms.add(new Kypera_Metadata__c(Name = 'GetPropertyStructureXML', Metadata__c = 'http://mobile.kypera.com/InformWebService/GetPropertyStructureXml'));
        kms.add(new Kypera_Metadata__c(Name = 'BatchRecordNumber', Metadata__c = '1'));
        kms.add(new Kypera_Metadata__c(Name = 'NumberOfRecordsToProcess', Metadata__c = '10'));
        kms.add(new Kypera_Metadata__c(Name = 'Frequency', Metadata__c = '10'));
        kms.add(new Kypera_Metadata__c(Name = 'GetSchemeBedSpaceDetailsBegin', Metadata__c = '<GetSchemeBedSpaceDetails xmlns="http://inform.kypera.com/InformWebService/">'));
        kms.add(new Kypera_Metadata__c(Name = 'GetSchemeBedSpaceDetailsEnd', Metadata__c = '</GetSchemeBedSpaceDetails>'));
        kms.add(new Kypera_Metadata__c(Name = 'GetSchemeBedSpaceDetailsEndPoint', Metadata__c = 'http://myrentandrepairs.centrepoint.org.uk:8080/InformWebServiceTest/GetSchemeBedSpaceDetails'));
        insert kms;

        // put these in a map for easy retrieval later on
        Map<String, String> kyperaMetaData = new Map<String, String>();
        for (Kypera_Metadata__c kmsMapItem: kms) {
            kyperaMetaData.put(kmsMapItem.Name, kmsMapItem.Metadata__c);
        }
        
           // create test Kypera Properties
        List<Kypera_Properties__c> KyperaProperties = new List<Kypera_Properties__c>();
        for(Integer i = 0; i < 2; i++) {
            KyperaProperties.add(new Kypera_Properties__c
                                    (
                                    Scheme_Name__c = 'Property ' + i, 
                                    Scheme_Reference__c = String.valueOf(i),
                                    Unit_Name__c = 'Unit ' + i,
                                    Unit_Reference__c = '10',
                                    H_Core_Rent__c = 100,
                                    P_Water__c = 10,
                                    P_Food__c = 10,
                                    P_Gas__c = 10,
                                    P_Electricity__c = 10,
                                    P_TV_Licence__c = 10,
                                    P_Other_including_toiletries__c = 10,
                                    H_Council_tax__c = 7.5,
                                    H_Cleaning_of_communal_areas_and__c = 7.5,
                                    H_Service_contracts__c = 7.5,
                                    H_Heat_light__c = 7.5,
                                    H_Renewals_and_replacements__c = 7.5,
                                    H_Laundry_facilities__c = 7.5,
                                    Type_of_tenancy__c = 'T Type ' + i,
                                    Bedspace_Type__c = 'B Type ' + i                                   
                                    )
                                );
        }
        
        insert KyperaProperties;
                      
                
        // Add the Scheme Codes
        List<Kypera_Scheme_Codes__c> ksc = new List<Kypera_Scheme_Codes__c>();
        ksc.add(new Kypera_Scheme_Codes__c(Name = 'A01', BatchRecordNumber__c = 1));
        ksc.add(new Kypera_Scheme_Codes__c(Name = 'A02', BatchRecordNumber__c = 2));
        ksc.add(new Kypera_Scheme_Codes__c(Name = 'A03', BatchRecordNumber__c = 3));
        ksc.add(new Kypera_Scheme_Codes__c(Name = 'A05', BatchRecordNumber__c = 4));
        ksc.add(new Kypera_Scheme_Codes__c(Name = 'A06', BatchRecordNumber__c = 5));
        ksc.add(new Kypera_Scheme_Codes__c(Name = 'A07', BatchRecordNumber__c = 6));
        ksc.add(new Kypera_Scheme_Codes__c(Name = 'A08', BatchRecordNumber__c = 7));
        ksc.add(new Kypera_Scheme_Codes__c(Name = 'A09', BatchRecordNumber__c = 8));
        ksc.add(new Kypera_Scheme_Codes__c(Name = 'A10', BatchRecordNumber__c = 9));
        ksc.add(new Kypera_Scheme_Codes__c(Name = 'A11', BatchRecordNumber__c = 10));
        ksc.add(new Kypera_Scheme_Codes__c(Name = 'ZZI', BatchRecordNumber__c = 11));
        insert ksc;
        
        String CRON_EXP = '0 0 3 * * ?'; 

        KyperaBedspaceDataScheduledImport sync = new KyperaBedspaceDataScheduledImport();

        // Schedule the test job
        String jobId = System.schedule('testScheduledApex-KyperaBedspaceDataScheduledImport', CRON_EXP,sync);

        // Get the information from the CronTrigger API object
                CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
           Kypera_Bedspace_Import_Log__c log  = new Kypera_Bedspace_Import_Log__c ();     
         KyperaBedspaceDataProcessorParser kyperabed = new KyperaBedspaceDataProcessorParser (log);
        
        kyperabed.removeNoIds();
        
        KyperaBedspaceDataProcessor KyperParser = new KyperaBedspaceDataProcessor();
      
        

        Test.StopTest();
    }
    
       
}