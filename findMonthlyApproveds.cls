public class findMonthlyApproveds {
    private final List<Change_Suggestion__c> requests;

    public findMonthlyApproveds() {
        requests = [select Name,Subject__c, Details__c, CreatedById, Planned_Completion_Date__c 
        from Change_Suggestion__c 
        where Proceed_with_Change_Request__c = 'Yes'
        and CreatedDate = LAST_MONTH
        ];
    }

    public List<Change_Suggestion__c> getMonthlyApproveds() {

        return requests;
    }
}