public class basicCurl {

    public string urlToSend {get; set; }
    public string responseBody { get; set; }
    public integer responseStatusCode { get; set; }
    public string responseStatus { get; set; }

    public void wGet() {
        HttpRequest req = new HttpRequest();
        HTTPResponse res = new HttpResponse();
        //Set HTTPRequest Method
        req.setMethod('GET');
        req.setEndPoint(urlToSend); //'https://kypera.changing-lives.org.uk/InformWebService/ServiceHandler.asmx');
        Http http = new Http();
        try {
        if (!Test.IsRunningTest()) {
            res = http.send(req);      
            responseBody = res.getBody();
        } else {
            responseBody = '<html>test</html>';
            responseStatusCode = 200;
            responseStatus = 'OK';
        }
            System.debug('*** res.body: '+ res.getBody());
            System.debug('*** res.getStatus: '+ res.getStatus());
            System.debug('*** res.getStatusCode:'+ res.getStatusCode());   
        } catch(Exception e) {
            responsebody = e.getMessage();
        }    
    }

}