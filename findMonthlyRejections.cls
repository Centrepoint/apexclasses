Public class findMonthlyRejections {
    private final List<Change_Suggestion__c> requests;

    public findMonthlyRejections() {
        requests = [select Name,Subject__c, Details__c, CreatedById, Reason_for_rejecting_the_change__c 
        from Change_Suggestion__c 
        where Proceed_with_Change_Request__c = 'No'
        and CreatedDate = LAST_N_DAYS:30
        ];
    }

    public List<Change_Suggestion__c> getMonthlyRejections() {

        return requests;
    }
}