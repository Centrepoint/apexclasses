global with sharing class KyperaTransactionDisplayController {

    private final Contact con { get; set; }
    private Client_Information_Sent_to_Kypera__c log;
    private KyperaProcessor kp;
    global Map<String, String> kpContactData;
    global string request { get; set; } 
    global string response { get; set; }
    global string response2 { get; set; }
    global Map<String,String> kpFields { get; set; }
    global Dom.Document domDoc { get; set; }
    global Dom.XMLNode domNode { get; set; }
    //global String responseStub { get; set; }
    global Boolean hasNoErrors { get; set; }
    global Map<String, String> contactFromKyperaFields {get; set; }
    // the list of fields and their settings
    global static List<Kypera_Contact_Fields__c> fields = new List<Kypera_Contact_Fields__c>();
    global static Map<String, Boolean> fieldsThatCanSyncBack { get; set; }
    global static Map<String, Kypera_Metadata__c> metadata = new Map<String, Kypera_Metadata__c>();
    // the translations of In-Form picklists to Kypera picklists
    global static List<Kypera_Contact_Field_Mappings__c> mappings = new List<Kypera_Contact_Field_Mappings__c>();
    // variable for responses from callouts during tests
    global static string responseStub;           
    global String kyperaFieldName { get; set; }
    global string inFormFieldName { get; set; }
    
    // The constructor initialises the contact variable by using the getRecord method from the standard controller.
    global KyperaTransactionDisplayController(ApexPages.StandardController stdController) {
        this.con = (Contact)stdController.getRecord(); 
        kp = new KyperaProcessor();
        contactFromKyperaFields = new Map<String, String>();      
        // initialise the list of fields and their settings
        fields = [SELECT 
                Id, Name, Element_Name__c, Order__c, Do_Not_Sync__c, Operation__c,
                Override__c, Has_Mapping__c, Kypera_Data_Type__c, Can_Sync_Back__c
             FROM
                Kypera_Contact_Fields__c
             ORDER BY 
                Order__c];  
        fieldsThatCanSyncBack = new Map<String, Boolean>();
        for (Kypera_Contact_Fields__c kcf : fields) {
            fieldsThatCanSyncBack.put(kcf.Name, kcf.Can_Sync_Back__c);
        }            
        // call the method that gets contact data from Kypera 
        retrieveTransactionData();
        processTransactionData();
    }
    
    global void retrieveTransactionData() {
        request = '';
        response = '';
        // build the request to Kypera
        request += kp.xmlStart(KyperaProcessor.metadata);
        request += KyperaProcessor.metadata.get('GetCurrentTransactionsBegin').MetaData__c;
        request += '<ExternalApplicationReference></ExternalApplicationReference>';
        request += '<KyperaReference>'+con.Kypera_Reference__c+'</KyperaReference>';
        request += '<StartDate>2000-Jan-01</StartDate>';
        request += '<EndDate>2025-Dec-31</EndDate>';   
        request += KyperaProcessor.metadata.get('GetCurrentTransactionsEnd').MetaData__c;
        request += kp.xmlEnd(KyperaProcessor.metadata);  
        // populate the request variable with a call to Kypera  
        response = callWebServiceEndPoint(request, 'GetCurrentTransactionsEndPoint');    
        //response = response.replace('\n','').replace('\r','');
        // populate the contactFields map
        return;    
    }

    global void processTransactionData() {
        domDoc = new Dom.Document();
       
            domDoc.load(response);
        
        DOM.XMLNode root = domDoc.getRootElement();
        response2 = walkThrough(root);
        //response = response.replace('<?xml version="1.0" encoding="utf-8"?>', '');
        //response += '<br /><h1>----' + getXMLText('<xml><data><transactions><t1>Test Transaction1</t1><t2>Test Transaction 2</t2></transactions></data></xml>', 'transactions', 0) + '</h1>';
        return;
    }

    private String walkThrough(DOM.XMLNode node) {
      String result = '\n';
      if (node.getNodeType() == DOM.XMLNodeType.COMMENT) {
        return 'Comment (' +  node.getText() + ')';
      }
      if (node.getNodeType() == DOM.XMLNodeType.TEXT) {
        return 'Text (' + node.getText() + ')';
      }
      if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
        result += 'Element: ' + node.getName();
        if (node.getText().trim() != '') {
          result += ', text=' + node.getText().trim();
        }
        if (node.getAttributeCount() > 0) { 
          for (Integer i = 0; i< node.getAttributeCount(); i++ ) {
            result += ', attribute #' + i + ':' + node.getAttributeKeyAt(i) + '=' + node.getAttributeValue(node.getAttributeKeyAt(i), node.getAttributeKeyNsAt(i));
          }  
        }
        for (Dom.XMLNode child: node.getChildElements()) {
          result += walkThrough(child);
        }
        return result;
        system.debug('result = \r' + result);
      }
      return '';  //should never reach here 
    }
    
    // calls the Kypera webservice's endpoint and returns the response as a string. The serviceEndPointName must be a valid Kypera metadata custom setting
    global string callWebServiceEndPoint(String xmlToSend, String serviceEndPointName)
    {
        String xmlRequest = xmlToSend;
        HttpRequest req = new HttpRequest();
        HTTPResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setEndpoint(KyperaProcessor.metadata.get('ServiceHandler').MetaData__c); 
        system.debug('*** KyperaProcessor.metadata.get(\'ServiceHandler\').MetaData__c = ' + KyperaProcessor.metadata.get('ServiceHandler').MetaData__c);
        req.setHeader('Content-Type', 'text/xml; charset=utf-8');
        req.setHeader('SOAPAction', KyperaProcessor.metadata.get(serviceEndPointName).MetaData__c); 
        system.debug('*** KyperaProcessor.metadata.get(serviceEndPointName).MetaData__c = ' + KyperaProcessor.metadata.get(serviceEndPointName).MetaData__c);
        system.debug('*** serviceEndPointName = ' + serviceEndPointName);
        req.setBody(xmlRequest);
        system.debug('*** req.getBody: '+ req.getBody());
        Http http = new Http();
        try { 
            if (!Test.isRunningTest()){ 
                res = http.send(req);        
                System.debug('*** res.body: '+ res.getBody());
                System.debug('*** res.getStatus: '+ res.getStatus());
                System.debug('*** res.getStatusCode:'+ res.getStatusCode());   
            } else {
                return responseStub;
            }
            return res.getBody();
        } catch(System.CalloutException e) {
            response = e.GetMessage();
            return '';
        }     
    }  
    
    // gets the text from a given XML tag. It is needed because the response from create person is badly formed and returns encoded versions of < and > 
    global string getXMLText(string xmlString, string tagName, integer position) {
        string retVal = '';
        if(xmlString.contains('<' + tagName + '>') && xmlString.contains('</' + tagName + '>')){
          try{
              retVal = xmlString.substring(xmlString.indexOf('<' + tagName + '>', position) + tagName.length() + 2,
                xmlString.indexOf('</' + tagName + '>', position));   
            }catch (exception e){
                system.debug('*** Exception in method getXMLText ' + e.getMessage() + ' tagName = ' + tagName + ' xmlString = ' + xmlString);
            } 
        }
        return retVal;
    }    
    

    
    // reformat dates
    global Date reformatDate(String dateValue) {
        // handles the blank formats that Kypera sends back
        if (dateValue == '00:00:00') {
            return null;
        }
        Date retVal = Date.parse(dateValue.substring(0,2)+'/'+dateValue.substring(3,5)+'/'+dateValue.substring(6,10));
        return retVal;    
    }

}