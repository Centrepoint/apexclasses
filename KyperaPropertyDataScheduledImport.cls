global class KyperaPropertyDataScheduledImport implements Schedulable {

   global void execute(SchedulableContext SC) {
        KyperaPropertyBatchProcessor batchApex = new KyperaPropertyBatchProcessor();
        ID batchprocessid = Database.executeBatch(batchApex);
   }
   
}