global with sharing class KyperaContactDisplayController {

    private final Contact con { get; set; }
    private Client_Information_Sent_to_Kypera__c log;
    private KyperaProcessor kp;
    global Map<String, String> kpContactData;
    global string request { get; set; } 
    global string response { get; set; }
    global Map<String,String> kpFields { get; set; }
    global Dom.Document domDoc;
    //global String responseStub { get; set; }
    global Boolean hasNoErrors { get; set; }
    global Map<String, String> contactFromKyperaFields {get; set; }
    // the list of fields and their settings
    global static List<Kypera_Contact_Fields__c> fields = new List<Kypera_Contact_Fields__c>();
    global static Map<String, Boolean> fieldsThatCanSyncBack { get; set; }
    global static Map<String, Kypera_Metadata__c> metadata = new Map<String, Kypera_Metadata__c>();
    // the translations of In-Form picklists to Kypera picklists
    global static List<Kypera_Contact_Field_Mappings__c> mappings = new List<Kypera_Contact_Field_Mappings__c>();
    // the translations of Kypera 'lookups' to In-Form picklists
    global static List<Kypera_Contact_Field_Reverse_Mappings__c> reverseMappings = new List<Kypera_Contact_Field_Reverse_Mappings__c>();    
    // schema variables to find the data type for fields
    global static Schema.SObjectType soType;
    global static Schema.DescribeSObjectResult soRes;
    global static Schema.DescribeFieldResult fdRes;
    // variable for responses from callouts during tests
    global static string responseStub;           
    global String kyperaFieldName { get; set; }
    global string inFormFieldName { get; set; }
    
    // The constructor initialises the contact variable by using the getRecord method from the standard controller.
    global KyperaContactDisplayController(ApexPages.StandardController stdController) {
        this.con = (Contact)stdController.getRecord(); 
        kp = new KyperaProcessor();
        contactFromKyperaFields = new Map<String, String>();      
        // initialise the list of fields and their settings
        fields = [SELECT 
                Id, Name, Element_Name__c, Order__c, Do_Not_Sync__c, Operation__c,
                Override__c, Has_Mapping__c, Kypera_Data_Type__c, Can_Sync_Back__c
             FROM
                Kypera_Contact_Fields__c
             ORDER BY 
                Order__c];  
        fieldsThatCanSyncBack = new Map<String, Boolean>();
        for (Kypera_Contact_Fields__c kcf : fields) {
            fieldsThatCanSyncBack.put(kcf.Name, kcf.Can_Sync_Back__c);
        }            
        // call the method that gets contact data from Kypera
        retrieveContactData();    
    }

    // method that gets contact data from Kypera
    global void retrieveContactData() {
        request = '';
        response = '';
        // build the request to Kypera
        // TODO: re-write to not use the kp variable
        request += kp.xmlStart(KyperaProcessor.metadata);
        request += KyperaProcessor.metadata.get('GetPeopleBegin').MetaData__c;
        request += '<KyperaReferenceStart>'+con.Kypera_Reference__c+'</KyperaReferenceStart>';
        request += '<KyperaReferenceEnd>'+con.Kypera_Reference__c+'</KyperaReferenceEnd>';
        request += '<NoExternalApplicationReference>false</NoExternalApplicationReference>';
        request += KyperaProcessor.metadata.get('GetPeopleEnd').MetaData__c;
        request += kp.xmlEnd(KyperaProcessor.metadata);  
        // populate the request variable with a call to Kypera  
        response = callWebServiceEndPoint(request, 'GetPeopleEndPoint');    
        // populate the contactFields map
        populateContactFieldMap(response);
        return;
    }
    
    // calls the Kypera webservice's endpoint and returns the response as a string. The serviceEndPointName must be a valid Kypera metadata custom setting
    global string callWebServiceEndPoint(String xmlToSend, String serviceEndPointName)
    {
        String xmlRequest = xmlToSend;
        HttpRequest req = new HttpRequest();
        HTTPResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setEndpoint(KyperaProcessor.metadata.get('ServiceHandler').MetaData__c); 
        system.debug('*** KyperaProcessor.metadata.get(\'ServiceHandler\').MetaData__c = ' + KyperaProcessor.metadata.get('ServiceHandler').MetaData__c);
        req.setHeader('Content-Type', 'text/xml; charset=utf-8');
        req.setHeader('SOAPAction', KyperaProcessor.metadata.get(serviceEndPointName).MetaData__c); 
        system.debug('*** KyperaProcessor.metadata.get(serviceEndPointName).MetaData__c = ' + KyperaProcessor.metadata.get(serviceEndPointName).MetaData__c);
        system.debug('*** serviceEndPointName = ' + serviceEndPointName);
        req.setBody(xmlRequest);
        system.debug('*** req.getBody: '+ req.getBody());
        Http http = new Http();
        try { 
            if (!Test.isRunningTest()){ 
                res = http.send(req);         
                System.debug('*** res.body: '+ res.getBody());
                System.debug('*** res.getStatus: '+ res.getStatus());
                System.debug('*** res.getStatusCode:'+ res.getStatusCode());   
            } else {
                return responseStub;
            }
            return res.getBody();
        } catch(System.CalloutException e) {
            response = e.GetMessage();
            return '';
        }     
    }  
    
    // builds a map of field names and field values from Kypera
    global void populateContactFieldMap(String xmlResponse) {
        List<Kypera_Contact_Fields__c> contactFields = [SELECT Id, Element_Name__c, Name FROM Kypera_Contact_Fields__c WHERE Do_Not_Sync__c =: false ORDER BY Order__c];
        system.debug('§§§ contactFields.size = ' + contactFields.size());
        system.debug('§§§ xmlResponse = ' + xmlResponse);
        // populates contactFromKyperaFields
        for (Kypera_Contact_Fields__c fld : contactFields) {
            contactFromKyperaFields.put(fld.Element_Name__c, getXMLText(xmlResponse, fld.Element_Name__c, 0).replace('<![CDATA[', '').replace(']]>', ''));        
        }
    }
    
    // gets the text from a given XML tag. It is needed because the response from create person is badly formed and returns encoded versions of < and > 
    global string getXMLText(string xmlString, string tagName, integer position) {
        string retVal = '';
        if(xmlString.contains('<' + tagName + '>') && xmlString.contains('</' + tagName + '>')){
          try{
              retVal = xmlString.substring(xmlString.indexOf('<' + tagName + '>', position) + tagName.length() + 2,
                xmlString.indexOf('</' + tagName + '>', position));   
            }catch (exception e){
                system.debug('*** Exception in method getXMLText ' + e.getMessage() + ' tagName = ' + tagName + ' xmlString = ' + xmlString);
            } 
        }
        return retVal;
    }    
    
    global PageReference updateContactFromKyperaData() {
        system.debug('$$$ entering updateContactFromKyperaData where hasNoErrors = ' + hasNoErrors);

        // re-initialise the list of fields and their settings
        fields = [
             SELECT 
                Id, Name, Element_Name__c, Order__c, Do_Not_Sync__c, Operation__c,
                Override__c, Has_Mapping__c, Has_Reverse_Mapping__c, Kypera_Data_Type__c, Can_Sync_Back__c
             FROM
                Kypera_Contact_Fields__c
             ORDER BY 
                Order__c]; 
                
        fieldsThatCanSyncBack = new Map<String, Boolean>();   
        for (Kypera_Contact_Fields__c kcf : fields) {
            fieldsThatCanSyncBack.put(kcf.Name, kcf.Can_Sync_Back__c);
        }    
                     
        // initialise the translations of In-Form picklists to Kypera picklists
        mappings = [SELECT
                Id, Kypera_Contact_Field__c, In_Form_Value__c, Kypera_Value__c
             FROM
                Kypera_Contact_Field_Mappings__c];
                
        // initialise the translations of Kypera picklists to In-Form picklists
        reversemappings = [
             SELECT
                Id, Kypera_Contact_Field__c, In_Form_Value__c, Kypera_Value__c
             FROM
                Kypera_Contact_Field_Reverse_Mappings__c];
                
        // initialise the schema variables
        soType = Schema.getGlobalDescribe().get('Contact');
        soRes = soType.getDescribe();
        
        // get metadata
        metadata = Kypera_Metadata__c.getAll();    
        
        // variables to hold the value of the field from Kypera 
        string strFieldValue = '';
        Date dteFieldValue = date.parse('01/01/1970'); // default the date to the epoch in Unix time
        boolean booFieldValue = false;
        string fieldType = '';
       
        // put all the mapped fields that can sync back into the contact object's map
        system.debug('$$$ KyperaProcessor.fields ' + fields);
        for(Kypera_Contact_Fields__c field : fields) {
        
            // only process fields where the Can Sync Back checkbox is checked
            if (field.Can_Sync_Back__c == true) {
                // set blank or default values for the field variables 
                strFieldValue = '';
                dteFieldValue = date.parse('01/01/1970'); // default the date to the epoch in Unix time
                booFieldValue = false; 
                
                // get the data type of the field
                fdRes = soRes.fields.getMap().get(field.Name).getDescribe();   
                fieldType = fdRes.getType().Name();  
                system.debug('^^^^^^^ ' + fieldType + field.Name); 
                
                // get the fieldValues from data returned by Kypera
                strFieldValue = contactFromKyperaFields.get(field.Element_Name__c);

                // if the field is not blank and (reverse) translation is required then perform the (reverse) translation
                if (!String.IsBlank(strFieldValue) && field.Has_Reverse_Mapping__c > 0) {
                    strFieldValue = reverseTranslatePicklist(field.Id, strFieldValue);
                }
                
                                
                // if the field is not blank and is a date field, convert it
                if (!String.IsBlank(strFieldValue) && fieldType == 'DATE') {
                    dtefieldValue = reformatDate(strFieldValue);
                }
 
                try {
                    if (fieldType == 'STRING' || fieldType =='PICKLIST' || fieldType == 'MULTIPICKLIST') {
                        con.put(field.Name, strFieldValue);
                        system.debug('String change ' + strFieldValue);
                    }
                    if (fieldType == 'Date') {
                        con.put(field.Name, dteFieldValue);
                        system.debug('Date change ' + dteFieldValue);                        
                    }
                    system.debug('$$$ SUCCESS contact field ' + field.Name + ' Kypera field ' + field.Element_Name__c + ' Id = ' + field.Id + ' strFieldValue ' + strFieldValue + ' dteFieldValue ' + dteFieldValue);                    
                } catch (Exception e) { 
                    system.debug('$$$ ERROR hasNoErrors = ' + hasNoErrors);            
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,  + e.GetMessage() + ' ... Please check the "Can Sync Back" field on the "Kypera Contact Fields"  object for the field called ' + field.Name + ' (' + field.Id +')'));   
                    system.debug('$$$ ERROR contact field ' + field.Name + ' Kypera field ' + field.Element_Name__c + ' Id = ' + field.Id + ' strFieldValue ' + strFieldValue + ' dteFieldValue ' + dteFieldValue + ' error message ' + e.getMessage());                
                    return null;
                }
            }
        }
        
        
        // update the contact
        try {
            update con;
        }
        catch (Exception e) {
            ApexPages.addMessages(e);
            return null;
        }
    
        return null;
    }  
    
    // reverse translates picklists, i.e. converts Kypera values to In-Form values
    global string reverseTranslatePicklist(Id fieldId, String fieldData) {            
        string retVal = fieldData;
        String lookupVal = '';
        // a map with the In-Form value as the key and the Kypera translation as the value                
        Map<String, String> fieldMappings = new Map<String, String>();
        for (Kypera_Contact_Field_Reverse_Mappings__c currentMap : reverseMappings) {
            if (fieldId == currentMap.Kypera_Contact_Field__c) {
                fieldMappings.put(currentMap.Kypera_Value__c, currentMap.In_Form_Value__c);
            }        
        }
        lookupVal = fieldMappings.get(fieldData);
        if (!String.IsBlank(lookupVal)) {
            retVal = lookupVal;
        }
        return retVal;
    }    
    
    // reformat dates
    global Date reformatDate(String dateValue) {
        // handles the blank formats that Kypera sends back
        if (dateValue == '00:00:00') {
            return null;
        }
        Date retVal = Date.parse(dateValue.substring(0,2)+'/'+dateValue.substring(3,5)+'/'+dateValue.substring(6,10));
        return retVal;    
    }

}