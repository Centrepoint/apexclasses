public class RecentSystemChangesController {
    // Create a locally declared list array to hold all of the Requests for Support - System Improvements
    private List<Request_Support__c> systemImprovements{get; set;}
    
    // Fetch the RecordTypeIds from the Custom Labels - this removes the need to 
    // hardcode variables within the code
    String SystemImprovementRecordTypeId = Label.Request_for_Support_System_Improvement_Record_Id;
    
    Date relativeDate = Date.Today().addDays(-30);

    // Declare a Standard Controller - this is so the data can be accessed using a Visualforce page
    public RecentSystemChangesController(ApexPages.StandardController controller) {
        
        // // Fetch this particular Action Plan and Outcomes Star record 
        // this.APOS = (Homelessness_Outcomes_Star__c)controller.getRecord();
        // // Fetch the Id and CreatedById values from this Action Plan and Outcomes Star record
        // // 
        // // NOTE - You need to fetch [SELECT] variables in order to reference them later in the code
        // this.APOS = [SELECT Id, CreatedById FROM Homelessness_Outcomes_Star__c WHERE Id = :APOS.Id];
    
    }
    // Create a function to populate the systemImprovements List
    public List<Request_Support__c> getSystemImprovements() {
        systemImprovements = new List<Request_Support__c>();

		// Hold systemImprovements in a temporary List whilst the results are filtered
		// Fetch [SELECT] the variables from [FROM] each Request for Support record which [WHERE] has a 
		// RecordTypeId of System Improvement  
        List<Request_Support__c> systemImprovementsTemp = [SELECT Id, What_System_does_this_relate_to__c, Change_Req_Details__c, 
                                                            Change_Req_Justification__c, Change_Req_Level_of_Change__c, Date_of_Request__c, 
                                                            Change_Req_Planned_Completion_Date__c, Change_Req_Actual_Completion_Date__c, 
                                                            Ask_Qu_Admin_Answer__c, Admin_Assigned__c, Ask_Qu_Admin_Notes__c, Change_Req_Admin_Notes__c, RecordTypeId  
                                                            FROM Request_Support__c 
                                                            WHERE RecordTypeId = :SystemImprovementRecordTypeId];
        // Now further filter the results to show only ones with an impact of major and were completed recently
        for(Request_Support__c singleRequestForSupport : systemImprovementsTemp){
            if(singleRequestForSupport.What_System_does_this_relate_to__c == 'In-Form' &&
            singleRequestForSupport.Change_Req_Actual_Completion_Date__c > relativeDate &&
            singleRequestForSupport.Change_Req_Level_of_Change__c == 'Major'){
                systemImprovements.add(singleRequestForSupport);
            }
        }
        return systemImprovements;
    }
}