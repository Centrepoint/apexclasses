public class KyperaBedspaceDataProcessorParser {

    // member level variable that is a map of scheme codes and xml
    public Map<String, String> schemeLevelXML = new Map<String, String>();
    // member level variables to retrieve Kypera Property records then update them
    public List<Kypera_Properties__c> kPropertiesToUpdate  = new List<Kypera_Properties__c>();
    public List<Kypera_Properties__c> kPropertiesToSelect = new List<Kypera_Properties__c>();
    Map<String, Kypera_Properties__c> kPropertiesToSelectMap = new Map<String, Kypera_Properties__c>();    
    // This is a list to hold data that's extracted & deserialised from the XML
    public List<Map<String, String>> datalist = new List<Map<String, String>>();
    // maintain a set of scheme codes that failed
    public Set<String> schemeCodeFailures = new Set<String>();
    // A log record - passed into the constructor by KyperaBedspaceDataProcessor
    public Kypera_Bedspace_Import_Log__c importLog;

    // constructor
    public KyperaBedspaceDataProcessorParser(Kypera_Bedspace_Import_Log__c log) {
        importLog = log;
    }

    // add a scheme code and it's XML to schemeLevelXML
    public void addScheme(String schemeCode, String xmlData) {
        schemeLevelXML.put(schemeCode, xmlData);
    }

    /* 
        - iterate over the map of scheme codes and xml 
        - parse it into a list of Kypera Properties to update
        - add the Id to the list of Kypera Properties to update from a list of live properties
        - use the list of Kypera Properties to update to actually update the Kypera Properties
    */
    public void processSchemeLevelData() {
        try {
            importLog.Stage__c = '002 Processing scheme level data';
            importLog.Technical_Information__c = 'KyperaBedspaceDataProcessorParser.processSchemeLevelData() entered';
            update importLog;
            // iterate over the map of scheme codes and xml 
            for (String schemeCode : schemeLevelXML.keyset()) {    
                if (schemeLevelXML.get(schemeCode) == null) {schemeCodeFailures.add(schemeCode); //'Nothing to process'
                } else {
                    String toParse = schemeLevelXML.get(schemeCode).replace('</unit>', '</unit><endOfUnit>EOF</endOfUnit>');
                    DOM.Document doc = new DOM.Document();
                    try {
                        doc.load(toParse);    
                        DOM.XMLNode root = doc.getRootElement();
                        dataList.clear();
                        parseXMLForSchemes(root, schemeCode); // add the information to dataList, a list of key value pairs                  
                        processDataList(schemeCode); // processes the information in dataList & adds it to kPropertiesToUpdate, a list of properties to update                
                    } catch (System.XMLException e) {  // invalid XML
                        importLog.Technical_Information__c = 'KyperaBedspaceDataProcessorParser.processSchemeLevelData() XML error; scheme code = ' + schemeCode;
                        importLog.Exception__c = e.getMessage(); update importLog;
                    }
                }
            }
            addIds(); // add Ids to kPropertiesToUpdate using data from kPropertiesToSelectMap
            removeNoIds(); // remove records with no Ids in kPropertiesToUpdate
            try {  
                update kPropertiesToUpdate;  // perform the update
            } catch(Exception e) {
                importLog.Stage__c = '002 Processing scheme level data';
                importLog.Technical_Information__c = 'KyperaBedspaceDataProcessorParser.processSchemeLevelData() DML Error';
                importLog.Exception__c = e.getMessage();
                update importLog;        
            }
            importLog.Stage__c = '004 Completed Processing scheme level data';
            importLog.Technical_Information__c = 'KyperaBedspaceDataProcessorParser.processSchemeLevelData() exiting';
            importLog.Batch_End_Date__c = DateTime.Now();
            update importLog;
        } catch (Exception e) {
        
        }
    }

    // This converts the XML data into a list of key value pairs for easier processing
    public void parseXMLForSchemes(DOM.XMLNode node, String schemeCode) {
      Map<String, String> keyValue = new Map<String, String>();
      if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
        if (node.getText().trim() != '') {
          keyValue.clear();
          keyValue.put(node.getName(), node.getText().trim());
          dataList.add(keyValue);
        }            
        if (node.getName() == 'housingServiceCharge') {
            keyValue.clear();
            keyValue.put('housingServiceCharge', String.ValueOf(node.getChildElements().size()));
            dataList.add(keyValue);    
        }
        if (node.getName() == 'personalServiceCharge') {
            keyValue.clear();
            keyValue.put('personalServiceCharge', String.ValueOf(node.getChildElements().size()));
            dataList.add(keyValue);
        }
        if (node.getAttributeCount() == 2 && node.getName() == 'charge') { 
          keyValue.clear();
          keyValue.put(node.getAttributeValue(node.getAttributeKeyAt(0), node.getAttributeKeyNsAt(0)), node.getAttributeValue(node.getAttributeKeyAt(1), node.getAttributeKeyNsAt(1)));
          dataList.add(keyValue);        
        }
        for (Dom.XMLNode child: node.getChildElements()) {
          //System.Debug('### recursing over ' + node.getName());
          parseXMLForSchemes(child, schemeCode);
        }
        if (node.getName() == 'endOfUnit') {
          keyValue.clear();
          keyValue.put('EOF', 'true');
        }
        // end 
      }
      //should never reach here      
    }
    
    // adds record Ids to kPropertiesToUpdate so we can update data
    public void addIds() {
        // intialise the list of existing properties on the system
        for (Kypera_Properties__c k : [SELECT Id, Name, Unit_Reference__c, Scheme_Reference__c FROM Kypera_Properties__c WHERE Unit_Reference__c !=: null LIMIT 10000]) {
           kPropertiesToSelect.add(k);
           kPropertiesToSelectMap.put(k.Unit_Reference__c, k);
        }    
        // add the Id to the list of properties to update
        for (Kypera_Properties__c kx: kPropertiesToUpdate) {
            kx.Id = kPropertiesToSelectMap.get(kx.Unit_Reference__c).Id;
            if (kx.Id == null && kx.Scheme_Reference__c != null) {
                schemeCodeFailures.add(kx.Scheme_Reference__c);
            }
        }
    }
    
    // removes records that do not have an Id in kPropertiesToUpdate
    public void removeNoIds() {
        Integer i = 0;
        List<Integer> idsToRemove = new List<Integer>();
        for (Kypera_Properties__c kx: kPropertiesToUpdate) {
            if (kx.Id == null) {
                idsToRemove.add(i);
            }
            i++;
        }
        for (Integer n : idsToRemove) {
            kPropertiesToUpdate.remove(n);
        }      
    }    
    
    public void processDataList(String schemeCode) {
        importLog.Stage__c = '003 Processing data for scheme ' + schemeCode;
        importLog.Technical_Information__c = 'KyperaBedspaceDataProcessorParser.processDataList() entered';
        update importLog;
        // convert the data into a Kypera Property record
        String serviceNumber, serviceStreet, serviceTown, serviceCounty, servicePostcode, serviceInception, serviceDecommission;
        String serviceTelephoneNumber1, serviceTelephoneNumber2, serviceHousingOfficer, serviceManager, serviceSSH01, serviceSSH02, serviceAreaCode, landlord;
        String unitName, unitReference, propertyType, defaultOccupancyType, ownerShip, propertySubType;
        Decimal hCoreRent, hCleaningOfCommunalAreas, hCouncilTax, hElectricity, hFood, hGas, hHeatAndLight;
        Decimal hLaundryFacilities, hOtherIncludingToiletries, hRenewalsAndReplacements, hServiceContracts, hTVLicence, hWater, hAdminAndManagement;
        Decimal pCoreRent, pCleaningOfCommunalAreas, pCouncilTax, pElectricity, pFood, pGas, pHeatAndLight, pAdminAndManagement;
        Decimal pLaundryFacilities, pOtherIncludingToiletries, pRenewalsAndReplacements, pServiceContracts, pTVLicence, pWater;
        Boolean hCoreRentProcessed = false, hCleaningOfCommunalAreasProcessed = false, hCouncilTaxProcessed = false;
        Boolean hElectricityProcessed = false, hFoodProcessed = false, hGasProcessed = false, hHeatAndLightProcessed = false;
        Boolean hLaundryFacilitiesProcessed = false, hOtherIncludingToiletriesProcessed = false, hRenewalsAndReplacementsProcessed = false;
        Boolean hServiceContractsProcessed = false, hTVLicenceProcessed = false, hWaterProcessed = false;
        Boolean pCoreRentProcessed = false, pCleaningOfCommunalAreasProcessed = false, pCouncilTaxProcessed = false;
        Boolean pElectricityProcessed = false, pFoodProcessed = false, pGasProcessed = false, pHeatAndLightProcessed = false;
        Boolean pLaundryFacilitiesProcessed = false, pOtherIncludingToiletriesProcessed = false, pRenewalsAndReplacementsProcessed = false;
        Boolean pServiceContractsProcessed = false, pTVLicenceProcessed = false, pWaterProcessed = false;        
        boolean hasHousingServiceCharge = false;
        boolean hasPersonalServiceCharge = false, pAdminAndManagementProcessed = false, hAdminAndManagementProcessed = false;
        system.debug('dataList'+dataList);
        for (Map<String, String> s : dataList) {
            system.debug(s);
            if (s.containsKey('number')) {
                serviceNumber = s.get('number');
            }
            if (s.containsKey('street')) {
                serviceStreet = s.get('street');
            }
            if (s.containsKey('town')) {
                serviceTown = s.get('town');
            }
            if (s.containsKey('county')) {
                serviceCounty = s.get('county');
            }
            if (s.containsKey('postcode')) {
                servicePostcode = s.get('postcode');
            }
            if (s.containsKey('inception')) {
                serviceInception = s.get('inception');
            }
            if (s.containsKey('decommision')) {
                serviceDecommission = s.get('decommision');
            }
            if (s.containsKey('telephoneNumber1')) {
                serviceTelephoneNumber1 = s.get('telephoneNumber1');
            }
            if (s.containsKey('telephoneNumber2')) {
                serviceTelephoneNumber2 = s.get('telephoneNumber2');
            }
            if (s.containsKey('housingOfficer')) {
                serviceHousingOfficer = s.get('housingOfficer');
            }
            if (s.containsKey('serviceManager')) {
                serviceManager = s.get('serviceManager');
            }
            if (s.containsKey('SSHO1')) {
                serviceSSH01 = s.get('SSHO1');
            }
            if (s.containsKey('SSHO2')) {
                serviceSSH02 = s.get('SSHO2');
            }
            if (s.containsKey('areaCode')) {
                serviceAreaCode = s.get('areaCode');
            }
        
            /* Get the code - this is the Unit Reference even though there are multiple code tags because 
               the code which is the Unit Reference is the innermost code tag and because, at the end of
               the unit, there is a EOF tag which is inserted at the end of each unit in parseXMLForSchemes */
            if (s.containsKey('code')) {
                unitReference = s.get('code');
            }
            // Housing Service Charge fields
            if (s.containsKey('housingServiceCharge')) {
                if (s.get('housingServiceCharge') != null && Integer.valueOf(s.get('housingServiceCharge')) > 0) {
                    hasHousingServiceCharge = true;
                }
            }
            if (hasHousingServiceCharge && s.containsKey('RENT - Core') && !hCoreRentProcessed) {
                hCoreRent = Decimal.valueOf(s.get('RENT - Core'));
                hCoreRentProcessed = true;
            }
            if (hasHousingServiceCharge && s.containsKey('RENT - Admin and Management') && !hAdminAndManagementProcessed) {
                hAdminAndManagement = Decimal.valueOf(s.get('RENT - Admin and Management'));
                hAdminAndManagementProcessed = true;
            }
            if (hasHousingServiceCharge && s.containsKey('RENT - Clean communal & Garde') && !hCleaningOfCommunalAreasProcessed) {
                hCleaningOfCommunalAreas = Decimal.valueOf(s.get('RENT - Clean communal & Garde'));
                hCleaningOfCommunalAreasProcessed = true;
            }
            if (hasHousingServiceCharge && s.containsKey('RENT - Council tax') && !hCouncilTaxProcessed) {
                hCouncilTax = Decimal.valueOf(s.get('RENT - Council tax'));
                hCouncilTaxProcessed = true;
            }
            if (hasHousingServiceCharge && s.containsKey('PSC - Electricity') && !hElectricityProcessed) {
                hElectricity = Decimal.valueOf(s.get('PSC - Electricity'));
                hElectricityProcessed = true;
            }
            if (hasHousingServiceCharge && s.containsKey('PSC - Food') && !hFoodProcessed) {
                hFood = Decimal.valueOf(s.get('PSC - Food'));
                hFoodProcessed = true;
            }
            if (hasHousingServiceCharge && s.containsKey('PSC - Gas') && !hGasProcessed) {
                hGas = Decimal.valueOf(s.get('PSC - Gas'));
                hGasProcessed = true;
            }
            if (hasHousingServiceCharge && s.containsKey('RENT - Heat & light') && !hHeatAndLightProcessed) {
                hHeatAndLight = Decimal.valueOf(s.get('RENT - Heat & light'));
                hHeatAndLightProcessed = true;
            }
            if (hasHousingServiceCharge && s.containsKey('RENT - Laundry facilities') && !hLaundryFacilitiesProcessed) {
                hLaundryFacilities = Decimal.valueOf(s.get('RENT - Laundry facilities'));
                hLaundryFacilitiesProcessed = true;
            }
            if (hasHousingServiceCharge && s.containsKey('PSC - Other inc toiletries') && !hOtherIncludingToiletriesProcessed) {
                hOtherIncludingToiletries = Decimal.valueOf(s.get('PSC - Other inc toiletries'));
                hOtherIncludingToiletriesProcessed = true;
            }
            if (hasHousingServiceCharge && s.containsKey('RENT - Renewals and replaceme') && !hRenewalsAndReplacementsProcessed) {
                hRenewalsAndReplacements = Decimal.valueOf(s.get('RENT - Renewals and replaceme'));
                hRenewalsAndReplacementsProcessed = true;
            }
            if (hasHousingServiceCharge && s.containsKey('RENT - Service contracts ') && !hServiceContractsProcessed) {
                hServiceContracts = Decimal.valueOf(s.get('RENT - Service contracts '));
                hServiceContractsProcessed = true;
            }
            if (hasHousingServiceCharge && s.containsKey('PSC - TV Licence') && !hTVLicenceProcessed) {
                hTVLicence = Decimal.valueOf(s.get('PSC - TV Licence'));
                hTVLicenceProcessed = true;
            } 
            if (hasHousingServiceCharge && s.containsKey('PSC - Water') && !hWaterProcessed) {
                hWater = Decimal.valueOf(s.get('PSC - Water'));
                hWaterProcessed = true;
            }              
            
            // Personal Service Charge fields
            if (s.containsKey('personalServiceCharge')) {
                if (s.get('personalServiceCharge') != null && Integer.valueOf(s.get('personalServiceCharge')) > 0) {
                    hasPersonalServiceCharge = true;
                }
            }
            if (hasPersonalServiceCharge && s.containsKey('RENT - Core') && !pCoreRentProcessed) {
                pCoreRent = Decimal.valueOf(s.get('RENT - Core'));
                pCoreRentProcessed = true;
            }
            if (hasPersonalServiceCharge && s.containsKey('RENT - Admin and Management') && !pAdminAndManagementProcessed) {
                pAdminAndManagement = Decimal.valueOf(s.get('RENT - Admin and Management'));
                pAdminAndManagementProcessed = true;
            }
            if (hasPersonalServiceCharge && s.containsKey('RENT - Clean communal & Garde') && !pCleaningOfCommunalAreasProcessed) {
                pCleaningOfCommunalAreas = Decimal.valueOf(s.get('RENT - Clean communal & Garde'));
                pCleaningOfCommunalAreasProcessed = true;
            }  
            if (hasPersonalServiceCharge && s.containsKey('RENT - Council tax') && !pCouncilTaxProcessed) {
                pCouncilTax = Decimal.valueOf(s.get('RENT - Council tax'));
                pCouncilTaxProcessed = true;
            }
            if (hasPersonalServiceCharge && s.containsKey('PSC - Electricity') && !pElectricityProcessed) {
                pElectricity = Decimal.valueOf(s.get('PSC - Electricity'));
                pElectricityProcessed = true;
            }
            if (hasPersonalServiceCharge && s.containsKey('PSC - Food') && !pFoodProcessed) {
                pFood = Decimal.valueOf(s.get('PSC - Food'));
                pFoodProcessed = true;
            }
            if (hasPersonalServiceCharge && s.containsKey('PSC - Gas') && !pGasProcessed) {
                pGas = Decimal.valueOf(s.get('PSC - Gas'));
                pGasProcessed = true;
            }
            if (hasPersonalServiceCharge && s.containsKey('RENT - Heat & light') && !pHeatAndLightProcessed) {
                pHeatAndLight = Decimal.valueOf(s.get('RENT - Heat & light'));
                pHeatAndLightProcessed = true;
            }
            if (hasPersonalServiceCharge && s.containsKey('RENT - Laundry facilities') && !pLaundryFacilitiesProcessed) {
                pLaundryFacilities = Decimal.valueOf(s.get('RENT - Laundry facilities'));
                pLaundryFacilitiesProcessed = true;
            }
            if (hasPersonalServiceCharge && s.containsKey('PSC - Other inc toiletries') && !pOtherIncludingToiletriesProcessed) {
                pOtherIncludingToiletries = Decimal.valueOf(s.get('PSC - Other inc toiletries'));
                pOtherIncludingToiletriesProcessed = true;
            }
            if (hasPersonalServiceCharge && s.containsKey('RENT - Renewals and replaceme') && !pRenewalsAndReplacementsProcessed) {
                pRenewalsAndReplacements = Decimal.valueOf(s.get('RENT - Renewals and replaceme'));
                pRenewalsAndReplacementsProcessed = true;
            }
            if (hasPersonalServiceCharge && s.containsKey('RENT - Service contracts ') && !pServiceContractsProcessed) {
                pServiceContracts = Decimal.valueOf(s.get('RENT - Service contracts '));
                pServiceContractsProcessed = true;
            }  
            if (hasPersonalServiceCharge && s.containsKey('PSC - TV Licence') && !pTVLicenceProcessed) {
                pTVLicence = Decimal.valueOf(s.get('PSC - TV Licence'));
                pTVLicenceProcessed = true;
            } 
            if (hasPersonalServiceCharge && s.containsKey('PSC - Water') && !pWaterProcessed) {
                pWater = Decimal.valueOf(s.get('PSC - Water'));
                pWaterProcessed = true;
            }    
            if (s.containsKey('propertyType')) {
                propertyType = s.get('propertyType');
            }
            if (s.containsKey('defaultOccupancyType')) {
                defaultOccupancyType = s.get('defaultOccupancyType');
            } 
            if (s.containsKey('propertySubType')) {
                propertySubType = s.get('propertySubType');
            } 
            if (s.containsKey('ownerShip')) {
                ownerShip = s.get('ownerShip');
            }           
            if (s.containsKey('landlordName')) {
                landlord = s.get('landlordName');
            }
            if (s.containsKey('EOF')) { // we have reached the end of a unit so...
                // add it to a list
                kPropertiesToUpdate.add(
                    new Kypera_Properties__c(
                        Unit_Reference__c = unitReference,
                        H_Core_Rent__c = hCoreRent,
                        P_Core_Rent__c = pCoreRent,
                        H_Cleaning_of_communal_areas_and__c = hCleaningOfCommunalAreas,
                        P_Cleaning_of_communal_areas_and__c = pCleaningOfCommunalAreas,
                        H_Admin_and_Management__c = hAdminAndManagement,
                        P_Admin_and_Management__c = pAdminAndManagement,
                        H_Council_Tax__c = hCouncilTax,
                        P_Council_Tax__c = pCouncilTax,
                        H_Electricity__c = hElectricity,
                        P_Electricity__c = pElectricity,
                        H_Food__c = hFood,
                        P_Food__c = pFood,
                        H_Gas__c = hGas,
                        P_Gas__c = pGas,
                        H_Heat_light__c = hHeatAndLight,
                        P_Heat_light__c = pHeatAndLight,
                        H_Laundry_facilities__c = hLaundryFacilities,
                        P_Laundry_facilities__c = pLaundryFacilities,
                        H_Other_including_toiletries__c = hOtherIncludingToiletries,
                        P_Other_including_toiletries__c = pOtherIncludingToiletries,
                        H_Renewals_and_replacements__c = hRenewalsAndReplacements,
                        P_Renewals_and_replacements__c = pRenewalsAndReplacements,
                        H_Service_Contracts__c = hServiceContracts,
                        P_Service_Contracts__c = pServiceContracts,
                        H_TV_Licence__c = hTVLicence,
                        P_TV_Licence__c = pTVLicence,
                        H_Water__c = hWater,
                        P_Water__c = pWater,
                        Bedspace_Type__c = propertyType,
                        Type_of_tenancy__c = defaultOccupancyType,
                        S_Area_Code__c =  serviceAreaCode,
                        S_County__c =  serviceCounty,
                        S_Housing_Officer__c =  serviceHousingOfficer,
                        S_Decommision__c =  serviceDecommission,
                        S_Inception__c =  serviceInception,
                        S_Number__c =  serviceNumber,
                        S_Postcode__c =  servicePostcode,
                        S_Service_Manager__c =  serviceManager,
                        S_SSHO1__c =  serviceSSH01,
                        S_SSHO2__c =  serviceSSH02,
                        S_Street__c =  serviceStreet,
                        S_Telephone_Number_1__c =  serviceTelephoneNumber1,
                        S_Telephone_Number_2__c =  serviceTelephoneNumber2,
                        S_Town__c =  serviceTown, 
                        Subtype__c = propertySubType,
                        Ownership__c = ownerShip,
                        Landlord__c = landlord                       
                    )
                );
                // reset the variables for the next iteration
                hasHousingServiceCharge = false;
                hasPersonalServiceCharge = false;
                pAdminAndManagementProcessed = false;
                hAdminAndManagementProcessed = false;
                hCoreRent = null;
                pCoreRent = null;
                hCoreRentProcessed = false;
                pCoreRentProcessed = false;
                hCleaningOfCommunalAreas = null;
                pCleaningOfCommunalAreas = null;
                hCleaningOfCommunalAreasProcessed = false;
                pCleaningOfCommunalAreasProcessed = false;
                hCouncilTax = null;
                pCouncilTax = null;
                hCouncilTaxProcessed = false;
                pCouncilTaxProcessed = false;
                hElectricity = null;
                pElectricity = null;
                hElectricityProcessed = false;
                pElectricityProcessed = false;
                hFood = null;
                pFood = null;
                hFoodProcessed = false;
                pFoodProcessed = false;
                hGas = null;
                pGas = null;
                hGasProcessed = false;
                pGasProcessed = false;
                hHeatAndLight = null;
                pHeatAndLight = null;
                hHeatAndLightProcessed = false;
                pHeatAndLightProcessed = false;
                hLaundryFacilities = null;
                pLaundryFacilities = null;
                hLaundryFacilitiesProcessed = false;
                pLaundryFacilitiesProcessed = false;
                hOtherIncludingToiletries = null;
                pOtherIncludingToiletries = null;
                hOtherIncludingToiletriesProcessed = false;
                pOtherIncludingToiletriesProcessed = false;
                hRenewalsAndReplacements = null;
                pRenewalsAndReplacements = null;
                hRenewalsAndReplacementsProcessed = false;
                pRenewalsAndReplacementsProcessed = false;
                hServiceContracts = null;
                pServiceContracts = null;
                hServiceContractsProcessed = false;
                pServiceContractsProcessed = false;
                hTVLicence = null;
                pTVLicence = null;
                hTVLicenceProcessed = false;
                pTVLicenceProcessed = false;
                hWater = null;
                pWater = null;
                hWaterProcessed = false;
                pWaterProcessed = false;
                propertyType = null;
                defaultOccupancyType = null;
                propertySubType = null;
                ownerShip = null;
            } // if (s.containsKey('EOF'))
        }// end for loop  
    }

}