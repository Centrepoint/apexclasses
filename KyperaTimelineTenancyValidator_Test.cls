@IsTest
global class KyperaTimelineTenancyValidator_Test {

    global static testMethod void testTimelineEventValidation() {

    Test.StartTest();
        
        // 1. Setup
    
        // turn on integration in the custom setting
        Trigger_Settings__c ts = new Trigger_Settings__c();
        ts.Sync_With_Kypera__c = true;
        ts.Timeline_Validation_Excluded_Profile__c = '';
        insert ts;
                
        // Get record type Ids
        List<RecordType> rTypeList = [SELECT Id, Name, SobjectType FROM RecordType WHERE SobjectType IN ('Account', 'Contact', 'infrm__TimelineEvents__c')];
        Map<String, Id> accTypeMap = new Map<String, Id>();
        Map<String, Id> conTypeMap = new Map<String, Id>();
        Map<String, Id> tetTypeMap = new Map<String, Id>();
        for (RecordType rt : rTypeList) {
            if (rt.SobjectType == 'Contact') {
                conTypeMap.put(rt.Name, rt.Id);
            }
            if (rt.SobjectType == 'Account') {
                accTypeMap.put(rt.Name, rt.Id);
            }
            if (rt.SobjectType == 'infrm__TimelineEvents__c') {
                tetTypeMap.put(rt.Name, rt.Id);
            }
        }
        
        // Create Kypera Contact Fields data      
        List<Kypera_Contact_Fields__c> fields = new List<Kypera_Contact_Fields__c>();
        fields.add(new Kypera_Contact_Fields__c(Name = 'Salutation', Do_Not_Sync__c = false, Element_Name__c = 'Title', Kypera_Data_Type__c = 'String', Order__c = 10, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = 'FirstName', Do_Not_Sync__c = false, Element_Name__c = 'Forename', Kypera_Data_Type__c = 'String', Order__c = 20, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = 'LastName', Do_Not_Sync__c = false, Element_Name__c = 'Surname', Kypera_Data_Type__c = 'String', Order__c = 25, Operation__c = 'Create; Update'));        
        fields.add(new Kypera_Contact_Fields__c(Name = 'Name', Do_Not_Sync__c = false, Element_Name__c = 'Salutation', Kypera_Data_Type__c = 'String', Order__c = 30, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = 'infrm__Gender__c', Do_Not_Sync__c = false, Element_Name__c = 'Gender', Kypera_Data_Type__c = 'String', Order__c = 40, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = 'infrm__Ethnic_Origin__c', Do_Not_Sync__c = false, Element_Name__c = 'EthnicOrigin', Kypera_Data_Type__c = 'String', Order__c = 50, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = 'Id', Do_Not_Sync__c = false, Element_Name__c = 'ExternalApplicationReference', Kypera_Data_Type__c = 'String', Order__c = 60, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = 'Kypera_Reference__c', Do_Not_Sync__c = false, Element_Name__c = 'KyperaReference', Kypera_Data_Type__c = 'String', Order__c = 65, Operation__c = 'Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = '-', Do_Not_Sync__c = true, Element_Name__c = 'OtherName', Kypera_Data_Type__c = 'String', Order__c = 70, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = 'Birthdate', Do_Not_Sync__c = false, Element_Name__c = 'DateOfBirth', Kypera_Data_Type__c = 'String', Order__c = 80, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = 'infrm__client_status__c', Do_Not_Sync__c = false, Element_Name__c = 'MaritalStatus', Kypera_Data_Type__c = 'String', Order__c = 90, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = 'infrm__Religion__c', Do_Not_Sync__c = false, Element_Name__c = 'ReligionBelief', Kypera_Data_Type__c = 'String', Order__c = 100, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = 'infrm__physical_health_issues__c', Do_Not_Sync__c = false, Element_Name__c = 'MedicalDetails', Kypera_Data_Type__c = 'String', Order__c = 110, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = 'infrm__Disabled_person__c', Do_Not_Sync__c = false, Element_Name__c = 'RegisteredDisabled', Kypera_Data_Type__c = 'Boolean', Order__c = 120, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = '-', Do_Not_Sync__c = true, Element_Name__c = 'SpecialNeeds', Kypera_Data_Type__c = 'String', Order__c = 130, Operation__c = 'Create; Update'));
        insert fields;
        
        fields = [SELECT Id, Name FROM Kypera_Contact_Fields__c];
        Map<String, Id> fieldmap = new Map<String, Id>();
        for (Kypera_Contact_Fields__c kcf : fields) {
            fieldmap.put(kcf.Name, kcf.Id);
        }
        
        // Create Kypera Contact Field Mappings data
        List<Kypera_Contact_Field_Mappings__c> mappings = new List<Kypera_Contact_Field_Mappings__c>();
        mappings.add(new Kypera_Contact_Field_Mappings__c(Kypera_Contact_Field__c = fieldmap.get('infrm__Disabled_person__c'), In_Form_Value__c = 'Yes', Kypera_Value__c = 'true'));
        
        insert mappings;

        // Create Kypera Metadata in the Custom Setting
        List<Kypera_Metadata__c> kms = new List<Kypera_Metadata__c>();
        kms.add(new Kypera_Metadata__c(Name = 'createPersonBegin', Metadata__c = '<CreatePerson xmlns="http://mobile.kypera.com/InformWebService/">'));
        kms.add(new Kypera_Metadata__c(Name = 'createPersonEnd', Metadata__c = '</CreatePerson>'));
        kms.add(new Kypera_Metadata__c(Name = 'soapBodyBegin', Metadata__c = '<soap:Body>'));
        kms.add(new Kypera_Metadata__c(Name = 'soapBodyEnd', Metadata__c = '</soap:Body>'));
        kms.add(new Kypera_Metadata__c(Name = 'soapEnvelopeBegin', Metadata__c = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'));
        kms.add(new Kypera_Metadata__c(Name = 'soapEnvelopeEnd', Metadata__c = '</soap:Envelope>'));
        kms.add(new Kypera_Metadata__c(Name = 'xmlTag', Metadata__c = '<?xml version="1.0" encoding="utf-8"?>'));
        kms.add(new Kypera_Metadata__c(Name = 'ServiceHandler',  Metadata__c = 'http://mobile.kypera.com/InformWebService/ServiceHandler.asmx'));
        kms.add(new Kypera_Metadata__c(Name = 'CreatePersonEndPoint',  Metadata__c = 'http://mobile.kypera.com/InformWebService/CreatePerson'));
        kms.add(new Kypera_Metadata__c(Name = 'UpdatePersonEndPoint',  Metadata__c = 'http://mobile.kypera.com/InformWebService/UpdatePerson'));
        kms.add(new Kypera_Metadata__c(Name = 'updatePersonBegin',  Metadata__c = '<UpdatePerson xmlns="http://mobile.kypera.com/InformWebService/">'));
        kms.add(new Kypera_Metadata__c(Name = 'updatePersonEnd',  Metadata__c = '</UpdatePerson>'));
        insert kms;
        // put these in a map for easy retrieval later on
        Map<String, String> kyperaMetaData = new Map<String, String>();
        for (Kypera_Metadata__c kmsMapItem: kms) {
            kyperaMetaData.put(kmsMapItem.Name, kmsMapItem.Metadata__c);
        }        

        KyperaProcessor.ResponseStub = '<CreatePersonResponse xmlns="http://mobile.kypera.com/InformWebService/"><CreatePersonResult>&lt;?xml version=\'1.0\'?&gt;&lt;Kypera&gt;&lt;Successful&gt;&lt;![CDATA[True]]&gt;&lt;/Successful&gt;&lt;KyperaReference&gt;&lt;![CDATA[XXXXX]]&gt;&lt;/KyperaReference&gt;&lt;/Kypera&gt;</CreatePersonResult></CreatePersonResponse>';


        // Create an Account
        Account acc = new Account(Name = 'Test Account 1', RecordTypeId = accTypeMap.get('Project'));
        insert acc;
                                
        // 2. Test 
        
        // Create a Client with the correct Record Type
        Contact con = new Contact();
        con.put('FirstName',  'Test');
        con.put('LastName', 'Contact 1');
        con.put('infrm__Gender__c', 'Male');
        con.put('Salutation','Mr');
        con.put('RecordTypeId', conTypeMap.get('Client'));
        con.put('infrm__NI_number_known__c', 'No');
        con.put('infrm__Client_Group_P__c', 'Rough sleeper');
        con.put('infrm__Sexuality__c', 'Does not wish to disclose');
        con.put('infrm__ex_Armed_forces_personnel_SP__c', 'No');
        con.put('infrm__Source_of_income__c', 'Don\'t know');
        con.put('infrm__Qualify_for_HB__c', 'don\'t know');
        con.put('infrm__Immigration_Status__c', 'Not known');
        insert con;   
        
        infrm__timelineevents__c t1 = new infrm__timelineevents__c();
        t1.put('infrm__Client__c', con.Id);
        t1.put('RecordTypeId', tetTypeMap.get('ORGANISATION LEAD PROJECT'));
        t1.put('infrm__referral_date__c', Date.newInstance(2015, 1, 1));
        t1.put('infrm__status__c', 'current');
        t1.put('infrm__start_date__c', Date.newInstance(2015, 1, 1));
        t1.put('infrm__Lead_Project2__c', acc.Id);
        t1.put('infrm__source_of_referral_CH__c', 'Referral Agency');
        t1.put('In_EET__c', 'Yes');
        t1.put('infrm__referral_processed_date__c', Date.newInstance(2015, 1, 1));
        t1.put('infrm__referral_decision__c', 'referral accepted');
        insert t1;
        
        Kypera_Tenancy__c kt1 = new Kypera_Tenancy__c();
        kt1.put('Client__c', con.Id);
        kt1.put('Timeline_Event__c', t1.Id);
        kt1.put('Kypera_Start_Date__c', '2015-01-01');
        kt1.put('Kypera_End_Date__c', '2015-01-15');
        kt1.put('Unit_Reference__c', 'TEST 1');
        insert kt1;
        
        Kypera_Tenancy__c kt2 = new Kypera_Tenancy__c();
        kt2.put('Client__c', con.Id);
        kt2.put('Timeline_Event__c', t1.Id);
        kt2.put('Kypera_Start_Date__c', '2015-01-16');
        kt2.put('Unit_Reference__c', 'TEST 2');     
        insert kt2;

        /*
        You cannot save a Timeline Event with a Start Date if:
        a.  The Timeline Event has related Tenancies 
        AND
        b.  The Start Date on the Timeline event does not match the Start Date of the first Tenancy.
        */

        try {
            t1.infrm__start_date__c = Date.newInstance(2015, 1, 2);
            update t1;
        } catch(Exception e) {
            System.assert(e.getMessage().contains(Label.Kypera_Timeline_Date_Validation_Msg_3), true);
        }

        /*
        You cannot save a Timeline Event with an End Date if
        a.  The Timeline Event has related Tenancies 
        AND
        b.  The related Tenancies do not have an End Date
        AND
        */
       
        try {
            t1.infrm__start_date__c = Date.newInstance(2015, 1, 1);
            t1.infrm__end_date__c = Date.newInstance(2015, 2, 28);
            t1.infrm__Departure_Destination__c = 'Internal Project move';
            t1.infrm__internal_project__c = 'Ashmore Road';
            t1.Compelled_to_leave_by_police_bailiff_sta__c = 'No';
            t1.Departure_Type__c = 'Abandoned';
            t1.Left_during_Notice_or_court_proceedings__c = 'No';
            t1.MoveOn_in_EET_at_point_of_leaving__c = 'Yes';
            update t1;
        } catch(Exception e) {
            System.debug('*** ' + e.getMessage());
            System.assert(e.getMessage().contains(Label.Kypera_Timeline_Date_Validation_Msg_1), true);
        
        }

        /*
        You cannot save a Timeline Event with an End Date if:
        a.  The Timeline Event has related Tenancies 
        AND 
        b.  The End Date on the Timeline Event does not match the End Date of the last Tenancy
        */

        kt2.Kypera_End_Date__c = '2015-02-27';
        update kt2;

        try {
            t1.infrm__end_date__c = Date.newInstance(2015, 2, 28);
            t1.infrm__Departure_Destination__c = 'Internal Project move';
            t1.infrm__internal_project__c = 'Ashmore Road';
            t1.Compelled_to_leave_by_police_bailiff_sta__c = 'No';
            t1.Departure_Type__c = 'Abandoned';
            t1.Left_during_Notice_or_court_proceedings__c = 'No';
            t1.MoveOn_in_EET_at_point_of_leaving__c = 'Yes';
            update t1;
        } catch(Exception e) {
            System.debug('*** ' + e.getMessage());
            System.assert(e.getMessage().contains(Label.Kypera_Timeline_Date_Validation_Msg_2), true);
        
        }
        
        // test success - matching end dates on the second tenancy and the timeline event means the record is saved
        kt2.Kypera_End_Date__c = '2015-02-28';
        update kt2;
        kt2 = [SELECT Id, End_Date__c FROM Kypera_Tenancy__c WHERE Kypera_End_Date__c =: '2015-02-28' AND Client__c =: con.Id LIMIT 1];

        try {
            t1.infrm__end_date__c = Date.newInstance(2015, 2, 28);
            t1.infrm__status__c = 'ex';
            t1.infrm__Departure_Destination__c = 'Internal Project move';
            t1.infrm__internal_project__c = 'Ashmore Road';
            t1.Compelled_to_leave_by_police_bailiff_sta__c = 'No';
            t1.Departure_Type__c = 'Abandoned';
            t1.Left_during_Notice_or_court_proceedings__c = 'No';
            t1.MoveOn_in_EET_at_point_of_leaving__c = 'Yes';
            update t1;
            System.AssertEquals(t1.infrm__end_date__c, kt2.End_Date__c);
        } catch(Exception e) {
            System.debug('*** ' + e.getMessage());
        }

        // test bulk updates don't get picked up by the validation
        infrm__timelineevents__c t2 = new infrm__timelineevents__c();
        t2.put('infrm__Client__c', con.Id);
        t2.put('RecordTypeId', tetTypeMap.get('PROJECT 2'));
        t2.put('infrm__referral_date__c', Date.newInstance(2015, 2, 1));
        t2.put('infrm__status__c', 'current');
        t2.put('infrm__start_date__c', Date.newInstance(2015, 2, 1));
        t2.put('infrm__Lead_Project2__c', acc.Id);
        t2.put('infrm__source_of_referral_CH__c', 'Referral Agency');
        t2.put('In_EET__c', 'Yes');
        t2.put('infrm__referral_processed_date__c', Date.newInstance(2015, 2, 1));
        t2.put('infrm__referral_decision__c', 'referral accepted');
        insert t2;
        
        List<infrm__timelineevents__c> tes = new List<infrm__timelineevents__c>();
        tes.add(t1);
        tes.add(t2);
        try {
            update tes;
        } catch (Exception e) {
            System.debug(e.getMessage());
        }
        
        Test.StopTest();
    
    }
    
}