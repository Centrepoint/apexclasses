global class KyperaPropertyToBedspaceScheduledUpdate implements Schedulable {

   global void execute(SchedulableContext SC) {
        KyperaPropertyToBedspaceBatchProcessor batchApex = new KyperaPropertyToBedspaceBatchProcessor();
        ID batchprocessid = Database.executeBatch(batchApex);
   }
   
}