public class outcomeStarController {
    private ApexPages.StandardController controller;
    Map<String,String> spRecordTypes = new Map<String,String>();

    private Map<Id, Outcome_Star__c> addedPlans = new Map<Id, Outcome_Star__c>();
    private Map<Id, Outcome_Star__c> datesForFilter = new Map<Id, Outcome_Star__c>();

    public outcomeStarController (ApexPages.StandardController controller){
        this.controller = controller;
        //Get record types for star
        List<RecordType> rtypes = [Select Name, Id From RecordType
            where sObjectType='infrm__Outcome_Star__c' and isActive=true];
        //Create a map between the Record Type Name and Id for easy retrieval
        for(RecordType rt: rtypes)spRecordTypes.put(rt.Name,rt.Id);
        clientId = string.valueof(getOutcomeStar().Get('Client__c'));
        // the following line needs to be un-commented out if the Outcome Star has record types added to it        
        //recType = string.valueof(getOutcomeStar().Get('RecordTypeId'));
        dateOfPlan = ApexPages.currentPage().getParameters().get('id');
        datesForFilter = GetSupportedOutcomeStars();
        DefaultStarData(datesForFilter);

        chartType = 'STAR';
        refresh=false;
        getPlanData();
    }

    public string clientId;
    public id recType;
    public string dateOfPlan {get{return dateOfPlan;} set{dateOfPlan = value;}}
    public string removePlan {public get; public set;}
    public string chartType {get{return chartType;} set{chartType = value;}}
    public boolean refresh {get{return refresh;} set{refresh = value;}}
    public string starData {get; set;}

    public string propPlanData {get; set;}

    public PageReference action(){
        System.debug('Rerun getPlanData with a plan date of ' + dateOfPlan);
        System.debug('Refresh = ' + refresh);
        //System.debug('Chart type = ' + chartType);
        refresh=true;//'true';
        getPlanData();
        System.debug('Refresh = ' + refresh);
        System.debug('Date Plan Created = ' + dateOfPlan);
        return null;
    }

    public sObject getOutcomeStar() {
        string outcomeStarId;
        sObject queryResult;

        //Need a way of accessing this support plan record without having this id if we want this page to be able to be
        //dropped onto any page
        outcomeStarId = ApexPages.currentPage().getParameters().get('id');
        System.debug('###outcomeStarId = ' + outcomeStarId);
        // swap the following line for the one after if record types are added to the outcome star
        //String qryString = 'Select Id, Client__c, RecordTypeId from Outcome_Star__c where Id = ' + '\'' + outcomeStarId + '\'';
        String qryString = 'Select Id, Client__c from Outcome_Star__c where Id = ' + '\'' + outcomeStarId + '\'';
        // execute the query
        queryResult = Database.query(qryString) ;
        return queryResult;
    }

    
    public list<SelectOption> getFilterDates(){
        list<SelectOption> options = new list<SelectOption>();
        for(Outcome_Star__c p : datesForFilter.values()){
            options.add(new SelectOption(p.Id, formatDate(p.Star_Date_Derived__c)));
        }
        return options;
    }

    
    private Map<Id, Outcome_Star__c> GetSupportedOutcomeStars(){
        return new Map<Id, Outcome_Star__c>([Select Id, X2_SELF_CARE_LIVING_SKILLS_staff__c, X3_MANAGING_MONEY_staff__c,
            X4_SOCIAL_NETWORKS_RELATIONSHIPS_staff__c, X5_SCORE5_DRUG_ALCOHOL_MISUSE_staff__c,
            X6_PHYSICAL_HEALTH_staff__c, X7_EMOTIONAL_MENTAL_HEALTH_staff__c,
            X8_MEANINGFUL_USE_TIME_staff__c, X9_MANAGING_TENANCY_ACCOMMODATION_staff__c,
            X10_OFFENDING_staff__c, X1_MOTIVATION_TAKING_RESPONSIBILITY_staf__c,
            X1_MOTIVATION_TAKING_RESPONSIBILITY_su__c, X2_SELF_CARE_LIVING_SKILLS_su__c,
            X3_MANAGING_MONEY_AND_PERSONAL_ADMIN_su__c, X4_SOCIAL_NETWORKS_RELATIONSHIPS_su__c,
            X5_DRUG_ALCOHOL_MISUSE_su__c, X6_PHYSICAL_HEALTH_su__c,
            X7_EMOTIONAL_MENTAL_HEALTH_su__c, X8_MEANINGFUL_USE_OF_TIME_su__c,
            X9_MANAGING_TENANCY_ACCOMMODATION_su__c, X10_OFFENDING_su__c, Star_Date_Derived__c from Outcome_Star__c
            Where Client__c = :clientId 
            // the following line needs to be un-commented out if the Outcome Star has record types added to it
            // and RecordTypeId = :recType
            Order by Star_Date_Derived__c desc
        ]);
    }

    
    public List<SelectOption> GetAddedPlans(){
        List<SelectOption> options = new list<SelectOption>();
        for(Outcome_Star__c p : addedPlans.values()){
            options.add(new SelectOption(p.Id, formatDate(p.Star_Date_Derived__c)));
        }
        return options;
    }

    public list<SelectOption> getChartTypes(){
        list<SelectOption> types = new list<SelectOption>();
        types.add(new SelectOption('STAR','Star Chart'));
        types.add(new SelectOption('BAR','Vertical Bar Chart'));
        types.add(new SelectOption('VERT','Horizontal Bar Chart'));
        return types;
    }

    
    public PageReference AddPlan(){
        String planId = dateOfPlan;
        Outcome_Star__c planToAdd = datesForFilter.get(planId);
        if(planToAdd != null && !addedPlans.containsKey(planToAdd.Id)){
            addedPlans.put(planToAdd.Id, planToAdd);
        }
        refresh=true;
        getPlanData();
        return null;
    }

    
    public PageReference RemovePlan(){
        if(addedPlans.size() > 1){
            String planId = removePlan;
            if(planId != null && planId.length() >= 10){
                Outcome_Star__c planToRemove = addedPlans.get(planId);
                if(planToRemove != null){
                    addedPlans.remove(planToRemove.Id);
                }
            }
        }else{
            addedPlans.clear();
        }
        refresh=true;
        getPlanData();
        return null;
    }

    
    public Integer GetPlanSize(){
        return addedPlans.size();
    }

    public Void getPlanData() {
        GoogleViz gv = new GoogleViz();

        gv.cols = new list<GoogleViz.col> {
            new GoogleViz.Col('col1','Element','string')
        };

        Integer i = 1;
        for(Outcome_Star__c a : addedPlans.values()){
            i++;
            gv.cols.add(new GoogleViz.Col('col'+String.valueOf(i),formatDate(a.Star_Date_Derived__c),'number'));
        }

        List<GoogleViz.row> rows = new List<GoogleViz.row>();
        for(Integer a = 0; a < 10; a++){
            GoogleViz.row row = new GoogleViz.row();
            rows.add(row);
        }

        rows[0].cells.add(new GoogleViz.cell('motivation'));
        rows[1].cells.add(new GoogleViz.cell('self care'));
        rows[2].cells.add(new GoogleViz.cell('money'));
        rows[3].cells.add(new GoogleViz.cell('networks'));
        rows[4].cells.add(new GoogleViz.cell('substance use'));
        rows[5].cells.add(new GoogleViz.cell('physical'));
        rows[6].cells.add(new GoogleViz.cell('mental wellbeing'));
        rows[7].cells.add(new GoogleViz.cell('use of time'));
        rows[8].cells.add(new GoogleViz.cell('tenancy'));
        rows[9].cells.add(new GoogleViz.cell('offending'));

        for(Outcome_Star__c a : addedPlans.values()){
            rows[0].cells.add ( new GoogleViz.cell(toInteger(a.X1_MOTIVATION_TAKING_RESPONSIBILITY_staf__c)));
            rows[1].cells.add ( new GoogleViz.cell(toInteger(a.X2_SELF_CARE_LIVING_SKILLS_staff__c)));
            rows[2].cells.add ( new GoogleViz.cell(toInteger(a.X3_MANAGING_MONEY_staff__c)));
            rows[3].cells.add ( new GoogleViz.cell(toInteger(a.X4_SOCIAL_NETWORKS_RELATIONSHIPS_staff__c)));
            rows[4].cells.add ( new GoogleViz.cell(toInteger(a.X5_SCORE5_DRUG_ALCOHOL_MISUSE_staff__c)));
            rows[5].cells.add ( new GoogleViz.cell(toInteger(a.X6_PHYSICAL_HEALTH_staff__c)));
            rows[6].cells.add ( new GoogleViz.cell(toInteger(a.X7_EMOTIONAL_MENTAL_HEALTH_staff__c)));
            rows[7].cells.add ( new GoogleViz.cell(toInteger(a.X8_MEANINGFUL_USE_TIME_staff__c)));
            rows[8].cells.add ( new GoogleViz.cell(toInteger(a.X9_MANAGING_TENANCY_ACCOMMODATION_staff__c)));
            rows[9].cells.add ( new GoogleViz.cell(toInteger(a.X10_OFFENDING_staff__c)));
        }

        for(GoogleViz.row row : rows){
            gv.addRow( row );
        }

        starData = GetStarData(addedPlans.values());
        system.debug('### STAR DATA ### = ' + starData);
        propPlanData = gv.toJsonString();
        //return gv.toJsonString();
    }

    
    private String GetValidStarData(List<Outcome_Star__c> plans, String field){
        String data = '';
        Integer i = 0;
        for(Outcome_Star__c a : plans){
            i++;
            if(i == plans.size())data += toInteger(String.valueOf(a.get(field)));
            else data += toInteger(String.valueOf(a.get(field))) + ',';
        }
        return data;
    }

    
    private String GetStarData(List<Outcome_Star__c> plans){
        String data = '[';
            data += '[';
                data += GetValidStarData(plans, 'X1_MOTIVATION_TAKING_RESPONSIBILITY_staf__c');
            data += '],';
            data += '[';
                data += GetValidStarData(plans, 'X2_SELF_CARE_LIVING_SKILLS_staff__c');
            data += '],';
            data += '[';
                data += GetValidStarData(plans, 'X3_MANAGING_MONEY_staff__c');
            data += '],';
            data += '[';
                data += GetValidStarData(plans, 'X4_SOCIAL_NETWORKS_RELATIONSHIPS_staff__c');
            data += '],';
            data += '[';
                data += GetValidStarData(plans, 'X5_SCORE5_DRUG_ALCOHOL_MISUSE_staff__c');
            data += '],';
            data += '[';
                data += GetValidStarData(plans, 'X6_PHYSICAL_HEALTH_staff__c');
            data += '],';
            data += '[';
                data += GetValidStarData(plans, 'X7_EMOTIONAL_MENTAL_HEALTH_staff__c');
            data += '],';
            data += '[';
                data += GetValidStarData(plans, 'X8_MEANINGFUL_USE_TIME_staff__c');
            data += '],';
            data += '[';
                data += GetValidStarData(plans, 'X9_MANAGING_TENANCY_ACCOMMODATION_staff__c');
            data += '],';
            data += '[';
                data += GetValidStarData(plans, 'X10_OFFENDING_staff__c');
            data += '],';
            data += '[';
                data += GetValidStarData(plans, 'X1_MOTIVATION_TAKING_RESPONSIBILITY_staf__c');
            data += ']';
        data += ']';
        return data;
    }

    
    public void DefaultStarData(Map<Id, Outcome_Star__c> filteredPlans){
        if(!filteredPlans.isEmpty()){
            String planId = ApexPages.currentPage().getParameters().get('id');
            Outcome_Star__c current = filteredPlans.get(planId);
            addedPlans.put(current.Id, current);
            for(Outcome_Star__c a : filteredPlans.values()){
                if(a != current){
                    addedPlans.put(a.Id, a);
                    break;
                }
            }
        }
    }

    
    public String GetValidDate(DateTime a){
        return String.ValueOf(a.day()) + '/' + String.ValueOf(a.month()) + '/' + String.ValueOf(a.year());
    }

    
    public String GetStarDates(){
        String data = '';
        Integer i = 0;
        for(Outcome_Star__c a : addedPlans.values()){
            i++;
            if(i == datesForFilter.size())data += GetValidDate(a.Star_Date_Derived__c);
            else data += GetValidDate(a.Star_Date_Derived__c) + '|';
        }
        String step = (data.endsWith('|') ? data.substring(0, data.lastIndexOf('|')) : data);
        return step;
    }

    Private integer toInteger(string stringToConvert){
        integer converted;
        Try {
            if (dateOfPlan <> null){
                system.debug('### string to convert = ' + stringToConvert);
                if (stringToConvert.length() > 1 && stringToConvert.substring(0,2) == '10')
                    converted = integer.valueOf(stringToConvert.substring(0,2));
                else{
                    converted = integer.valueOf(stringToConvert.substring(0,1));
                system.debug('### converted = ' + converted);
                }
            } else {
                converted = 0;
            }
        }
        Catch (Exception e) {
            converted = 0;
        }
        system.debug('### converted = ' + converted);
        return converted;
    }

    public string formatDate(DateTime inputDate){
    If (inputDate != null){
        return (string.valueOf(inputDate.format('dd/MM/yyyy')));
      } else
        return '01/01/2009';
    }
}