global class TriggerSettings 
{

    public static Boolean IsDisableAllTriggers(){
        Trigger_Settings__c hLevel = Trigger_Settings__c.getInstance(UserInfo.getProfileId());
        System.debug('### Settings Found ###: ' + hLevel);
        return (hLevel != null ? hLevel.Disable_all_Triggers__c : false);
    }
    
    public static Boolean IsSyncWithKypera() {

        Trigger_Settings__c hLevel = Trigger_Settings__c.getInstance(UserInfo.getProfileId());
        System.debug('### Settings Found ###: ' + hLevel);
        return (hLevel != null ? hLevel.Sync_with_Kypera__c : false);
    
    }

}