public class RecursiveTriggerHandler{
    // This recursive trigger handler class can be called by any trigger to ensure that the trigger only fires once.
    public static Boolean isFirstTime = true;
}