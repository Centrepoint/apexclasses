global class KyperaBedspaceDataProcessor implements Database.Batchable<sObject>,   Database.AllowsCallouts {
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // Get the custom settings that hold the 'batch record number', i.e. how far along the batch has come. Used in the OFFSET clause.
        Integer batchRecordNumber = integer.valueOf([SELECT MetaData__c FROM Kypera_Metadata__c WHERE Name =: 'BatchRecordNumber' LIMIT 1].get(0).MetaData__c);
        // Gets the number of records to process. Used in the LIMIT clause.
        Integer numberOfRecordsToProcess = integer.valueOf([SELECT MetaData__c FROM Kypera_Metadata__c WHERE Name =: 'NumberOfRecordsToProcess' LIMIT 1].get(0).MetaData__c);
           
        // for every Kypera Property between the LIMIT (numberOfRecordsToProcess) and the OFFSET (batchRecordNumber).
        return Database.getQueryLocator([SELECT Id, Name
                          FROM Kypera_Scheme_Codes__c 
                          WHERE batchRecordNumber__c >=: batchRecordNumber 
                          ORDER BY Name 
                          LIMIT : numberOfRecordsToProcess
                          ]);
    }
    
    // Calls the Kypera webservice then calls KyperaBedspaceDataProcessorParser which parses the data
    // Processes x schemes at a time depending on the value in the custom setting
    global void execute(Database.BatchableContext BC, List<Kypera_Scheme_Codes__c> records) {
        
        // set up a list based on a custom inner class (see below, at the end of this class)   
        List<xmlToParse> rawData = new List<xmlToParse>();  
        // metadata
        Map<String, Kypera_Metadata__c> metadataCustomSetting = new Map<String, Kypera_Metadata__c>();
        // scheme codes
        String schemeCodes = '';
        metadataCustomSetting = Kypera_Metadata__c.getAll();      
        String xmlMessage = '';
        // Get the Batch Record Number from the custom setting
        Integer batchRecordNumber = integer.valueOf([SELECT MetaData__c FROM Kypera_Metadata__c WHERE Name =: 'BatchRecordNumber' LIMIT 1].get(0).MetaData__c);
        // Get the number of records to process from the custom setting; this limits the code to 10 callouts
        Integer numberOfRecordsToProcess = integer.valueOf([SELECT MetaData__c FROM Kypera_Metadata__c WHERE Name =: 'NumberOfRecordsToProcess' LIMIT 1].get(0).MetaData__c);        
        for (Kypera_Scheme_Codes__c k : records) {
            xmlMessage = constructXML(k.Name, metadataCustomSetting);
            // make the callout and add the returned data to the list of records to be updated
            getBedspacesFromKypera(xmlMessage, k.Name, metadataCustomSetting, rawData);
            // increment the batch record number
            BatchRecordNumber ++;
        }
        Kypera_Bedspace_Import_Log__c log = new Kypera_Bedspace_Import_Log__c();
        log.Batch_Start_Date__c = datetime.now();
        log.Stage__c = '001 Retrieved data from Kypera';
        log.Technical_Information__c = 'KyperaBedspaceDataProcessor.execute()';
        insert log;
        
        KyperaBedspaceDataProcessorParser kbdpp = new KyperaBedspaceDataProcessorParser(log);
        // parse the data then insert it
        for (xmlToParse xtp : rawData) {
            kbdpp.addScheme(xtp.schemeCode, xtp.xmlBody);
            schemeCodes += xtp.schemeCode + ' ';
        }
        // update Kypera_Properties based on the data returned by the web service
        kbdpp.processSchemeLevelData();  
        
        // This manages the number of records that are processed. It is needed so that we do not hit the limit of 10 callouts per class / transaction
        // If the batchrecord number is less than the number of records in the Kypera Scheme Codes table, increase it...

        Kypera_Metadata__c brn = [SELECT Name, MetaData__c FROM Kypera_Metadata__c WHERE Name =: 'BatchRecordNumber' LIMIT 1];
        Integer MaxBatchRecordNumber = [SELECT COUNT() FROM Kypera_Scheme_Codes__c ];
        if (batchRecordNumber <= MaxBatchRecordNumber) {
            system.debug('setting batch record number to ' + BatchRecordNumber);
            brn.MetaData__c = String.valueOf(BatchRecordNumber);   
        } else {
            // ... otherwise, set it to 1
            brn.MetaData__c = '1';
            // ... and update the services
            log.Stage__c = '005 Updating Services';
            try {
                KyperaPropertyToService kpts = new KyperaPropertyToService();
                kpts.doUpdate();
                update log;
            } catch(Exception e) {
                log.Exception__c = e.getMessage();
                update log;
            }
            // ... and update the bedspaces
            log.Stage__c = '006 Updating Bedspaces';
            try {
                KyperaPropertyToBedspace kptd = new KyperaPropertyToBedspace();
                kptd.doUpdate();
                update log;
            } catch(Exception e) {
                log.Exception__c = e.getMessage();
                update log;
            }
        }
        update brn;
        log.Stage__c = '007 Completed';
        log.Technical_Information__c = 'KyperaBedspaceDataProcessor.processSchemeLevelData() exiting. Attempted Scheme Codes = ' + schemeCodes +'. Scheme codes not found = ' + kbdpp.schemeCodeFailures;
        update log;
    }   

    // Once a batch run is completed, this re-schedules the batch
    global void finish(Database.BatchableContext BC){
        // Get the Batch Record Number from the custom setting
        Integer batchRecordNumber = integer.valueOf([SELECT MetaData__c FROM Kypera_Metadata__c WHERE Name =: 'BatchRecordNumber' LIMIT 1].get(0).MetaData__c);
        // Get the number of records to process from the custom setting; this limits the code to 10 callouts
        Integer frequency = integer.valueOf([SELECT MetaData__c FROM Kypera_Metadata__c WHERE Name =: 'Frequency' LIMIT 1].get(0).MetaData__c);        
        // If processing is complete schedule it to run again at 3 a.m.
        if (batchRecordNumber == 1) {
            if (!Test.isRunningTest()) {
               System.Schedule('KyperaBedspaceDataScheduledImport', '0 0 3 * * ?', new KyperaBedspaceDataScheduledImport());
            }
        } else {
        // If processing is not complete, schedule it to run again in the number of minutes specified in the custom setting
            if (!Test.isRunningTest()) {
               System.scheduleBatch(new KyperaBedspaceDataProcessor(), 'KyperaBedspaceDataProcessor', frequency);
            }        
        }
    }

    // Make the callout
    public static void getBedspacesFromKypera(String xmlToSend, String schemeCode, Map<String, Kypera_Metadata__c> metadata, List<xmlToParse> dataList) {
        String xmlRequest = xmlToSend;
        HttpRequest req = new HttpRequest();
        HTTPResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setEndpoint(metadata.get('ServiceHandler').MetaData__c); 
        system.debug('*** metadata.get(\'ServiceHandler\').MetaData__c = ' + metadata.get('ServiceHandler').MetaData__c);
        req.setHeader('Content-Type', 'text/xml; charset=utf-8');
        req.setHeader('SOAPAction', metadata.get('GetSchemeBedSpaceDetailsEndPoint').MetaData__c); 
        system.debug('*** metadata.get(\'GetSchemeBedSpaceDetailsEndPoint\').MetaData__c = ' + metadata.get('GetSchemeBedSpaceDetailsEndPoint').MetaData__c);
        req.setBody(xmlRequest);
        system.debug('### req.getBody: '+ req.getBody());
        Http http = new Http();
        try { 
            if (!Test.isRunningTest()){ 
                // make the callout
                res = http.send(req); 
                System.debug('*** res.body: '+ res.getBody());
                System.debug('*** res.getStatus: '+ res.getStatus());
                System.debug('*** res.getStatusCode:'+ res.getStatusCode());   
            }
            String resBody = '';
            if (!Test.isRunningTest()){ 
                resBody = res.getBody();
            } else {
                resBody = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><GetSchemeBedSpaceDetailsResponse xmlns="http://inform.kypera.com/InformWebService/"><GetSchemeBedSpaceDetailsResult><Schemes xmlns=""><scheme><name>Aldo House</name><code>A01</code><number>5</number><street>Wilmer Drive</street><town>Bradford</town><county>Yorkshire</county><postcode>BD9 4AR</postcode><inception /><decommision /><Blocks><block><name>Aldo House</name><code>A01ALD</code><telephoneNumber1 /><telephoneNumber2 /><housingOfficer>Tyler Moore</housingOfficer><serviceManager /><SSHO1 /><SSHO2 /><areaCode>2</areaCode><Units><unit><name>Aldo House Rm 1, 2nd Floor</name><code>A01ALD01</code><corerent><housingServiceCharge><charge name="Core Rent" value="48.69" /><charge name="Cleaning of communal areas and" value="5.80" /><charge name="Council tax" value="4.02" /><charge name="Electricity" value="4.64" /><charge name="Food" value="0.00" /><charge name="Gas" value="4.03" /><charge name="Heat &amp; light" value="6.33" /><charge name="Laundry facilities" value="2.80" /><charge name="Other (including toiletries)" value="0.00" /><charge name="Renewals and replacements" value="22.53" /><charge name="Service contracts " value="10.13" /><charge name="TV Licence" value="0.00" /><charge name="Water" value="3.33" /></housingServiceCharge><personalServiceCharge><charge name="Core Rent" value="0.00" /><charge name="Cleaning of communal areas and" value="0.00" /><charge name="Council tax" value="0.00" /><charge name="Electricity" value="0.00" /><charge name="Food" value="0.00" /><charge name="Gas" value="0.00" /><charge name="Heat &amp; light" value="0.00" /><charge name="Laundry facilities" value="0.00" /><charge name="Other (including toiletries)" value="0.00" /><charge name="Renewals and replacements" value="0.00" /><charge name="Service contracts " value="0.00" /><charge name="TV Licence" value="0.00" /><charge name="Water" value="0.00" /></personalServiceCharge></corerent><propertyType></propertyType><defaultOccupancyType>Licence Agreement</defaultOccupancyType><ownerShip>Centrepoint</ownerShip><propertySubType>Hostel</propertySubType><managingAgent></managingAgent><landlordName>CENTREPOINT</landlordName></unit><unit><name>Aldo House Rm 2, 2nd Floor</name><code>A01ALD02</code><corerent><housingServiceCharge><charge name="Core Rent" value="48.69" /><charge name="Cleaning of communal areas and" value="5.80" /><charge name="Council tax" value="4.02" /><charge name="Electricity" value="4.64" /><charge name="Food" value="0.00" /><charge name="Gas" value="4.03" /><charge name="Heat &amp; light" value="6.33" /><charge name="Laundry facilities" value="2.80" /><charge name="Other (including toiletries)" value="0.00" /><charge name="Renewals and replacements" value="22.53" /><charge name="Service contracts " value="10.13" /><charge name="TV Licence" value="0.00" /><charge name="Water" value="3.33" /></housingServiceCharge><personalServiceCharge><charge name="Core Rent" value="0.00" /><charge name="Cleaning of communal areas and" value="0.00" /><charge name="Council tax" value="0.00" /><charge name="Electricity" value="0.00" /><charge name="Food" value="0.00" /><charge name="Gas" value="0.00" /><charge name="Heat &amp; light" value="0.00" /><charge name="Laundry facilities" value="0.00" /><charge name="Other (including toiletries)" value="0.00" /><charge name="Renewals and replacements" value="0.00" /><charge name="Service contracts " value="0.00" /><charge name="TV Licence" value="0.00" /><charge name="Water" value="0.00" /></personalServiceCharge></corerent><propertyType></propertyType><defaultOccupancyType>Licence Agreement</defaultOccupancyType><ownerShip>Centrepoint</ownerShip><propertySubType>Hostel</propertySubType><managingAgent></managingAgent><landlordName>CENTREPOINT</landlordName></unit><unit><name>Aldo House Rm 3, 1st Floor</name><code>A01ALD03</code><corerent><housingServiceCharge><charge name="Core Rent" value="48.69" /><charge name="Cleaning of communal areas and" value="5.80" /><charge name="Council tax" value="4.02" /><charge name="Electricity" value="4.13" /><charge name="Food" value="0.00" /><charge name="Gas" value="3.54" /><charge name="Heat &amp; light" value="4.80" /><charge name="Laundry facilities" value="2.80" /><charge name="Other (including toiletries)" value="0.00" /><charge name="Renewals and replacements" value="22.53" /><charge name="Service contracts " value="10.13" /><charge name="TV Licence" value="0.00" /><charge name="Water" value="3.33" /></housingServiceCharge><personalServiceCharge><charge name="Core Rent" value="0.00" /><charge name="Cleaning of communal areas and" value="0.00" /><charge name="Council tax" value="0.00" /><charge name="Electricity" value="0.00" /><charge name="Food" value="0.00" /><charge name="Gas" value="0.00" /><charge name="Heat &amp; light" value="0.00" /><charge name="Laundry facilities" value="0.00" /><charge name="Other (including toiletries)" value="0.00" /><charge name="Renewals and replacements" value="0.00" /><charge name="Service contracts " value="0.00" /><charge name="TV Licence" value="0.00" /><charge name="Water" value="0.00" /></personalServiceCharge></corerent><propertyType></propertyType><defaultOccupancyType>Licence Agreement</defaultOccupancyType><ownerShip>Centrepoint</ownerShip><propertySubType>Hostel</propertySubType><managingAgent></managingAgent><landlordName>CENTREPOINT</landlordName></unit><unit><name>Aldo House Rm 4, 1st Floor</name><code>A01ALD04</code><corerent><housingServiceCharge><charge name="Core Rent" value="48.69" /><charge name="Cleaning of communal areas and" value="5.80" /><charge name="Council tax" value="4.02" /><charge name="Electricity" value="4.64" /><charge name="Food" value="0.00" /><charge name="Gas" value="4.03" /><charge name="Heat &amp; light" value="6.33" /><charge name="Laundry facilities" value="2.80" /><charge name="Other (including toiletries)" value="0.00" /><charge name="Renewals and replacements" value="22.53" /><charge name="Service contracts " value="10.13" /><charge name="TV Licence" value="0.00" /><charge name="Water" value="3.33" /></housingServiceCharge><personalServiceCharge><charge name="Core Rent" value="0.00" /><charge name="Cleaning of communal areas and" value="0.00" /><charge name="Council tax" value="0.00" /><charge name="Electricity" value="0.00" /><charge name="Food" value="0.00" /><charge name="Gas" value="0.00" /><charge name="Heat &amp; light" value="0.00" /><charge name="Laundry facilities" value="0.00" /><charge name="Other (including toiletries)" value="0.00" /><charge name="Renewals and replacements" value="0.00" /><charge name="Service contracts " value="0.00" /><charge name="TV Licence" value="0.00" /><charge name="Water" value="0.00" /></personalServiceCharge></corerent><propertyType></propertyType><defaultOccupancyType>Licence Agreement</defaultOccupancyType><ownerShip>Centrepoint</ownerShip><propertySubType>Hostel</propertySubType><managingAgent></managingAgent><landlordName>CENTREPOINT</landlordName></unit><unit><name>Aldo House Rm 5, 1st Floor</name><code>A01ALD05</code><corerent><housingServiceCharge><charge name="Core Rent" value="48.69" /><charge name="Cleaning of communal areas and" value="5.80" /><charge name="Council tax" value="4.02" /><charge name="Electricity" value="4.64" /><charge name="Food" value="0.00" /><charge name="Gas" value="4.03" /><charge name="Heat &amp; light" value="6.33" /><charge name="Laundry facilities" value="2.80" /><charge name="Other (including toiletries)" value="0.00" /><charge name="Renewals and replacements" value="22.53" /><charge name="Service contracts " value="10.13" /><charge name="TV Licence" value="0.00" /><charge name="Water" value="3.33" /></housingServiceCharge><personalServiceCharge><charge name="Core Rent" value="0.00" /><charge name="Cleaning of communal areas and" value="0.00" /><charge name="Council tax" value="0.00" /><charge name="Electricity" value="0.00" /><charge name="Food" value="0.00" /><charge name="Gas" value="0.00" /><charge name="Heat &amp; light" value="0.00" /><charge name="Laundry facilities" value="0.00" /><charge name="Other (including toiletries)" value="0.00" /><charge name="Renewals and replacements" value="0.00" /><charge name="Service contracts " value="0.00" /><charge name="TV Licence" value="0.00" /><charge name="Water" value="0.00" /></personalServiceCharge></corerent><propertyType></propertyType><defaultOccupancyType>Licence Agreement</defaultOccupancyType><ownerShip>Centrepoint</ownerShip><propertySubType>Hostel</propertySubType><managingAgent></managingAgent><landlordName>CENTREPOINT</landlordName></unit><unit><name>Aldo House Rm 6, 1st Floor</name><code>A01ALD06</code><corerent><housingServiceCharge><charge name="Core Rent" value="48.69" /><charge name="Cleaning of communal areas and" value="5.80" /><charge name="Council tax" value="4.02" /><charge name="Electricity" value="4.64" /><charge name="Food" value="0.00" /><charge name="Gas" value="4.03" /><charge name="Heat &amp; light" value="6.33" /><charge name="Laundry facilities" value="2.80" /><charge name="Other (including toiletries)" value="0.00" /><charge name="Renewals and replacements" value="22.53" /><charge name="Service contracts " value="10.13" /><charge name="TV Licence" value="0.00" /><charge name="Water" value="3.33" /></housingServiceCharge><personalServiceCharge><charge name="Core Rent" value="0.00" /><charge name="Cleaning of communal areas and" value="0.00" /><charge name="Council tax" value="0.00" /><charge name="Electricity" value="0.00" /><charge name="Food" value="0.00" /><charge name="Gas" value="0.00" /><charge name="Heat &amp; light" value="0.00" /><charge name="Laundry facilities" value="0.00" /><charge name="Other (including toiletries)" value="0.00" /><charge name="Renewals and replacements" value="0.00" /><charge name="Service contracts " value="0.00" /><charge name="TV Licence" value="0.00" /><charge name="Water" value="0.00" /></personalServiceCharge></corerent><propertyType></propertyType><defaultOccupancyType>Licence Agreement</defaultOccupancyType><ownerShip>Centrepoint</ownerShip><propertySubType>Hostel</propertySubType><managingAgent></managingAgent><landlordName>CENTREPOINT</landlordName></unit><unit><name>Aldo House Rm 7, Basement</name><code>A01ALD07</code><corerent><housingServiceCharge><charge name="Core Rent" value="0.00" /><charge name="Cleaning of communal areas and" value="0.00" /><charge name="Council tax" value="0.00" /><charge name="Electricity" value="0.00" /><charge name="Food" value="0.00" /><charge name="Gas" value="0.00" /><charge name="Heat &amp; light" value="0.00" /><charge name="Laundry facilities" value="0.00" /><charge name="Other (including toiletries)" value="0.00" /><charge name="Renewals and replacements" value="0.00" /><charge name="Service contracts " value="0.00" /><charge name="TV Licence" value="0.00" /><charge name="Water" value="0.00" /></housingServiceCharge><personalServiceCharge /></corerent><propertyType></propertyType><defaultOccupancyType>Licence Agreement</defaultOccupancyType><ownerShip>Centrepoint</ownerShip><propertySubType>Hostel</propertySubType><managingAgent></managingAgent><landlordName>CENTREPOINT</landlordName></unit><unit><name>Aldo House Rm 8, Basement</name><code>A01ALD08</code><corerent><housingServiceCharge><charge name="Core Rent" value="48.69" /><charge name="Cleaning of communal areas and" value="5.80" /><charge name="Council tax" value="4.02" /><charge name="Electricity" value="4.64" /><charge name="Food" value="0.00" /><charge name="Gas" value="4.03" /><charge name="Heat &amp; light" value="6.33" /><charge name="Laundry facilities" value="2.80" /><charge name="Other (including toiletries)" value="0.00" /><charge name="Renewals and replacements" value="22.53" /><charge name="Service contracts " value="10.13" /><charge name="TV Licence" value="0.00" /><charge name="Water" value="3.33" /></housingServiceCharge><personalServiceCharge><charge name="Core Rent" value="0.00" /><charge name="Cleaning of communal areas and" value="0.00" /><charge name="Council tax" value="0.00" /><charge name="Electricity" value="0.00" /><charge name="Food" value="0.00" /><charge name="Gas" value="0.00" /><charge name="Heat &amp; light" value="0.00" /><charge name="Laundry facilities" value="0.00" /><charge name="Other (including toiletries)" value="0.00" /><charge name="Renewals and replacements" value="0.00" /><charge name="Service contracts " value="0.00" /><charge name="TV Licence" value="0.00" /><charge name="Water" value="0.00" /></personalServiceCharge></corerent><propertyType></propertyType><defaultOccupancyType>Licence Agreement</defaultOccupancyType><ownerShip>Centrepoint</ownerShip><propertySubType>Hostel</propertySubType><managingAgent></managingAgent><landlordName>CENTREPOINT</landlordName></unit></Units></block></Blocks></scheme></Schemes></GetSchemeBedSpaceDetailsResult></GetSchemeBedSpaceDetailsResponse></soap:Body></soap:Envelope>';            
            }
            // add the scheme details to the list
            dataList.add(new xmlToParse(schemeCode, resBody));
        } catch(System.CalloutException e) {
            // This cannot log these errors because the callouts are made in a loop and subsequent callouts would be prevented by the pending 'commit'
            System.debug(e.GetMessage());
        }     
    }
    
    
    public string constructXML(String schemeCode, Map<String, Kypera_Metadata__c> metadata) {
        system.debug('*** entering KyperaBedspaceDataProcessor.constructXML() ');
        String request = '';
        // build the request to Kypera
        system.debug('*** about to set metadata in constructXML() with ' + metadata); 
        request += xmlStart(metadata);
        system.debug('*** set metadata in constructXML() ');
        request += metadata.get('GetSchemeBedSpaceDetailsBegin').MetaData__c;
        request += '<KyperaSchemeCode>' + schemeCode + '</KyperaSchemeCode>';
        request += metadata.get('GetSchemeBedSpaceDetailsEnd').MetaData__c;
        request += xmlEnd(metadata);
        system.debug('*** exiting KyperaBedspaceDataProcessor.constructXML() ');
        return request;
    }        
    
    
    // create the start of the outerXML 
    public string xmlStart(Map<String, Kypera_Metadata__c> metaDataList) {
        string retVal = '';
        retVal += metaDataList.get('xmlTag').MetaData__c;
        retVal += metaDataList.get('soapEnvelopeBegin').MetaData__c;
        retVal += metaDataList.get('soapBodyBegin').MetaData__c;        
        return retVal;
    }

    // create the end of the outerXML 
    public string xmlEnd(Map<String, Kypera_Metadata__c> metaDataList) {
        string retVal = '';
        retVal += metaDataList.get('soapBodyEnd').MetaData__c;        
        retVal += metaDataList.get('soapEnvelopeEnd').MetaData__c;
        return retVal;
    }

    public class xmlToParse {
        String schemeCode;
        String xmlBody;
        
        public xmlToParse (String recordName, String xmlText) {
            schemeCode = recordName;
            xmlBody = xmlText;
        }       
    }
  
}