public with sharing class KyperaTimelineTenancyValidator {

    /* adds an error to the Timeline Event if it violates the validation rules
       This validation is not performed on mass updates because data loaded in bulk 
       may not meet these rules or may not be loaded in the correct order
    */
    public static void validateTimelineDates(List<infrm__TimelineEvents__c> ts, String profileId) {
        // this validation only applies when updating the records via the UI, one at a time
        if (ts.size() != 1) {
            return;
        }
        // exit if user profile is listed as one to exclude, e.g. System Adminisrators
        if (profileIsExcludedFromValidation(profileId)) {
            return;
        }
        
        // only do the validation if the Timeline Event has related tenancies
        if (tenancyCount(ts[0].Id) > 0) {
            if (ts[0].infrm__end_date__c != null && relatedTenanciesWithNoEndDates(ts[0].Id)) {
                ts[0].addError(Label.Kypera_Timeline_Date_Validation_Msg_1);
                return;
            }
            if (nonMatchingEndDates(ts[0].Id, ts[0].infrm__end_date__c)) {
                ts[0].addError(Label.Kypera_Timeline_Date_Validation_Msg_2);
                return;
            }
            if (nonMatchingStartDates(ts[0].Id, ts[0].infrm__start_date__c)) {
                ts[0].addError(Label.Kypera_Timeline_Date_Validation_Msg_3);
                return;
            }            
        }

    }

    /*
        1. You cannot save a Timeline Event with an End Date if
        there are related Tenancies that do not have an End Date
    */
    public static boolean relatedTenanciesWithNoEndDates(Id timelineId) {
        boolean retVal = false;
        integer rows = [SELECT count() FROM Kypera_Tenancy__c WHERE Timeline_Event__c =: timelineId AND End_Date__c =: null];
        if (rows > 0) {
            retVal = true;
        }
        return retVal;
    }

    /* 
        2. You cannot save a Timeline Event with an End Date if
        the End Date on the Timeline Event does not match the 
        End Date of the last Tenancy
    */
    public static boolean nonMatchingEndDates(Id timelineId, Date timelineEndDate) {
        boolean retVal = false;
        if (timelineEndDate == null) { 
            return retVal; // exit if the timeline does not have an end date
        }
        if (timelineEndDate != lastTenancyEndDate(timelineId)) { 
            retVal = true; // there's an error if the timeline end date doesn't match the last tenancy end date
        }
        return retVal;
    }


    /* 
        3. You cannot save a Timeline Event with a Start Date if
        the Start Date on the Timeline Event does not match the 
        Start Date of the first Tenancy
    */
    public static boolean nonMatchingStartDates(Id timelineId, Date timelineStartDate) {
        boolean retVal = false;
        if (timelineStartDate == null) { 
            return retVal; // exit if the timeline does not have an start date
        }
        if (timelineStartDate != firstTenancyStartDate(timelineId)) { 
            retVal = true; // there's an error if the timeline start date doesn't match the first tenancy end date
        }
        return retVal;
    }

    // takes a Timeline Event Id and returns the last (i.e. highest or 'maximum') tenancy end date
    public static Date lastTenancyEndDate(Id timelineId) {
        Date retVal;
        AggregateResult[] groupedResults = [SELECT MAX(End_Date__c) mx FROM Kypera_Tenancy__c WHERE Timeline_Event__c =: timelineId];
        Object maxAmount = groupedResults[0].get('mx');
        retval = (Date)maxAmount;
        return retVal;
    }

    // takes a Timeline Event Id and returns the first (i.e. lowest or 'minimum') tenancy start date
    public static Date firstTenancyStartDate(Id timelineId) {
        Date retVal;
        AggregateResult[] groupedResults = [SELECT MIN(Start_Date__c) mn FROM Kypera_Tenancy__c WHERE Timeline_Event__c =: timelineId];
        Object minAmount = groupedResults[0].get('mn');
        retval = (Date)minAmount;
        return retVal;
    }

    // takes a Timeline Event Id and returns the number of tenancies
    public static integer tenancyCount(Id timelineId) {
        integer retVal = 0;
        retVal = [SELECT Id FROM Kypera_Tenancy__c WHERE Timeline_Event__c =: timelineId].size();
        return retVal;
    }

    // checks if the profile is excluded from timeline event validation
    public static boolean profileIsExcludedFromValidation(String pid) {
        boolean retVal = false;
        String ProfileName = [SELECT Name FROM Profile WHERE Id =: pid LIMIT 1].Name;
        /*
        String RestrictedProfile = [SELECT Timeline_Validation_Excluded_Profile__c FROM Trigger_Settings__c LIMIT 1].Timeline_Validation_Excluded_Profile__c;
        
        Querying the custom settings using SOQL causes error and the ActionPlanOutcomesStarPrintAsPDFTest test class to fail
        Changed the retrieval of the timeline validation exluded profile to custom settings methods to resolve the issue
        */
        String RestrictedProfile = Trigger_Settings__c.getOrgDefaults().Timeline_Validation_Excluded_Profile__c;
        if (Profilename == RestrictedProfile) {
            retVal = true;
        }
        
        return retVal;
    }
    

}