global class familyStarControllerEnhanced {
    private ApexPages.StandardController controller;
    
    private Map<Id, Family_Star__c> addedPlans = new Map<Id, Family_Star__c>();
    private Map<Id, Family_Star__c> datesForFilter = new Map<Id, Family_Star__c>();
    private integer questionCount = 8; 

    global familyStarControllerEnhanced(ApexPages.StandardController controller){
        this.controller = controller;
        // clientId = string.valueof(getFamilyStar().Get('Client__c')); // TODO: deprecated
        clientId= string.valueof(getFamilyStar().Get('Client__c'));
        dateOfPlan = ApexPages.currentPage().getParameters().get('id');
        datesForFilter = GetAllStars(); // GetSupportedPlans
        DefaultStarData(datesForFilter);
        chartType = 'STAR';
        refresh=false;
        getFormData();
    }
    
    // global string clientId; // TODO: deprecate
    global string clientId;
    global id recType;
    global string dateOfPlan {get{return dateOfPlan;} set{dateOfPlan = value;}}
    global string removePlan {global get; global set;}
    global string chartType {get{return chartType;} set{chartType = value;}}
    global boolean refresh {get{return refresh;} set{refresh = value;}}
    global string starData {get; set;}
    global string propPlanData {get; set;}
    global string AstroCye {get; set;}

    global PageReference action(){
        System.debug('Rerun getPlanData with a plan date of ' + dateOfPlan);
        System.debug('Refresh = ' + refresh);
        //System.debug('Chart type = ' + chartType);
        refresh=true;//'true';
        getFormData();
        System.debug('Refresh = ' + refresh);
        System.debug('Date Plan Created = ' + dateOfPlan);
        return null;
    }
    
    global sObject getFamilyStar() { // getSupportPlan
        string familyStarId;
        sObject queryResult;
        familyStarId = ApexPages.currentPage().getParameters().get('id');
        String qryString = 'Select Id, Client__c from Family_Star__c where Id = ' + '\'' + familyStarId + '\'';
        // execute the query
        queryResult = Database.query(qryString) ;
        return queryResult;
    }

    global list<SelectOption> getFilterDates(){
        list<SelectOption> options = new list<SelectOption>();
        for(Family_Star__c p : datesForFilter.values()){
            options.add(new SelectOption(p.Id, formatDate(p.Star_Date_Derived__c)));
        }
        return options;
    }

    private Map<Id, Family_Star__c> GetAllStars(){ // GetSupportedPlans
        return new Map<Id, Family_Star__c>([
            SELECT
                Id, 
                Promoting_good_health__c,
                Meeting_emotional_needs__c,
                Keeping_your_child_safe__c,
                Social_networks__c,
                Supporting_learning__c,
                Setting_boundaries__c,
                Keeping_a_family_routine__c, 
                Providing_home_and_money__c,
                Star_Date_Derived__c,
                Score_promoting_good_health__c,
                Score_meeting_emotional_needs__c,
                Score_keeping_your_child_safe__c,
                Score_social_networks__c,
                Score_supporting_learning__c,
                Score_setting_boundaries__c,
                Score_keeping_a_family_routine__c,
                Score_providing_home_and_money__c
            FROM
                Family_Star__c
            WHERE
                Client__c = :clientId
            ORDER BY Star_Date_Derived__c desc
        ]);
    }
 
    global List<SelectOption> GetAddedPlans(){
        List<SelectOption> options = new list<SelectOption>();
        for(Family_Star__c p : addedPlans.values()){
            options.add(new SelectOption(p.Id, formatDate(p.Star_Date_Derived__c)));
        }
        return options;
    }
    
    global list<SelectOption> getChartTypes(){
        list<SelectOption> types = new list<SelectOption>();
        types.add(new SelectOption('STAR','Star Chart'));
        types.add(new SelectOption('BAR','Vertical Bar Chart'));
        types.add(new SelectOption('VERT','Horizontal Bar Chart'));
        return types;
    }

    global PageReference AddPlan(){
        String planId = dateOfPlan;
        Family_Star__c planToAdd = datesForFilter.get(planId);
        if(planToAdd != null && !addedPlans.containsKey(planToAdd.Id)){
            addedPlans.put(planToAdd.Id, planToAdd);
        }
        refresh=true;
        getFormData();
        return null;
    }

    global PageReference RemovePlan(){
        if(addedPlans.size() > 1){
            String planId = removePlan;
            if(planId != null && planId.length() >= 10){
                Family_Star__c planToRemove = addedPlans.get(planId);
                if(planToRemove != null){
                    addedPlans.remove(planToRemove.Id);
                }
            }
        }else{
            addedPlans.clear();
        }
        refresh=true;
        getFormData();
        return null;
    }

    global Integer GetPlanSize(){
        return addedPlans.size();
    }
  
    global Void getFormData() { // getPlanData
        FamilyStarViz gv = new FamilyStarViz();

        gv.cols = new list<FamilyStarViz.col> {
            new FamilyStarViz.Col('col1','Element','string')
        };

        Integer i = 1;
        for(Family_Star__c a : addedPlans.values()){
            i++;
            gv.cols.add(new FamilyStarViz.Col('col'+String.valueOf(i),formatDate(a.Star_Date_Derived__c),'number'));
        }

        List<FamilyStarViz.row> rows = new List<FamilyStarViz.row>();
        //TODO: soft-code
        for(Integer a = 0; a < questionCount; a++){
            FamilyStarViz.row row = new FamilyStarViz.row();
            rows.add(row);
        }

        // TODO: re-factor into a for loop over a string array        

        rows[0].cells.add(new FamilyStarViz.cell('promoting good health'));
        rows[1].cells.add(new FamilyStarViz.cell('meeting emotional needs'));
        rows[2].cells.add(new FamilyStarViz.cell('keeping your child safe'));
        rows[3].cells.add(new FamilyStarViz.cell('social networks'));
        rows[4].cells.add(new FamilyStarViz.cell('supporting learning'));
        rows[5].cells.add(new FamilyStarViz.cell('setting boundaries'));
        rows[6].cells.add(new FamilyStarViz.cell('keeping a family routine'));
        rows[7].cells.add(new FamilyStarViz.cell('providing home and money'));

        for(Family_Star__c a : addedPlans.values()){
            rows[0].cells.add ( new FamilyStarViz.cell(toInteger(a.Promoting_good_health__c)));
            rows[1].cells.add ( new FamilyStarViz.cell(toInteger(a.Meeting_emotional_needs__c)));
            rows[2].cells.add ( new FamilyStarViz.cell(toInteger(a.Keeping_your_child_safe__c)));
            rows[3].cells.add ( new FamilyStarViz.cell(toInteger(a.Social_networks__c)));
            rows[4].cells.add ( new FamilyStarViz.cell(toInteger(a.Supporting_learning__c)));
            rows[5].cells.add ( new FamilyStarViz.cell(toInteger(a.Setting_boundaries__c)));
            rows[6].cells.add ( new FamilyStarViz.cell(toInteger(a.Keeping_a_family_routine__c)));
            rows[7].cells.add ( new FamilyStarViz.cell(toInteger(a.Providing_home_and_money__c)));
            system.debug('a.Promoting_good_health__c' + a.Promoting_good_health__c);

        }

        for(FamilyStarViz.row row : rows){
            gv.addRow( row );
        }

        starData = GetStarData(addedPlans.values());
        system.debug('### STAR DATA ### = ' + starData);
        propPlanData = gv.toJsonString();
    }    

    private String GetValidStarData(List<Family_Star__c> stars, String field){
        String data = '';
        Integer i = 0;
        for(Family_Star__c a : stars){
            i++;
            if(i == stars.size())data += toInteger(String.valueOf(a.get(field)));
            else data += toInteger(String.valueOf(a.get(field))) + ',';
        }
        return data;
    }

    private String GetStarData(List<Family_Star__c> stars){
        String data = '[';
            // TODO: re-factor into a for loop going over an array
            data += '[';
                data += GetValidStarData(stars, 'Promoting_good_health__c');
            data += '],';
            data += '[';
                data += GetValidStarData(stars, 'Meeting_emotional_needs__c');
            data += '],';
            data += '[';
                data += GetValidStarData(stars, 'Keeping_your_child_safe__c');
            data += '],';
            data += '[';
                data += GetValidStarData(stars, 'Social_networks__c');
            data += '],';
            data += '[';
                data += GetValidStarData(stars, 'Supporting_learning__c');
            data += '],';
            data += '[';
                data += GetValidStarData(stars, 'Setting_boundaries__c');
            data += '],';
            data += '[';
                data += GetValidStarData(stars, 'Keeping_a_family_routine__c');
            data += '],';
            data += '[';
                data += GetValidStarData(stars, 'Providing_home_and_money__c');
            data += '],';
            data += '[';
                data += GetValidStarData(stars, 'Promoting_good_health__c');
            data += ']';
        data += ']';
        system.debug('data = ' + data);
        return data;
    }
    
    global void DefaultStarData(Map<Id, Family_Star__c> filteredPlans){ // DefaultPlanData
        if(!filteredPlans.isEmpty()){
            String starId = ApexPages.currentPage().getParameters().get('id');
            Family_Star__c current = filteredPlans.get(starId);
            addedPlans.put(current.Id, current);
            for(Family_Star__c a : filteredPlans.values()){
                if(a != current){
                    addedPlans.put(a.Id, a);
                    break;
                }
            }
        }
    }

    private PageReference pageRef;
    global PageReference printSPCharted () {
        string spids;
        spids = '';
        for(Family_Star__c p : addedplans.values()){
            spids = spids + p.Id + ',';
        }
        spids = (spids.endsWith(',') ? spids.substring(0, spids.lastIndexOf(',')) : spids);
        pageRef = new PageReference('apex/familystar?id='+spids);  
        pageRef.setRedirect(true);  
        return pageRef;  
    }

    global String GetValidDate(DateTime a){
        return String.ValueOf(a.day()) + '/' + String.ValueOf(a.month()) + '/' + String.ValueOf(a.year());
    }

    global String GetStarDates(){
        String data = '';
        Integer i = 0;
        for(Family_Star__c a : addedPlans.values()){
            i++;
            if(i == datesForFilter.size())data += GetValidDate(a.Star_Date_Derived__c);
            else data += GetValidDate(a.Star_Date_Derived__c) + '|';
        }
        String step = (data.endsWith('|') ? data.substring(0, data.lastIndexOf('|')) : data);
        return step;
    }
        
    Private integer toInteger(string stringToConvert){
        integer converted;
        Try {
            if (dateOfPlan <> null){
                system.debug('### string to convert = ' + stringToConvert);
                if (stringToConvert.length() > 1 && stringToConvert.substring(0,2) == '10')
                    converted = integer.valueOf(stringToConvert.substring(0,2));
                else{
                    converted = integer.valueOf(stringToConvert.substring(0,1));
                system.debug('### converted = ' + converted);
                }
            } else {
                converted = 0;
            }
        }
        Catch (Exception e) {
            converted = 0;
        }
        system.debug('### converted = ' + converted);
        return converted;
    }

    global string formatDate(DateTime inputDate){
    If (inputDate != null){
        return (string.valueOf(inputDate.format('dd/MM/yyyy')));
      } else
        return '01/01/2012';
    }    
    
}