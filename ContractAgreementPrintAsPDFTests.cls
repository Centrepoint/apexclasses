@isTest
private class ContractAgreementPrintAsPDFTests {
  private static testMethod void testContractAgreementPrintAsPDFTests() {
    Id devRecordTypeId = Schema.SObjectType.Contract_Agreement__c.getRecordTypeInfosByName().get('Health - Dietetics Contract').getRecordTypeId();
    Contract_Agreement__c ca = new Contract_Agreement__c(Date_Staff_Member_Signed__c = Date.Today().addDays(-10),
                                                        Date_Young_Person_Signed__c = Date.Today().addDays(-10),
                                                        Staff_Members_Name__c = 'Test SM',
                                                        Young_Persons_Name__c = 'Test YP',
                                                        RecordTypeId = devRecordTypeId);
    insert ca;
    E_Signature__c es1 = new E_Signature__c(Contract_Agreement__c = ca.Id);
    E_Signature__c es2 = new E_Signature__c(Contract_Agreement__c = ca.Id);
    insert es1;
    insert es2;

    ContentVersion contentVersion1 = new ContentVersion(
            Title = 'CPStaffSignature.png',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
    insert contentVersion1;
    ContentDocument CD1 = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument order by createdDate DESC limit 1];
    
    //create ContentDocumentLink  record
    ContentDocumentLink cdl1 = New ContentDocumentLink();
    cdl1.LinkedEntityId = es1.id;
    cdl1.ContentDocumentId = CD1.Id;
    cdl1.shareType = 'V';
    insert cdl1;

    ContentVersion contentVersion2 = new ContentVersion(
            Title = 'YPSignature.png',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
    insert contentVersion2;
    ContentDocument CD2 = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument order by createdDate DESC limit 1];

    //create ContentDocumentLink  record
    ContentDocumentLink cdl2 = New ContentDocumentLink();
    cdl2.LinkedEntityId = es2.id;
    cdl2.ContentDocumentId = CD2.Id;
    cdl2.shareType = 'V';
    insert cdl2;

    Test.startTest();
    PageReference pageRef = Page.ContractAgreementPrintAsPDF;
    pageRef.getParameters().put('Id', String.valueOf(ca.Id));
    System.Test.setCurrentPage(pageRef);
    ApexPages.currentPage().getParameters().put('Id', ca.Id);

    ApexPages.StandardController sc = new ApexPages.StandardController(ca);
    ContractAgreementPrintAsPDFController con = new ContractAgreementPrintAsPDFController(sc);
    con.getTitle();
    con.getSMSigUrl();
    con.getYPSigUrl();
    Test.stopTest();
  }
}