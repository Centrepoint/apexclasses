@IsTest
global class KyperaTriggerSettings_Test {

    global static testMethod void testKyperaTriggerSettings() {
        Test.StartTest();
        
        boolean b = TriggerSettings.IsDisableAllTriggers();
        System.assertEquals(false, b);
        
        boolean c = TriggerSettings.IsSyncWithKypera();
        System.assertEquals(false, c);
        Test.StopTest();
    }
    
}