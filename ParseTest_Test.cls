@IsTest
public class ParseTest_Test {
    
    static testmethod void m1() {
       Test.StartTest();
         
        String responseStub = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><GetPropertyStructureXmlResponse xmlns="http://mobile.kypera.com/InformWebService/"><GetPropertyStructureXmlResult><Kypera xmlns=""><PropertyStructure><Districts><District><DistrictReference><![CDATA[GLB]]></DistrictReference><DistrictName><![CDATA[Central West Midlands]]></DistrictName><Scheme><SchemeReference><![CDATA[ALX]]></SchemeReference><SchemeName><![CDATA[BIG SKY COUNTRY]]></SchemeName><Block><BlockReference><![CDATA[BL509510]]></BlockReference><BlockName><![CDATA[1/5 Coster Road]]></BlockName><Unit><UnitReference><![CDATA[0951]]></UnitReference><UnitName><![CDATA[1 5 Coster Road]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL509520]]></BlockReference><BlockName><![CDATA[2/5 Coster Road]]></BlockName><Unit><UnitReference><![CDATA[0952]]></UnitReference><UnitName><![CDATA[2 5 Coster Road]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL509530]]></BlockReference><BlockName><![CDATA[3/5 Coster Road]]></BlockName><Unit><UnitReference><![CDATA[0953]]></UnitReference><UnitName><![CDATA[3 5 Coster Road]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL511900]]></BlockReference><BlockName><![CDATA[1/19 Rose Cottage]]></BlockName><Unit><UnitReference><![CDATA[1190]]></UnitReference><UnitName><![CDATA[1 19 Rose Cottage]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL511910]]></BlockReference><BlockName><![CDATA[2/19 Rose Cottage]]></BlockName><Unit><UnitReference><![CDATA[1191]]></UnitReference><UnitName><![CDATA[2 19 Rose Cottage]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL511920]]></BlockReference><BlockName><![CDATA[3/19 Rose Cottage]]></BlockName><Unit><UnitReference><![CDATA[1192]]></UnitReference><UnitName><![CDATA[3 19 Rose Cottage]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL511930]]></BlockReference><BlockName><![CDATA[1/76 Nihil Road]]></BlockName><Unit><UnitReference><![CDATA[1193]]></UnitReference><UnitName><![CDATA[1 76 Nihil Road]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL511940]]></BlockReference><BlockName><![CDATA[2/76 Nihil Road]]></BlockName><Unit><UnitReference><![CDATA[1194]]></UnitReference><UnitName><![CDATA[2 76 Nihil Road]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL511950]]></BlockReference><BlockName><![CDATA[3/76 Nihil Road]]></BlockName><Unit><UnitReference><![CDATA[1195]]></UnitReference><UnitName><![CDATA[3 76 Nihil Road]]></UnitName></Unit></Block></Scheme><Scheme><SchemeReference><![CDATA[BRD]]></SchemeReference><SchemeName><![CDATA[EXPLORER COUNTRY]]></SchemeName><Block><BlockReference><![CDATA[BL504710]]></BlockReference><BlockName><![CDATA[1/22 Pinniger Court]]></BlockName><Unit><UnitReference><![CDATA[0471]]></UnitReference><UnitName><![CDATA[1 22 Pinniger Court]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL504720]]></BlockReference><BlockName><![CDATA[16 Rupert Drive]]></BlockName><Unit><UnitReference><![CDATA[0472]]></UnitReference><UnitName><![CDATA[ 16 Rupert Drive]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL510390]]></BlockReference><BlockName><![CDATA[3 Davidson Place]]></BlockName><Unit><UnitReference><![CDATA[1039]]></UnitReference><UnitName><![CDATA[ 3 Davidson Place]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL510810]]></BlockReference><BlockName><![CDATA[31 Ferguson Court]]></BlockName><Unit><UnitReference><![CDATA[1081]]></UnitReference><UnitName><![CDATA[ 31 Ferguson Court]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL511010]]></BlockReference><BlockName><![CDATA[12 Dean Court]]></BlockName><Unit><UnitReference><![CDATA[1101]]></UnitReference><UnitName><![CDATA[ 12 Dean Court]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL511300]]></BlockReference><BlockName><![CDATA[1/133 High Lane]]></BlockName><Unit><UnitReference><![CDATA[1130]]></UnitReference><UnitName><![CDATA[1 133 High Lane]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL511310]]></BlockReference><BlockName><![CDATA[2/133 High Lane]]></BlockName><Unit><UnitReference><![CDATA[1131]]></UnitReference><UnitName><![CDATA[2 133 High Lane]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL511320]]></BlockReference><BlockName><![CDATA[3/133 High Lane]]></BlockName><Unit><UnitReference><![CDATA[1132]]></UnitReference><UnitName><![CDATA[3 133 High Lane]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL511470]]></BlockReference><BlockName><![CDATA[21 Greenwood Road]]></BlockName><Unit><UnitReference><![CDATA[1147]]></UnitReference><UnitName><![CDATA[ 21 Greenwood Road]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL511520]]></BlockReference><BlockName><![CDATA[1/30 Murchison Place]]></BlockName><Unit><UnitReference><![CDATA[1152]]></UnitReference><UnitName><![CDATA[1 30 Murchison Place]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL511530]]></BlockReference><BlockName><![CDATA[2/30 Murchison Place]]></BlockName><Unit><UnitReference><![CDATA[1153]]></UnitReference><UnitName><![CDATA[2 30 Murchison Place]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL511540]]></BlockReference><BlockName><![CDATA[3/30 Murchison Place]]></BlockName><Unit><UnitReference><![CDATA[1154]]></UnitReference><UnitName><![CDATA[3 30 Murchison Place]]></UnitName></Unit></Block></Scheme><Scheme><SchemeReference><![CDATA[CBR]]></SchemeReference><SchemeName><![CDATA[HUNTER VALLEY]]></SchemeName><Block><BlockReference><![CDATA[BL308210]]></BlockReference><BlockName><![CDATA[12 Gorton Road]]></BlockName><Unit><UnitReference><![CDATA[0821]]></UnitReference><UnitName><![CDATA[ 12 Gorton Road]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL308230]]></BlockReference><BlockName><![CDATA[3/7 William Drive]]></BlockName><Unit><UnitReference><![CDATA[0823]]></UnitReference><UnitName><![CDATA[3 7 William Drive]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL308240]]></BlockReference><BlockName><![CDATA[7 Golden Lane]]></BlockName><Unit><UnitReference><![CDATA[0824]]></UnitReference><UnitName><![CDATA[ 7 Golden Lane]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL308260]]></BlockReference><BlockName><![CDATA[12 Wallowa Court]]></BlockName><Unit><UnitReference><![CDATA[0826]]></UnitReference><UnitName><![CDATA[ 12 Wallowa Court]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL308270]]></BlockReference><BlockName><![CDATA[21 Denise Place]]></BlockName><Unit><UnitReference><![CDATA[0827]]></UnitReference><UnitName><![CDATA[ 21 Denise Place]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL308500]]></BlockReference><BlockName><![CDATA[1/33A Campbell Street]]></BlockName><Unit><UnitReference><![CDATA[0850]]></UnitReference><UnitName><![CDATA[1 33A Campbell Street]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL308510]]></BlockReference><BlockName><![CDATA[2/33A Campbell Street]]></BlockName><Unit><UnitReference><![CDATA[0851]]></UnitReference><UnitName><![CDATA[2 33A Campbell Street]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL308520]]></BlockReference><BlockName><![CDATA[5/27 Wallowa Court]]></BlockName><Unit><UnitReference><![CDATA[0858]]></UnitReference><UnitName><![CDATA[5 27 Wallowa Court]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL308530]]></BlockReference><BlockName><![CDATA[2/33 Campbell Street]]></BlockName><Unit><UnitReference><![CDATA[0853]]></UnitReference><UnitName><![CDATA[2 33 Campbell Street]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL308540]]></BlockReference><BlockName><![CDATA[3/33 Campbell Street]]></BlockName><Unit><UnitReference><![CDATA[0854]]></UnitReference><UnitName><![CDATA[3 33 Campbell Street]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL308550]]></BlockReference><BlockName><![CDATA[1/32 Wallowa Court]]></BlockName><Unit><UnitReference><![CDATA[0855]]></UnitReference><UnitName><![CDATA[1 32 Wallowa Court]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL308560]]></BlockReference><BlockName><![CDATA[2/32 Wallowa Court]]></BlockName><Unit><UnitReference><![CDATA[0856]]></UnitReference><UnitName><![CDATA[2 32 Wallowa Court]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL308570]]></BlockReference><BlockName><![CDATA[6/27 Wallowa Court]]></BlockName><Unit><UnitReference><![CDATA[0859]]></UnitReference><UnitName><![CDATA[6 27 Wallowa Court]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL308580]]></BlockReference><BlockName><![CDATA[7/27 Wallowa Court]]></BlockName><Unit><UnitReference><![CDATA[0860]]></UnitReference><UnitName><![CDATA[7 27 Wallowa Court]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL308590]]></BlockReference><BlockName><![CDATA[1/27 Wallowa Court]]></BlockName><Unit><UnitReference><![CDATA[0852]]></UnitReference><UnitName><![CDATA[1 27 Wallowa Court]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL308600]]></BlockReference><BlockName><![CDATA[4/24 Wallowa Court]]></BlockName><Unit><UnitReference><![CDATA[0857]]></UnitReference><UnitName><![CDATA[4 27 Wallowa Court]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL308610]]></BlockReference><BlockName><![CDATA[1/45 Blackwood Grove]]></BlockName><Unit><UnitReference><![CDATA[0861]]></UnitReference><UnitName><![CDATA[1 45 Blackwood Grove]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL310290]]></BlockReference><BlockName><![CDATA[34 Sturt Road]]></BlockName><Unit><UnitReference><![CDATA[1029]]></UnitReference><UnitName><![CDATA[ 34 Sturt Road]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL310300]]></BlockReference><BlockName><![CDATA[1 Sanctuary Place]]></BlockName><Unit><UnitReference><![CDATA[1030]]></UnitReference><UnitName><![CDATA[ 1 Sanctuary Place]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL310310]]></BlockReference><BlockName><![CDATA[1/13 Williams Avenue]]></BlockName><Unit><UnitReference><![CDATA[1031]]></UnitReference><UnitName><![CDATA[1 13 Williams Avenue]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL310370]]></BlockReference><BlockName><![CDATA[7 Dudley Park Crescent]]></BlockName><Unit><UnitReference><![CDATA[1037]]></UnitReference><UnitName><![CDATA[ 7 Dudley Park Crescent]]></UnitName></Unit></Block><Block><BlockReference><![CDATA[BL310380]]></BlockReference><BlockName><![CDATA[9 Moorpark Road]]></BlockName><Unit><UnitReference><![CDATA[1038]]></UnitReference><UnitName><![CDATA[ 9 Moorpark Road]]></UnitName></Unit></Block></Scheme></District></Districts></PropertyStructure></Kypera></GetPropertyStructureXmlResult></GetPropertyStructureXmlResponse></soap:Body></soap:Envelope>';            

        // Create Kypera Metadata in the Custom Setting
        List<Kypera_Metadata__c> kms = new List<Kypera_Metadata__c>();
        
        kms.add(new Kypera_Metadata__c(Name = 'GetCurrentTransactionsBegin', Metadata__c = '<GetCurrentTransactions xmlns="http://mobile.kypera.com/InformWebService/">'));
        kms.add(new Kypera_Metadata__c(Name = 'GetCurrentTransactionsEnd', Metadata__c = '</GetCurrentTransactions>'));
        kms.add(new Kypera_Metadata__c(Name = 'GetCurrentTransactionsEndPoint', Metadata__c = 'http://mobile.kypera.com/InformWebService/GetCurrentTransactions'));
        
        kms.add(new Kypera_Metadata__c(Name = 'createPersonBegin', Metadata__c = '<CreatePerson xmlns="http://mobile.kypera.com/InformWebService/">'));
        kms.add(new Kypera_Metadata__c(Name = 'createPersonEnd', Metadata__c = '</CreatePerson>'));
        kms.add(new Kypera_Metadata__c(Name = 'soapBodyBegin', Metadata__c = '<soap:Body>'));
        kms.add(new Kypera_Metadata__c(Name = 'soapBodyEnd', Metadata__c = '</soap:Body>'));
        kms.add(new Kypera_Metadata__c(Name = 'soapEnvelopeBegin', Metadata__c = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'));
        kms.add(new Kypera_Metadata__c(Name = 'soapEnvelopeEnd', Metadata__c = '</soap:Envelope>'));
        kms.add(new Kypera_Metadata__c(Name = 'xmlTag', Metadata__c = '<?xml version="1.0" encoding="utf-8"?>'));
        kms.add(new Kypera_Metadata__c(Name = 'ServiceHandler',  Metadata__c = 'http://mobile.kypera.com/InformWebService/ServiceHandler.asmx'));
        kms.add(new Kypera_Metadata__c(Name = 'CreatePersonEndPoint',  Metadata__c = 'http://mobile.kypera.com/InformWebService/CreatePerson'));
        kms.add(new Kypera_Metadata__c(Name = 'UpdatePersonEndPoint',  Metadata__c = 'http://mobile.kypera.com/InformWebService/UpdatePerson'));
        kms.add(new Kypera_Metadata__c(Name = 'updatePersonBegin',  Metadata__c = '<UpdatePerson xmlns="http://mobile.kypera.com/InformWebService/">'));
        kms.add(new Kypera_Metadata__c(Name = 'updatePersonEnd',  Metadata__c = '</UpdatePerson>'));
        kms.add(new Kypera_Metadata__c(Name = 'GetPropertyStructureXML', Metadata__c = 'http://mobile.kypera.com/InformWebService/GetPropertyStructureXml'));
        kms.add(new Kypera_Metadata__c(Name = 'BatchRecordNumber', Metadata__c = '1'));
        kms.add(new Kypera_Metadata__c(Name = 'NumberOfRecordsToProcess', Metadata__c = '10'));
        kms.add(new Kypera_Metadata__c(Name = 'Frequency', Metadata__c = '10'));
        kms.add(new Kypera_Metadata__c(Name = 'GetSchemeBedSpaceDetailsBegin', Metadata__c = '<GetSchemeBedSpaceDetails xmlns="http://inform.kypera.com/InformWebService/">'));
        kms.add(new Kypera_Metadata__c(Name = 'GetSchemeBedSpaceDetailsEnd', Metadata__c = '</GetSchemeBedSpaceDetails>'));
        kms.add(new Kypera_Metadata__c(Name = 'GetSchemeBedSpaceDetailsEndPoint', Metadata__c = 'http://myrentandrepairs.centrepoint.org.uk:8080/InformWebServiceTest/GetSchemeBedSpaceDetails'));
        insert kms;

        // put these in a map for easy retrieval later on
        Map<String, String> kyperaMetaData = new Map<String, String>();
        for (Kypera_Metadata__c kmsMapItem: kms) {
            kyperaMetaData.put(kmsMapItem.Name, kmsMapItem.Metadata__c);
        }
        
           // create test Kypera Properties
        List<Kypera_Properties__c> KyperaProperties = new List<Kypera_Properties__c>();
        for(Integer i = 0; i < 2; i++) {
            KyperaProperties.add(new Kypera_Properties__c
                                    (
                                    Scheme_Name__c = 'Property ' + i, 
                                    Scheme_Reference__c = String.valueOf(i),
                                    Unit_Name__c = 'Unit ' + i,
                                    Unit_Reference__c = '10',
                                    H_Core_Rent__c = 100,
                                    P_Water__c = 10,
                                    P_Food__c = 10,
                                    P_Gas__c = 10,
                                    P_Electricity__c = 10,
                                    P_TV_Licence__c = 10,
                                    P_Other_including_toiletries__c = 10,
                                    H_Council_tax__c = 7.5,
                                    H_Cleaning_of_communal_areas_and__c = 7.5,
                                    H_Service_contracts__c = 7.5,
                                    H_Heat_light__c = 7.5,
                                    H_Renewals_and_replacements__c = 7.5,
                                    H_Laundry_facilities__c = 7.5,
                                    Type_of_tenancy__c = 'T Type ' + i,
                                    Bedspace_Type__c = 'B Type ' + i                                   
                                    )
                                );
        }
        
        insert KyperaProperties;
                      
                
        // Add the Scheme Codes
        List<Kypera_Scheme_Codes__c> ksc = new List<Kypera_Scheme_Codes__c>();
        ksc.add(new Kypera_Scheme_Codes__c(Name = 'A01', BatchRecordNumber__c = 1));
        ksc.add(new Kypera_Scheme_Codes__c(Name = 'A02', BatchRecordNumber__c = 2));
        ksc.add(new Kypera_Scheme_Codes__c(Name = 'A03', BatchRecordNumber__c = 3));
        ksc.add(new Kypera_Scheme_Codes__c(Name = 'A05', BatchRecordNumber__c = 4));
        ksc.add(new Kypera_Scheme_Codes__c(Name = 'A06', BatchRecordNumber__c = 5));
        ksc.add(new Kypera_Scheme_Codes__c(Name = 'A07', BatchRecordNumber__c = 6));
        ksc.add(new Kypera_Scheme_Codes__c(Name = 'A08', BatchRecordNumber__c = 7));
        ksc.add(new Kypera_Scheme_Codes__c(Name = 'A09', BatchRecordNumber__c = 8));
        ksc.add(new Kypera_Scheme_Codes__c(Name = 'A10', BatchRecordNumber__c = 9));
        ksc.add(new Kypera_Scheme_Codes__c(Name = 'A11', BatchRecordNumber__c = 10));
        ksc.add(new Kypera_Scheme_Codes__c(Name = 'ZZI', BatchRecordNumber__c = 11));
        insert ksc;
        
        String CRON_EXP = '0 0 3 * * ?'; 

        //KyperaBedspaceDataScheduledImport sync = new KyperaBedspaceDataScheduledImport();

        // Schedule the test job
        //String jobId = System.schedule('testScheduledApex-KyperaBedspaceDataScheduledImport', CRON_EXP,sync);

        // Get the information from the CronTrigger API object
                //CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        String expectedResult = '<?xml version="1.0" encoding="utf-8"?>' +
                                '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"' +
                                'xmlns="urn:partner.soap.sforce.com"' +
                                'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
                                    '<soapenv:Body>' +
                                     '<loginResponse>' +
                                        '<result>' +
                                           '<personalServiceCharge>tset</personalServiceCharge>' +
                                           '<housingServiceCharge>4</housingServiceCharge>' +   
                                           '<code>34</code>'+ 
                                           '<propertyType>test</propertyType>'+
                                           '<defaultOccupancyType>Tset34</defaultOccupancyType>'+
                                           '<hCoreRentProcessed>false</hCoreRentProcessed>'+  
                                           '<serverUrl>https://na1.salesforce.com/services/Soap/u/10.0</serverUrl>' +
                                           '<sessionId>QwWsHJyTPW.1pd0_jXlNKOSU</sessionId>' +
                                           '<EOF>EF</EOF>'+
                                           '<userId>005D0000000nVYVIA2</userId>' +
                                           '<endOfUnit>' +
                                              '<!-- ... -->' +
                                           '</endOfUnit>' +
                                        '</result>' +
                                     '</loginResponse>' +
                                    '</soapenv:Body>' +
                                '</soapenv:Envelope>';
        Dom.Document doc = new Dom.Document();
        doc.load(expectedResult);
        Dom.XMLNode root = doc.getRootElement();   
  
        ParseTest parsetest = new ParseTest ();     
        parsetest.unitXML(responseStub);
        parsetest.textToParse=responseStub;
        parsetest.parse();
        parsetest.parseXMLForSchemes(root);
        parsetest.processDataList();
        
        Test.StopTest();
    }
}