global class KyperaBedspaceDataScheduledImport implements Schedulable {

   global void execute(SchedulableContext SC) {
        KyperaBedspaceDataProcessor batchApex = new KyperaBedspaceDataProcessor();
        ID batchprocessid = Database.executeBatch(batchApex);
   }
   
}