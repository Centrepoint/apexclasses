@IsTest
global without sharing class KyperaPropertyToServiceScheduledUpdate_t {

    static testmethod void m1() {
        Test.StartTest();
        String CRON_EXP = '0 0 23 * * ?'; 

        KyperaPropertyToServiceScheduledUpdate sync = new KyperaPropertyToServiceScheduledUpdate();

        // Schedule the test job
        String jobId = System.schedule('testScheduledApex-KyperaPropertyToServiceScheduledUpate', CRON_EXP,sync);

        // Get the information from the CronTrigger API object
                CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        

        Test.StopTest();
    }
    
}