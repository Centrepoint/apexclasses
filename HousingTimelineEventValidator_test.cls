@IsTest
public class HousingTimelineEventValidator_test {
    @IsTest
    public static void methodName(){
        // Create Test Service
        Account testService = new Account(
            Name = 'Test Service',
            SPCR_Forms_Submitted__c = 'Yes',
            SP_Outcomes_Forms_Submitted__c = 'Yes',
            SP_Outcomes_Completed__c = 'Yes',
            CORE_Forms_Submitted__c = 'Yes',
            CORE_Mix_of_Shared_and_Self_Contained__c = 'Yes'
        );
        Insert testService;

        // Create a client record
        Contact testClient = new Contact(
            Salutation = 'Mr.',
            FirstName = 'Test',
            LastName = 'Client',
            infrm__Consent__c = 'Yes - written',
            Birthdate = Date.today() - 7300,
            infrm__Gender__c = 'Male',
            infrm__Transgender_HL__c = 'No',
            AccountId = testService.Id,
            infrm__Ethnic_Origin__c = 'White: British',
            infrm__Sexuality__c	= 'Heterosexual',
            infrm__Disabled_person__c = 'No',
            Has_children__c = 'Yes',
            infrm__Religion__c = 'None',
            Local_Authority_of_origin__c = 'Bedford',
            infrm__Nationality__c = 'UK',
            infrm__Immigration_Status__c = 'British Citizen',
            infrm__Prefered_Language__c = 'English',
            Translator_Required__c = 'No',
            Ever_Slept_Rough__c = 'YES',
            infrm__ex_Armed_forces_personnel_SP__c = 'Yes',
            Are_you_a_care_leaver__c = 'Refused',
            Ever_been_In_Care__c = 'Refused'
        );
        Insert testClient;

        // Get recordtypeid for timeline events
        Id recordType;
        for(HousingTimelineEventRecordTypes__mdt rtmd : [SELECT MasterLabel, QualifiedApiName FROM HousingTimelineEventRecordTypes__mdt LIMIT 1]){
            recordType = Schema.SObjectType.infrm__TimelineEvents__c.getRecordTypeInfosByName().get(rtmd.MasterLabel).getRecordTypeId();
        }

        // Timeline Event List
        List<infrm__TimelineEvents__c> teList = new List<infrm__TimelineEvents__c>();

        // TimelineId Set

        Set<Id> timelineId = new Set<Id>();


        Test.startTest();
        // Create timeline event for organisation lead project
        infrm__TimelineEvents__c teTest1 = new infrm__TimelineEvents__c(
            infrm__Client__c = testClient.Id,
            infrm__Lead_Project2__c = testService.Id,
            RecordTypeId = recordType,
            infrm__start_date__c = Date.today(),
            In_EET__c = 'Yes',
            Keyworker__c = UserInfo.getUserId(),
            infrm__referral_date__c = Date.today()
        );
        Insert teTest1;
        

        infrm__TimelineEvents__c teTest2 = new infrm__TimelineEvents__c(
            infrm__Client__c = testClient.Id,
            infrm__Lead_Project2__c = testService.Id,
            RecordTypeId = recordType,
            infrm__start_date__c = Date.today(),
            In_EET__c = 'Yes',
            Keyworker__c = UserInfo.getUserId(),
            infrm__referral_date__c = Date.today()
        );
        insert teTest2;
        timelineId.add(teTest2.Id);


        for(infrm__TimelineEvents__c te : [SELECT Id, infrm__Client__c, infrm__Lead_Project2__c, RecordTypeId, Timeline_Status__c, infrm__start_date__c, infrm__end_date__c, infrm__Record_Type_formula__c FROM infrm__TimelineEvents__c WHERE Id IN :timelineId]){
            if(te != null){
                teList.add(te);
            }
        }
        
        // Run the housing validation method to validate the number of timline events
        HousingTimelineEventValidator.validateNumberOfTimelineEvents(teList);
        

        Test.stopTest();
        
    }
}