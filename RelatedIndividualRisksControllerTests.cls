@isTest
private class RelatedIndividualRisksControllerTests {
    @testSetup static void setupTestData(){
        List<Individual_Risk__c> testIRs = new List<Individual_Risk__c>();
        // Create Contact
        Contact testCon = new Contact(FirstName = 'Test',
                                        LastName = 'Test');
        insert testCon;

        // Create Housing_Assessment__c record
        Housing_Assessment__c testHA = new Housing_Assessment__c(Date_Submitted_for_Approval__c = Date.Today().addDays(-10),
                                                                Young_Persons_Name__c = testCon.Id);
        insert testHA;
        // Create Individual_Risk__c records for all types of risk and time scenarios
        Individual_Risk__c testIR1 = new Individual_Risk__c(Client__c = testCon.Id,
                                                            Current_Risk__c = 'Yes',
                                                            Date_Risk_Assessed__c = Date.Today().addDays(-12),
                                                            Type_Of_Risk__c = 'Harm from Others');
        Individual_Risk__c testIR2 = new Individual_Risk__c(Client__c = testCon.Id,
                                                            Current_Risk__c = 'No',
                                                            Date_Risk_Became_No_Longer_Current__c = Date.Today(),
                                                            Type_Of_Risk__c = 'Harm from Others');
        Individual_Risk__c testIR3 = new Individual_Risk__c(Client__c = testCon.Id,
                                                            Current_Risk__c = 'Yes',
                                                            Date_Risk_Assessed__c = Date.Today().addDays(-12),
                                                            Type_Of_Risk__c = 'Harm to Others');
        Individual_Risk__c testIR4 = new Individual_Risk__c(Client__c = testCon.Id,
                                                            Current_Risk__c = 'No',
                                                            Date_Risk_Became_No_Longer_Current__c = Date.Today(),
                                                            Type_Of_Risk__c = 'Harm to Others');
        Individual_Risk__c testIR5 = new Individual_Risk__c(Client__c = testCon.Id,
                                                            Current_Risk__c = 'Yes',
                                                            Date_Risk_Assessed__c = Date.Today().addDays(-12),
                                                            Type_Of_Risk__c = 'Harm to Self');
        Individual_Risk__c testIR6 = new Individual_Risk__c(Client__c = testCon.Id,
                                                            Current_Risk__c = 'No',
                                                            Date_Risk_Became_No_Longer_Current__c = Date.Today(),
                                                            Type_Of_Risk__c = 'Harm to Self');
        testIRs.add(testIR1);
        testIRs.add(testIR2);
        testIRs.add(testIR3);
        testIRs.add(testIR4);
        testIRs.add(testIR5);
        testIRs.add(testIR6);

        insert testIRs;
    }

    private static testMethod void testIrsFromOthers() {
        Test.startTest();
        Housing_Assessment__c testHA = [SELECT Id, Name FROM Housing_Assessment__c LIMIT 1];
        PageReference pageRef = Page.RelatedIndividualRisksFromOthers;
        pageRef.getParameters().put('Id', String.valueOf(testHA.Id));
        System.Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', testHA.Id);

        ApexPages.StandardController sc = new ApexPages.StandardController(testHA);
        RelatedIndividualRisksController con = new RelatedIndividualRisksController(sc);
        con.getIrsToOthers();
        con.getIrsFromOthers();
        con.getIrsToSelf();
        Test.stopTest();
    }
  
}