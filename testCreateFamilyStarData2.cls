/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class testCreateFamilyStarData2 {

    static testMethod void myUnitTest() {
    
    // Map<String, RecordType> recNameMap = ContactUtils.GetRecTypeName();
    
    // Account family = new Account(Name = 'TestStarTest', RecordTypeId = recNameMap.get('Family').Id);
    Contact client = new Contact(FirstName = 'TestStarTest', LastName = 'TestStarTest');
    insert client;
    /*
    insert family;
    family = [select id from Account where Name = 'TestStarTest'];
    */
    /*
    Contact client = new Contact(FirstName = 'Arthur', LastName = 'TestAskey');
    insert client;
    client = [select id from Contact where LastName = 'TestAskey'];*/
    
    Family_Star__c familystar = new Family_Star__c(Client__c = client.Id);
    insert familystar;//test trigger with null values
    familystar = [Select Star_Data__c from Family_Star__c where Client__c =: client.Id];
    System.assertEquals(familystar.Star_Data__c, '0,0,0,0,0,0,0,0,0');
    
    familystar.Social_networks__c = '4';
    update familystar;//test trigger with a value
    familystar = [Select Star_Data__c from Family_Star__c where Client__c =: client.Id];
    System.assertEquals(familystar.Star_Data__c, '0,0,0,4,0,0,0,0,0');
        
    familystar.Promoting_good_health__c = '10';
    update familystar;// test trigger with 10 to get 100% coverage
    familystar = [Select Star_Data__c, Promoting_good_health__c from Family_Star__c where Client__c =: client.Id];
    System.assertEquals(familystar.Star_Data__c, '10,0,0,4,0,0,0,0,10');
    }
        
    
}