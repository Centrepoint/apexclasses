global with sharing class KyperaController {

    private final Contact con;
    private Client_Information_Sent_to_Kypera__c log;
    private KyperaProcessor kp;
    
    // The constructor initialises the contact variable by using the getRecord method from the standard controller.
    public KyperaController(ApexPages.StandardController stdController) {
        this.con = (Contact)stdController.getRecord();
        log = new Client_Information_Sent_to_Kypera__c();
        log.Client__c = con.Id;
        log.Operation__c = 'Create';        
        kp = new KyperaProcessor();
    }
    
    // checks & preps the data before calling the Kypera web service
    public PageReference CallKypera() {
        try {
            if(!TriggerSettings.IsSyncWithKypera()){
                throw new CustomException('Error: integration with Kypera is not turned on in the Custom Setting called \'Trigger Settings\'. Please contact In-Form Support.');
            }  
            // variables to hold the response from Kypera and track whether an error has occurred.
            String request = '';
            String response = '';
            String hasNoErrors = 'true';

    
            // if the kypera reference on the contact is blank, call create person; 
            if (String.IsBlank(con.Kypera_Reference__c)) {
                // TODO (tbc an else statement that calls "update person" if it's false) OR (change the button to a link in a formula)  
                // call the method that calls the web service
                KyperaProcessor kp = new KyperaProcessor();
                request = kp.buildXmlRequest(con, 'Create', 'createPersonBegin', 'createPersonEnd');
                response = kp.callCreatePerson(request, log, 'CreatePersonEndPoint');    
                // create the log entry 
                log.Response__c = response;
                insert log;
                if (log.Id == null) {
                    insert log;
                } else {
                    update log;
                }
                hasNoErrors = kp.getErrorFromResponse(response, hasNoErrors);      
                      
                // if getErrorFromResponse returns true (i.e. no errors)
                if (hasNoErrors == 'true') {
                    // extract the Kypera reference from the response
                    String kyperaReference = kp.getKeyFromResponse(response, log, 'CreatePersonExtendedResult');
                    system.debug('*** kyperaReference = ' + kyperaReference);
                    // if the Kypera reference has come back as a number...
                    if (kyperaReference.isNumeric() && Integer.ValueOf(kyperaReference) > 0) {
                        // ... update the client with the Kypera ID         
                        con.Kypera_Reference__c = kyperaReference; 
                        update con;
                        // set the status on the log
                        log.Status__c = 'Success';
                        update log;
                    } else { // if the Kypera reference has not come back as a number
                        log.Status__c = 'Error';
                        log.Error_Message__c = 'An error occurred while processing the response received from Kypera. This error happened in the method CallKypera, in the class KyperaController while checking if the KyperaReference is numeric';
                        update log;
                    } // end if (kyperaReference.isNumeric())  
                } else { // if (hasNoErrors == 'true') 
                    log.Status__c = 'Error';
                    log.Error_Message__c = 'An error occurred while processing the response received from Kypera. This error happened in the method CallKypera, in the class KyperaController while checking if the response contains an error. See the Response field for the error details.';
                    update log;
                } // end if (hasNoErrors == 'true')            
            } // end if (String.IsBlank(con.Kypera_Reference__c)) 
            
            // return to the contact
            return kp.redirectToContact(con.Id); 
        } catch (Exception e){
            log.Status__c = 'Error';
            log.Error_Message__c = e.getMessage();
            if (log.Id == null) {
                insert log;
            } else {
                update log;
            }
            ApexPages.addMessages(e);
            return null;
        }  // end try
        
       
    } // end CallKypera
    
}