global class KyperaPropertyToServiceBatchProcessor implements Database.Batchable<sObject> {

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([SELECT ID, Name FROM Kypera_Metadata__c LIMIT 1]);
    }

    global void execute(Database.BatchableContext BC, List<Kypera_Metadata__c> records) {       
       // update the services data
       KyperaPropertyToService kpts = new KyperaPropertyToService();
       kpts.doUpdate();
    }   

    global void finish(Database.BatchableContext BC){
        // post-processing  
    }
    
}