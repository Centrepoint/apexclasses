global with sharing class KyperaProcessor {

    // the list of fields and their settings
    global static List<Kypera_Contact_Fields__c> fields = new List<Kypera_Contact_Fields__c>();
    global static Map<String, Kypera_Metadata__c> metadata = new Map<String, Kypera_Metadata__c>();
    
    // the translations of In-Form picklists to Kypera picklists
    global static List<Kypera_Contact_Field_Mappings__c> mappings = new List<Kypera_Contact_Field_Mappings__c>();

    // schema variables to find the data type for fields
    global static Schema.SObjectType soType { get; set; } 
    global static Schema.DescribeSObjectResult soRes { get; set; } 
    global static Schema.DescribeFieldResult fdRes { get; set; } 
    
    // variable for responses from callouts during tests
    global static string responseStub { get; set; }      
      
    // constructor to initialise variables
    global KyperaProcessor() {
        // initialise the list of fields and their settings
        fields = [SELECT 
                Id, Name, Element_Name__c, Order__c, Do_Not_Sync__c, Operation__c,
                Override__c, Has_Mapping__c, Kypera_Data_Type__c, Can_Sync_Back__c,DefaultValue__c
             FROM
                Kypera_Contact_Fields__c
             ORDER BY 
                Order__c];    
                
        // initialise the translations of In-Form picklists to Kypera picklists
        mappings = [SELECT
                Id, Kypera_Contact_Field__c, In_Form_Value__c, Kypera_Value__c
             FROM
                Kypera_Contact_Field_Mappings__c];
        
        // initialise the schema variables
        soType = Schema.getGlobalDescribe().get('Contact');
        soRes = soType.getDescribe();
        
        // get metadata
        metadata = Kypera_Metadata__c.getAll();
        
    }

    /* 
        Returns the XML for an operation. 
        The operation parameter must be a valid value in Kypera_Contact_Field__c.Operations__c
        The xmlBegin and xmlEnd parameters must be valid custom settings in Kypera_Metadata__c    
    */
    global string buildXmlRequest(Contact con, String operation, String xmlBegin, String xmlEnd) {
        string retVal = '';
        retVal += xmlStart(metadata);
        retVal += metadata.get(xmlBegin).MetaData__c;
        retVal += constructPersonNode(con, operation);
        retVal += metadata.get(xmlEnd).MetaData__c;
        retVal += xmlEnd(metadata);        
        return retval;
    }
    
    // create the start of the outerXML to wrap around the CreatePerson XML node
    global string xmlStart(Map<String, Kypera_Metadata__c> metaDataList) {
        string retVal = '';
        retVal += metaDataList.get('xmlTag').MetaData__c;
        retVal += metaDataList.get('soapEnvelopeBegin').MetaData__c;
        retVal += metaDataList.get('soapBodyBegin').MetaData__c;        
        return retVal;
    }

    // create the end of the outerXML to wrap around the CreatePerson XML node
    global string xmlEnd(Map<String, Kypera_Metadata__c> metaDataList) {
        string retVal = '';
        retVal += metaDataList.get('soapBodyEnd').MetaData__c;        
        retVal += metaDataList.get('soapEnvelopeEnd').MetaData__c;
        return retVal;
    }

    // construct the Person XML node. Requires a contact and the operation, i.e. 'Create' or 'Update'
    global string constructPersonNode(Contact newContact, String op) { 
        string retVal = '';
        string fieldValue = '';
        string fieldType = '';
        
        // iterate through each field
        for (Kypera_Contact_Fields__c field : fields) {
            // set the default field value to '' so that if the field is not being synced it is an empty string
            fieldValue = '';
            fieldType = '';
            
            // if the fields needs to be synced, convert nulls to blanks, escape xml entities and lookup up the data type
            if (!field.Do_Not_Sync__c) {
                // convert nulls to blanks
                fieldValue = nullToBlankDefault(string.ValueOf(newContact.get(field.Name)),field.DefaultValue__c);
                                
                // get the data type of the field
                fdRes = soRes.fields.getMap().get(field.Name).getDescribe();   
                fieldType = fdRes.getType().Name();                
            }
            System.debug('*** constructPersonNode: fieldValue = ' + fieldValue + ' fieldType = ' + fieldType + ' field.Name = ' + field.Name);
            
            // if the field has an override value in the metadata, use that. This is for fields that the Kypera service requires but don't exist in In-Form.
            if (!String.IsBlank(field.Override__c)) {
                fieldValue = field.Override__c;
            }
                        
            // if the field is not blank and translation is required then perform the translation
            if (!String.IsBlank(fieldValue) && field.Has_Mapping__c > 0) {
                fieldValue = translatePicklist(field.Id, fieldValue);
            }
            
            // if the field is not blank and is a date field, convert it
            if (!String.IsBlank(fieldValue) && fieldType == 'DATE') {
                fieldValue = reformatDate(fieldValue);
            }
            
            // if the field is blank and the kypera data type is boolean 
            if (String.IsBlank(fieldValue) && field.Kypera_Data_Type__c == 'Boolean') {
                fieldValue = 'false';
            }
            
            // escape xml entities
            if (!String.IsBlank(fieldValue)) {
                fieldValue = escapeXMLEntities(fieldValue);
            }
            
            
            // write the tag...
            // if the field needs to be included in the operation
            if (field.Operation__c.Contains(op)) {
                retVal += writeTag(field.Element_Name__c, fieldValue);
            }
        } // end of iterate through each field'
        
        return retVal;
        
    }
    
    // reformat dates
    global string reformatDate(String dateValue) {
        String retVal = dateValue;
        retVal = dateValue.substring(0,10);
        return retVal;    
    }
    
    // translates picklists
    global string translatePicklist(Id fieldId, String fieldData) {
        string retVal = fieldData;
        String lookupVal = '';
        // a map with the In-Form value as the key and the Kypera translation as the value                
        Map<String, String> fieldMappings = new Map<String, String>();
        for (Kypera_Contact_Field_Mappings__c currentMap : mappings) {
            if (fieldId == currentMap.Kypera_Contact_Field__c) {
                fieldMappings.put(currentMap.In_Form_Value__c, currentMap.Kypera_Value__c);
            }        
        }
        lookupVal = fieldMappings.get(fieldData);
        if (!String.IsBlank(lookupVal)) {
            retVal = lookupVal;
        }
        
        return retVal;
    }
    
    global string writeTag(String tagName, String data) {
        string retVal = '';
        retVal += '<' + tagName + '>' + data + '</' + tagName + '>';
        return retVal;    
    }
    
    // convert blank and/or null fields to empty strings
    global string nullToBlankDefault(string field,String defaultValue) {
        string retVal = field;
        try {
            if(String.IsBlank(field)) {
                if(String.IsBlank(defaultValue)) {
                    retval = '';    
                }else {
                    retval = defaultValue;    
                }
            }
            return retVal;
        } catch (Exception e) {
            system.debug('*** error occurred in KyperaProcessor in method nullToBlank when processing the value ' + field);
            return retVal;
        }
    }
    
    // convert blank and/or null fields to empty strings
    global string nullToBlank(string field) {
        system.debug('field-----'+field);
        string retVal = field;
        try {
            if (String.IsBlank(field)) {
                retval = '';
            }
            return retVal;
        } catch (Exception e) {
            system.debug('*** error occurred in KyperaProcessor in method nullToBlank when processing the value ' + field);
            return retVal;
        }
    }
    
    
    // escape 5 xml entities
    global string escapeXMLEntities(string field) {
        string retval = field;
        try {
            retval = retval.replace('&','&amp;');
            retval = retval.replace('"','&quot;');
            retval = retval.replace('\'','&apos;');
            retval = retval.replace('<','&lt;');
            retval = retval.replace('>','&gt;');
            System.Debug('+++ retVal in escapeXMLEntities = ' + retVal);
            return retVal;
        } catch (Exception e) {
            system.debug('*** error occurred in KyperaProcessor in method nullToBlank when processing the value ' + field);
            return retVal;
        }
    }
    
    
    // string comparison method
    global static boolean StringComp(string field1, string field2) {
        boolean retVal = false;
        if (field1 == field2) {
            retVal = true;
        }
        return retVal;
    }    

    // Boolean comparison method
    global static boolean BoolComp(Boolean field1, Boolean field2) {
        boolean retVal = false;
        if (field1 == field2) {
            retVal = true;
        }
        return retVal;
    }  
    
    // Integer comparison method
    global static boolean IntComp(Integer field1, Integer field2) {
        boolean retVal = false;
        if (field1 == field2) {
            retVal = true;
        }
        return retVal;
    }     
    
    // Pre-process updated data then send to Kypera. Used in the after update trigger
    global static void EnqueueOutboundContactData(Map<Id, Contact> newContacts, Map<Id, Contact> oldContacts, Set<Id> newContactIds, Set<Id> oldContactIds) {
        system.debug('*** entering method EnqueueOutboundContactData');
        // set up local, member-level variables for the method        
        Client_Information_Sent_to_Kypera__c log = new Client_Information_Sent_to_Kypera__c();
         KyperaProcessor kp = new KyperaProcessor();     
        Contact newContact;
        Contact oldContact;
        String oldFieldValue = '';
        String newFieldValue = '';
        Boolean requiresUpdate = false;

        List<Contact> newContactList = new List<Contact>(newContacts.values());
        List<Contact> oldContactList = new List<Contact>(oldContacts.values());        
        
        // exit if there is more than one record being updated
        if (newContacts.size() > 1) {
            return;
        }
        
        if (newContacts.size() == 1 && oldContacts.size() == 1) {
            newContact = newContactList.get(0);
            oldContact = oldContactList.get(0);
        }
        
        system.debug('*** newContact ' + newContact);
        system.debug('*** oldContact ' + oldContact);        
        
        // exit if the record does not have a Kypera Housing reference number
        if (String.IsBlank(newContact.Kypera_Reference__c)) {
            return;
        }
        
        // exit if the Contact didn't have a Kypera Housing reference before the update and does now
        if (String.IsBlank(oldContact.Kypera_Reference__c) && !String.IsBlank(newContact.Kypera_Reference__c)) {
            return;
        }    

        // iterate through each field that needs to be synched and check they have changed
        for (Kypera_Contact_Fields__c field : fields) {
           if (!field.Do_Not_Sync__c) {
               newFieldValue = kp.nullToBlank(string.ValueOf(newContact.get(field.Name))); 
               oldFieldValue = kp.nullToBlank(string.ValueOf(oldContact.get(field.Name))); 
               if (newFieldValue != oldFieldValue) {
                   requiresUpdate = true;
                   break;
               }              
           }
        }
        
        // exit if no fields have changed
        if (!requiresUpdate) {
            return;
        }
        
        UpdateKypera(newContactIds, oldContactIds);
        system.debug('*** exiting method EnqueueOutboundContactData');
        
    
    }
    
    // send updates to Kypera asynchronously
    @Future(callout=true)
    global static void updateKypera(Set<Id> newContacts, Set<Id> oldContacts) {
        system.debug('*** entering method updateKypera');     

        Id newContactId = new List<Id>(newContacts).get(0);
        Id oldContactId = new List<Id>(oldContacts).get(0);        
              
        Contact newContact;
        Contact oldContact;
        KyperaProcessor kp = new KyperaProcessor();
        
        Client_Information_Sent_to_Kypera__c log = new Client_Information_Sent_to_Kypera__c();        
        log.Client__c = newContactId;
        log.Operation__c = 'Update';          
        
        try {
            newContact = database.query('SELECT ' + kp.getCSVStringOfFields() + ' FROM Contact WHERE Id =: newContactId');
            oldContact = database.query('SELECT ' + kp.getCSVStringOfFields() + ' FROM Contact WHERE Id =: oldContactId');
        } catch (Exception e) {
            log.Status__c = 'Error';
            log.Error_Message__c = 'An error occurred while processing the response received from Kypera. This error happened in the method updateKypera, in the class KyperaController while retrieving the client record. The error message was ' + e.getMessage();
            insert log;  
            return;      
        }
                         
        String request = '';
        String response = '';
        String hasNoErrors = 'true';

        // build the xmlRequest
        request = kp.buildXmlRequest(newContact, 'Update', 'updatePersonBegin', 'updatePersonEnd');
        
        // call the Kypera web service's UpdatePerson method 
        response = kp.callUpdatePerson(request, log, 'UpdatePersonEndPoint', kp);
        system.debug('### response in updateKypera ' + response);
        // update the log entry 
        log.Status__c = 'Response Received';
        log.Response__c = response;
        insert log;
        hasNoErrors = kp.getErrorFromResponse(response, hasNoErrors);      
                 
        if (hasNoErrors == 'true') {
            // extract the Kypera reference from the response
            // if the response contains the tag '<successful>True</successful>
            if (kp.getSuccessFromResponse(response)) {
                // set the status on the log
                log.Status__c = 'Success';
                update log;
            } else { // if Kypera does not return a success
                log.Status__c = 'Error';
                log.Error_Message__c = 'An error occurred while processing the response received from Kypera. This error happened in the method updateKypera, in the class KyperaController while checking if the response from Kypera reported a success.';
                update log;
            } // if (kp.getSuccessFromResponse(response))  
        } else { // if (hasNoErrors == 'true') 
            log.Status__c = 'Error';
            log.Error_Message__c = 'An error occurred while processing the response received from Kypera. This error happened in the method updateKypera, in the class KyperaController while checking if the response contains an error. See the Response field for the error details.';
            update log;
        } // end if (hasNoErrors == 'true')            
        system.debug('*** exiting method updateKypera');
        
        return;
    }
    
    /* 
        Calls the Kypera webservice's endpoint for the updatePerson method. 
        The serviceEndPointName must be CreatePersonEndPoint
    */
    global string callUpdatePerson(String xmlMsg, Client_Information_Sent_to_Kypera__c logger, String endPoint, KyperaProcessor kp) {
        return callWebServiceEndPoint(xmlMsg, logger, endPoint);
    }

    /* 
        Calls the Kypera webservice's endpoint for the CreatePerson method.
        The serviceEndPointName must be the Kypera metadata custo setting for the CreatePerson endpoint
    */
    global string callCreatePerson(String xmlMsg, Client_Information_Sent_to_Kypera__c logger, String endPoint) {
        return callWebServiceEndPoint(xmlMsg, logger, endPoint);
    }
    
    // calls the Kypera webservice's endpoint and returns the response as a string. The serviceEndPointName must be a valid Kypera metadata custom setting
    global string callWebServiceEndPoint(String xmlToSend, Client_Information_Sent_to_Kypera__c clientLog, String serviceEndPointName)
    {
        String xmlRequest = xmlToSend;
        HttpRequest req = new HttpRequest();
        HTTPResponse res = new HttpResponse();
        //Set HTTPRequest Method
        req.setMethod('POST');
        req.setEndpoint(metadata.get('ServiceHandler').MetaData__c); 
        req.setHeader('Content-Type', 'text/xml; charset=utf-8');
        req.setHeader('SOAPAction', metadata.get(serviceEndPointName).MetaData__c); 

        req.setBody(xmlRequest);
        clientLog.Request__c = xmlRequest;
        system.debug('*** req.getBody: '+ req.getBody());
        Http http = new Http();
        try { 
            if (!Test.isRunningTest()){ 
                res = http.send(req);         
                System.debug('*** res.body: '+ res.getBody());
                System.debug('*** res.getStatus: '+ res.getStatus());
                System.debug('*** res.getStatusCode:'+ res.getStatusCode());   
            } else {
                // return xmlStart(metadata) + nullToBlank(metadata.get('TestResponse1').MetaData__c) + nullToBlank(metadata.get('TestResponse2').MetaData__c) + nullToBlank(metadata.get('TestResponse3').MetaData__c) + nullToBlank(metadata.get('TestResponse4').MetaData__c) + xmlEnd(metadata);
                return xmlStart(metadata) + responseStub + xmlEnd(metadata);
            }
            clientLog.Status__c = 'Request Sent'; 
            return res.getBody();
        } catch(System.CalloutException e) {
            clientLog.Status__c = 'Error';
            clientLog.Error_Message__c = e.GetMessage();
            insert clientLog;
            return '*** ' + e.GetMessage();
        }  
       
    }
    
    // method to redirect to the contact page of a contact. The parameter contactId must be a valid Contact Id
    global PageReference redirectToContact(Id contactId) {
        PageReference pageRef = new PageReference('/' + contactId);        
        return pageRef;   
    }   
    
    // returns true if there is an error
    global string getErrorFromResponse(String response, String errorCheck) {
        string retVal = errorCheck;
        string kyperaResponse = response;
        kyperaResponse = getXMLText(kyperaResponse, 'soap:Body', 0);
        if (kyperaResponse.contains('soap:Fault')) {
            retval = 'false';
            system.debug('*** found error in getErrorFromResponse; response (parameter) is ' + response);
        } else {
            system.debug('*** no error');
        }
        System.debug('*** retval is ' + retval);     
        return retval;
    }  
    
    // returns true if there is a success
    global boolean getSuccessFromResponse(String response) {
        boolean retVal = false;
        string kyperaResponse = response;
        kyperaResponse = getXMLText(kyperaResponse, 'soap:Body', 0);
        if (kyperaResponse.contains('<Successful><![CDATA[True]]></Successful>')) {
            retval = true;
            system.debug('*** no error');
        } else {
            system.debug('*** found error in getSuccessFromResponse; response (parameter) is ' + response);        
        }
        System.debug('*** retval is ' + retval);     
        return retval;
    }   
    
    // gets the text from a given XML tag. It is needed because the response from create person is badly formed and returns encoded versions of < and > 
    global string getXMLText(string xmlString, string tagName, integer position) {
        string retVal = '';
        if(xmlString.contains('<' + tagName + '>') && xmlString.contains('</' + tagName + '>')){
          try{
              retVal = xmlString.substring(xmlString.indexOf('<' + tagName + '>', position) + tagName.length() + 2,
                xmlString.indexOf('</' + tagName + '>', position));   
            }catch (exception e){
                system.debug('*** Exception in method getXMLText ' + e.getMessage() + ' tagName = ' + tagName + ' xmlString = ' + xmlString);
            } 
        }
        return retVal;
    }

    // gets the Kypera Refernce from the response; only call this if getErrorFromResponse returns false
    global string getKeyFromResponse(String response, Client_Information_Sent_to_Kypera__c clientLog, String xmlNodeName) {
        string retVal = '';
        string kyperaResponse = response;
        kyperaResponse = getXMLText(kyperaResponse, 'soap:Body', 0);
        String createPersonXML = '';
        String kyperaReference = '';
        Integer kyperaReferenceInt;
        if (kyperaResponse.contains(xmlNodeName)) {
            createPersonXML = getXMLText(kyperaResponse, xmlNodeName, 0).unescapeHtml4(); // the innerXML for CreatePersonResult contains entities instead of character start and end elements so we have to fix it with unescapeHtml4
            kyperaReference = getXMLText(createPersonXML, 'KyperaReference', 0);
            kyperaReference = kyperaReference.replace('<![CDATA[', '').replace(']]>', '');
        } else { 
            clientLog.Status__c = 'Error';
            clientLog.Error_Message__c = 'An error occurred while processing the response received from Kypera. This error happened in the method getKeyFromResponse, in the class KyperaProcessor while attempting to find the XML node createPersonResult';
            update clientLog;            
        }
        System.debug('*** response (parameter) sent to getKeyFromResponse = ' + response);
        System.debug('*** createPersonXML in getKeyFromResponse = ' + createPersonXML);
        System.debug('*** kyperaReference (return value) in getKeyFromResponse = ' + kyperaReference);
        retval = kyperaReference;
        return retVal;
    }
    
    global string getCSVStringOfFields() {
        string retVal = '';
        // iterate through each field
        for (Kypera_Contact_Fields__c field : fields) {            
            if (!field.Do_Not_Sync__c) {  
                retval += field.Name + ', ';
            }
        }      
        if (retVal.lastIndexOf(', ') > -1) {
            retval = retval.substring(0, retVal.lastIndexOf(', '));
        }        
                 
        return retVal;
    }   
    
}