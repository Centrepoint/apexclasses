global class KyperaUtilities {

    // string comparison method
    global static boolean StringComp(string field1, string field2) {
        boolean retVal = false;
        if (field1 == field2) {
            retVal = true;
        }
        return retVal;
    }    

    // Boolean comparison method
    global static boolean BoolComp(Boolean field1, Boolean field2) {
        boolean retVal = false;
        if (field1 == field2) {
            retVal = true;
        }
        return retVal;
    }  
    
    // Integer comparison method
    global static boolean IntComp(Integer field1, Integer field2) {
        boolean retVal = false;
        if (field1 == field2) {
            retVal = true;
        }
        return retVal;
    }     
    
}