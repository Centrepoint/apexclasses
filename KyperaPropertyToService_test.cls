@IsTest
global class KyperaPropertyToService_test {


    @testSetup static void setup() {
        // create test Kypera Properties
        List<Kypera_Properties__c> KyperaProperties = new List<Kypera_Properties__c>();
        for(Integer i = 0; i < 1000; i++) {
            //if (i < 500) {
                KyperaProperties.add(new Kypera_Properties__c(
                    Scheme_Name__c = 'Property ' + i,
                    Scheme_Reference__c = String.valueOf(i),
                    Block_Name__c = 'Block ' + String.valueOf(i),
                    S_Number__c = String.valueOf(i), 
                    S_Street__c = 'Street ' + String.valueOf(i),
                    S_Town__c = 'Town ' + String.valueOf(i),
                    S_County__c = 'County ' + String.valueOf(i),
                    S_Postcode__c = 'Zip ' + String.valueOf(i),
                    S_Telephone_Number_1__c = 'Phone 1 ' + String.valueOf(i),
                    S_Telephone_Number_2__c = 'Phone 2 ' + String.valueOf(i),
                    S_Inception__c = '01/01/2015',
                    S_Decommision__c = '01/01/2015',
                    S_Housing_Officer__c = 'Data Import',
                    S_Service_Manager__c = 'Data Import',
                    S_SSHO1__c = 'Data Import',
                    S_SSHO2__c = 'Data Import'
                ));
        }
        
        insert KyperaProperties;
        KyperaProperties = [SELECT Id, Scheme_Name__c, Scheme_Reference__c FROM Kypera_Properties__c LIMIT 10000];
        System.AssertEquals(1000, KyperaProperties.size());
        
        // create test Services
        List<Account> accountServices = new List<Account>();
        for(Integer i = 0; i < 1500; i++) {
            if (i < 1001) {
                accountServices.add(new Account(Name = 'Service ' + i, Kypera_Scheme_Code__c = String.ValueOf(i)));  
            } else {
                accountServices.add(new Account(Name = 'Service ' + i));            
            }
        }
        insert accountServices;
        
        // Create a Property Log within 24 hours
        Kypera_Property_Log__c  kpl = new Kypera_Property_Log__c(Import_Stage__c ='5 - Success', Batch_End_Date__c = DateTime.now());
        insert kpl;
        
    }

    // testing bad records with errors and only partial updates
    static testmethod void m1() {
        Test.StartTest();

        List<Account> accountServices = new List<Account>();
        List<Kypera_Properties__c> KyperaProperties = new List<Kypera_Properties__c>();
        KyperaProperties = [SELECT Id, Scheme_Name__c, Scheme_Reference__c FROM Kypera_Properties__c LIMIT 10000];
        Kypera_Property_Log__c kpl = [SELECT Id, Import_Stage__c, Batch_End_Date__c FROM Kypera_Property_Log__c LIMIT 1];

        // add an Service that will later be updated with no name therefore raising an error
        Account badAccount = new Account(Name = 'XYZ Account', Kypera_Scheme_Code__c = 'xyz');
        insert badAccount;
        accountServices = [SELECT Id, Name, Kypera_Scheme_Code__c FROM Account LIMIT 10000];
        
        // add a Property with a Reference but no name (to test validation)
        Kypera_Properties__c badProperty = new Kypera_Properties__c(Scheme_Name__c = '', Scheme_Reference__c = 'xyz');
        insert badProperty;
        
        // test how many records have 'Property' as the start of their name before the update - it should be zero
        Integer rows = [SELECT COUNT() FROM Account WHERE Name LIKE 'Property%'];
        System.AssertEquals(0, rows);
        
        // run the update
        KyperaPropertyToService kpts = new KyperaPropertyToService();
        kpts.doUpdate();
        
        // verify the update worked - there should be 1000 records with 'Property' at the start of their name
        rows = [SELECT COUNT() FROM Account WHERE Name LIKE 'Property%'];
        System.AssertEquals(1000, rows);       
        
        Test.StopTest();
    }
    
    // testing the standard case
    static testmethod void m2() {
        Test.StartTest();

        List<Account> accountServices = new List<Account>();
        List<Kypera_Properties__c> KyperaProperties = new List<Kypera_Properties__c>();
        KyperaProperties = [SELECT Id, Scheme_Name__c, Scheme_Reference__c FROM Kypera_Properties__c LIMIT 10000];   
        Kypera_Property_Log__c kpl = [SELECT Id, Import_Stage__c, Batch_End_Date__c FROM Kypera_Property_Log__c LIMIT 1]; 
        
        // test how many records have 'Property' as the start of their name before the update - it should be zero
        Integer rows = [SELECT COUNT() FROM Account WHERE Name LIKE 'Property%'];
        System.AssertEquals(0, rows);
        
        // run the update
        KyperaPropertyToService kpts = new KyperaPropertyToService();
        kpts.doUpdate();
        
        // verify the update worked - there should be 1000 records with 'Property' at the start of their name
        rows = [SELECT COUNT() FROM Account WHERE Name LIKE 'Property%'];
        System.AssertEquals(1000, rows);
        
        Kypera_Services_Update_Log__c ksul = [SELECT Id, Name, Record_level_errors__c FROM Kypera_Services_Update_Log__c LIMIT 1];
        System.Debug('*** Attempt 2' + ksul);
        
        Test.StopTest();
    }
    
    // testing when no changes need to be made
    static testmethod void m4() {
        Test.StartTest();

        List<Account> accountServices = new List<Account>();
        
        List<Kypera_Properties__c> KyperaPropertiesToDelete = new List<Kypera_Properties__c>();
        KyperaPropertiesToDelete = [SELECT Id, Scheme_Name__c, Scheme_Reference__c FROM Kypera_Properties__c LIMIT 10000];
        delete KyperaPropertiesToDelete;
        
        List<Kypera_Properties__c> KyperaProperties = new List<Kypera_Properties__c>();
        
        
        // create properties with the same name
        for(Integer i = 0; i < 1000; i++) {
            KyperaProperties.add(new Kypera_Properties__c(Scheme_Name__c = 'Service ' + i, Scheme_Reference__c = String.valueOf(i)));
        }
        insert KyperaProperties;        
           
        Kypera_Property_Log__c kpl = [SELECT Id, Import_Stage__c, Batch_End_Date__c FROM Kypera_Property_Log__c LIMIT 1]; 
        kpl.Batch_End_Date__c = DateTime.Now() - 10;
        update kpl;
        
        // test how many records have 'Property' as the start of their name before the update - it should be zero
        Integer rows = [SELECT COUNT() FROM Account WHERE Name LIKE 'Property%'];
        System.AssertEquals(0, rows);
        
        // run the update
        KyperaPropertyToService kpts = new KyperaPropertyToService();
        kpts.doUpdate();
        
        // verify the update worked - there should be 0 records with 'Property' at the start of their name
        rows = [SELECT COUNT() FROM Account WHERE Name LIKE 'Property%'];
        System.AssertEquals(0, rows);
        
        Kypera_Services_Update_Log__c ksul = [SELECT Id, Name, Record_level_errors__c FROM Kypera_Services_Update_Log__c LIMIT 1];
        System.Debug('*** Attempt 3' + ksul);
        
        Test.StopTest();
    }

}