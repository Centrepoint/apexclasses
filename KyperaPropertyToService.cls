/*
    Takes Kypera Scheme information previously imported into Kypera_Properties__c and uses it to 
    update Services with modified Scheme information using records in 
    Kypera_Properties__c with a Scheme_Reference__c that matches the Service's Kypera_Scheme_Code__c
*/
global with sharing class KyperaPropertyToService {

    global static Kypera_Services_Update_Log__c log { get; set; }

    global void doUpdate() {
        // Log: 1 - Update started
        instantiateLog();
        
        // Check if there has been a successful import of properties in the last 32 hours and exist if there hasn't, exit
        DateTime LastSuccessfulBatchDate = [SELECT Batch_End_Date__c  FROM Kypera_Property_Log__c  WHERE Import_Stage__c =: '5 - Success' ORDER BY Batch_End_Date__c DESC LIMIT 1].Batch_End_Date__c;
        DateTime ThirtyTwoHoursAgo = DateTime.Now()-1.5;
        if (LastSuccessfulBatchDate < ThirtyTwoHoursAgo) {
            updateLog('Update_Stage__c', '0 - Failed - No Recent Properties');
            return;
        }
    
        // Construct a 'map' of Scheme References to Properties
        Map<String, Kypera_Properties__c> mainMap = new Map<String, Kypera_Properties__c>();
        for (Kypera_Properties__c k : [SELECT Id, Scheme_Name__c, Scheme_Reference__c, Block_Name__c, 
                                       S_Number__c, S_Street__c, S_Town__c, S_County__c, S_Postcode__c,
                                       S_Telephone_Number_1__c, S_Telephone_Number_2__c, S_Inception__c,
                                       S_Decommision__c, S_Housing_Officer__c, S_Service_Manager__c,Landlord__c,
                                       S_SSHO1__c, S_SSHO2__c, Subtype__c, Type_of_tenancy__c, Ownership__c,Area_Name__c
                                       FROM Kypera_Properties__c LIMIT 10000]) {
            mainMap.put(k.Scheme_Reference__c, k);
        }
        // Log: 2 - Processed properties
        updateLog('Update_Stage__c', '2 - Processed Properties');
        
        // construct a map of Users to Names
        Map<String, Id> userMap = new Map<String, Id>();
        for (User u : [SELECT Id, Name FROM User WHERE IsActive =: true LIMIT 10000]) {
            userMap.put(u.Name, u.Id);
        }
        
        /*
            Loop over every 'Service' (i.e. Account) and if you find a matching 
            Property ( Account.Kypera_Scheme_Code__c == Kypera_Properties__c.Kypera_Scheme_Code__c )
            add this to a list of Accounts to update
        */
        
        // Log: 3 - Processed Services
        updateLog('Update_Stage__c', '3 - Processed Services');
        List<Account> accountsToUpdate = new List<Account>(); // These are the accounts we are writing to the database;  
            
        String accName, accKyperaSchemeCode, accStreet, accTown, accPostcode, accCounty, accPhone, accPhone2;
        String accRegionalManager, accServiceManager, accSSHO, accSSHO2, propertySubType;
        Date accInception, accDecommission;
        Boolean hasChanged;
        try {        
            Kypera_Properties__c KyperaProperty = new Kypera_Properties__c();
            for (Account a : [SELECT Id, Name, Kypera_Scheme_Code__c,
                              BillingStreet, BillingCity, BillingPostalCode, BillingState,
                              Phone, Phone_2__c, Inception__c, Decomission__c, Regional_Manager__c,Owning_Housing_Associations__c,
                              Service_Manager__c, SSHO__c, SSHO_2__c, Service_Type__c, Tenancy_Type__c, Landlord_Name__c
                              FROM Account LIMIT 10000]) {       
                // Within this loop, check any of the fields have changed. If they have, add the record to the list.
                hasChanged = false;
                KyperaProperty = mainMap.get(a.Kypera_Scheme_Code__c); // retrieve the Kypera Property record using the Scheme Code on the Account
                if (KyperaProperty != null) { // if a property has been found, perform the update
                    accName = '';
                    accName = KyperaProperty.Scheme_Name__c;
                    if (a.Name != accName) { // if the Scheme Name has changed, perform the update
                        a.Name = accName;
                        hasChanged = true;
                    }
                    accKyperaSchemeCode = '';
                    accKyperaSchemeCode = KyperaProperty.Scheme_Reference__c;
                    if (a.Kypera_Scheme_Code__c != accKyperaSchemeCode) {
                        a.Kypera_Scheme_Code__c = accKyperaSchemeCode;
                        hasChanged = true;
                    }
                    accStreet = '';
                    if (KyperaProperty.S_Number__c != null) {
                        accStreet = KyperaProperty.S_Number__c;
                    }
                    if (KyperaProperty.S_Street__c != null) {
                        accStreet += ' ' + KyperaProperty.S_Street__c;
                    }
                    if (a.BillingStreet != accStreet) {
                        a.BillingStreet = accStreet;
                        hasChanged = true;
                    }                
                    accTown = '';
                    accTown = KyperaProperty.S_Town__c;
                    if (nullToEmpty(a.BillingCity) != nullToEmpty(accTown)) {
                        a.BillingCity = accTown;
                        hasChanged = true;
                    }
                    accPostcode = '';
                    accPostcode = KyperaProperty.S_Postcode__c;
                    if (nullToEmpty(a.BillingPostalCode) != nullToEmpty(accPostcode)) {
                        a.BillingPostalCode = accPostcode;
                        hasChanged = true;
                    }
                    accCounty = '';
                    accCounty = KyperaProperty.Area_Name__c; //S_County__c
                    if (nullToEmpty(a.BillingState) != nullToEmpty(accCounty)) {
                        a.BillingState = accCounty;
                        hasChanged = true;
                    }
                    accPhone = '';
                    accPhone = KyperaProperty.S_Telephone_Number_1__c;
                    if (nullToEmpty(a.Phone) != nullToEmpty(accPhone)) {
                        a.Phone = accPhone;
                        hasChanged = true;
                    }
                    accPhone2 = '';
                    accPhone2 = KyperaProperty.S_Telephone_Number_2__c;
                    if (nullToEmpty(a.Phone_2__c) != nullToEmpty(accPhone2)) {
                        a.Phone_2__c = accPhone2;
                        hasChanged = true;
                    }
                    accInception = null;
                    if (KyperaProperty.S_Inception__c != null) {
                        accInception = Date.Parse(KyperaProperty.S_Inception__c);
                        if (a.Inception__c != accInception) {
                            a.Inception__c = accInception;
                            hasChanged = true;
                        }
                    } else {
                        a.Inception__c = null;
                        hasChanged = true;
                    }
                    accDecommission = null;
                    if (KyperaProperty.S_Decommision__c != null) {
                        accDecommission = Date.Parse(KyperaProperty.S_Decommision__c);
                        if (a.Decomission__c != accDecommission) {
                            a.Decomission__c = accDecommission;
                            hasChanged = true;
                        }
                    } else {
                        a.Decomission__c = null;
                        hasChanged = true;                    
                    }
                    accRegionalManager = null;
                    if (KyperaProperty.S_Housing_Officer__c != null) {
                        accRegionalManager = userMap.get(KyperaProperty.S_Housing_Officer__c);
                        if (a.Regional_Manager__c != accRegionalManager) {
                            a.Regional_Manager__c = accRegionalManager;
                            hasChanged = true;
                        }
                    } else {
                        a.Regional_Manager__c = null;
                        hasChanged = true;                    
                    }
                    accServiceManager = null;
                    if (KyperaProperty.S_Service_Manager__c != null) {
                        accServiceManager = userMap.get(KyperaProperty.S_Service_Manager__c);
                        if (a.Service_Manager__c != accServiceManager) {
                            a.Service_Manager__c = accServiceManager;
                            hasChanged = true;
                        }
                    } else {
                        a.Service_Manager__c = null;
                        hasChanged = true;                    
                    }
                    accSSHO = null;
                    if (KyperaProperty.S_SSHO1__c != null) {
                        accSSHO = userMap.get(KyperaProperty.S_SSHO1__c);
                        if (a.SSHO__c != accSSHO) {
                            a.SSHO__c = accSSHO;
                            hasChanged = true;
                        }
                    } else {
                        a.SSHO__c = null;
                        hasChanged = true;
                    }
                    accSSHO2 = null;
                    if (KyperaProperty.S_SSHO2__c != null) {
                        accSSHO2 = userMap.get(KyperaProperty.S_SSHO2__c);
                        if (a.SSHO_2__c != accSSHO2) {
                            a.SSHO_2__c = accSSHO2;
                            hasChanged = true;
                        }
                    } else {
                        a.SSHO_2__c = null;
                        hasChanged = true;
                    }
                    propertySubType = null;
                    if (KyperaProperty.SubType__c != null) {
                        propertySubType = KyperaProperty.SubType__c;
                        if (a.Service_Type__c != propertySubType) {
                            a.Service_Type__c = propertySubType;
                            hasChanged = true;
                        }
                    } else {
                        a.Service_Type__c = null;
                        hasChanged = true;
                    }         
                    
                    if (KyperaProperty.Type_of_tenancy__c != null) {
                        
                        if (a.Tenancy_Type__c != KyperaProperty.Type_of_tenancy__c) {
                            a.Tenancy_Type__c = KyperaProperty.Type_of_tenancy__c;
                            hasChanged = true;
                        }
                    } else {
                        a.Tenancy_Type__c = null;
                        hasChanged = true;
                    }         
                    if (KyperaProperty.Ownership__c != null) {
                        
                        if (a.Owning_Housing_Associations__c != KyperaProperty.Ownership__c) {
                            a.Owning_Housing_Associations__c = KyperaProperty.Ownership__c;
                            hasChanged = true;
                        }
                    } else {
                        a.Owning_Housing_Associations__c = null;
                        hasChanged = true;
                    }
                    if (KyperaProperty.Landlord__c != null) {
                        
                        if (a.Landlord_Name__c != KyperaProperty.Landlord__c) {
                            a.Landlord_Name__c = KyperaProperty.Landlord__c;
                            hasChanged = true;
                        }
                    } else {
                        a.Landlord_Name__c = null;
                        hasChanged = true;
                    }
                    // The following block helps avoid a "System.ListException: Duplicate id in list" when more than one field has changed
                    if (hasChanged) { 
                        accountsToUpdate.add(a);
                    }
                }
            }
        } catch (Exception e) {
            updateLog('Update_Stage__c', '3 - Processed Services');
            updateLog('Exception__c', e.getMessage() + ' at line ' + e.getLineNumber() + ' in KyperaPropertyToService.doUpdate().');
            return;
        }
        updateLog('Update_Stage__c', '4 - Prepared Data for Updates');
        
        List<Database.SaveResult> result = new List<Database.SaveResult>();
        try {            
            if (accountsToUpdate.size() > 0) {
                result = Database.update(accountsToUpdate, false);
                updateLog('Update_Stage__c', '5 - Updated services');
            } else {
                updateLog('Update_Stage__c', '6 - No updates performed');
            }   
        } catch (Exception e) {
            updateLog('Update_Stage__c', '9 - Error');
        }
        if (result.size() > 0) {
            integer i = 0;
            integer ii = 1;
            String RecordLevelErrors = '';
            for (Database.SaveResult dsr : result) {
                if (!dsr.IsSuccess()) {
                    RecordLevelErrors = RecordLevelErrors + '#' +ii + '. ' + accountsToUpdate.get(i).Name + ' - ' + dsr.getErrors().get(0).getMessage() + ' ';
                    i++;
                    ii++;
                }
            }    
            if (RecordLevelErrors.length() > 131072) {
                RecordLevelErrors = RecordLevelErrors.substring(0, 131072);
            }
            if (i > 0) {
                RecordlevelErrors = 'Number of errors = ' + i + '. ' + RecordlevelErrors;
                updateLog('Record_level_errors__c', RecordLevelErrors);
                updateLog('Update_Stage__c', '8 - Success with errors');
                updateLog('End_Date__c', datetime.now());
            } else {
                updateLog('Update_Stage__c', '7 - Success with no errors');
                updateLog('End_Date__c', datetime.now());
            }
        }   
    }

    // method to instatiate the log
    global void instantiateLog() {
        log = new Kypera_Services_Update_Log__c();
        log.Start_Date__c = datetime.now();
        log.Update_Stage__c = '1 - Update started';
        insert log;
    }
    
    // method to update the log - string fields
    global void updateLog(String field, string value) {
        log.put(field, value);
        update log;
    }
    
    // method to update the log - date time fields
    global void updateLog(String field, Datetime value) {
        log.put(field, value);
        update log;
    }    

    global string nullToEmpty(String s) {
        if (s == null) {
            return '';
        }
        return s;
    }

}