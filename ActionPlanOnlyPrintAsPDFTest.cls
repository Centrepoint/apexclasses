// Declare that this is a test class
@isTest
// Declare the test class name. This is for the ActionPlanOnlyPrintAsPDFController class
private class ActionPlanOnlyPrintAsPDFTest {
    // Declare a method to create a test record
  private static testMethod void testActionPlanOnlyPrintAsPDFTest() {
      // Fetch the RecordTypeID 'Housing - Keywork Session' from infrm__Action__c Custom Object
    Id devRecordTypeId = Schema.SObjectType.infrm__Action__c.getRecordTypeInfosByName().get('Housing - Keywork Session').getRecordTypeId();
    	// Create a new Record for infrm__Action__c Custom Object and Assign test values to the fields 
      infrm__Action__c ia = new infrm__Action__c(Date_Staff_Member_Signed__c = Date.Today().addDays(-10),
                                                        Date_Young_Person_Signed__c = Date.Today().addDays(-10),
                                                        Staff_Members_Name__c = 'Test Staff Member Name',
                                                        Young_Persons_Name__c = 'Test Young Person NAme',
                                               			// Populate this field with a specific recordId as it is required on the RecordType
                                                 		infrm__Client__c = '0031w00000B47tDAAR',
                                                 		// Use the RecordTypeId 'Housing - Keywork Session' which is 
                                                 		// fetched above
                                                        RecordTypeId = devRecordTypeId);
    insert ia;
      // Create 2 new E-Signature Records and link them to the Action Plan test record
    E_Signature__c es1 = new E_Signature__c(Action_Plan__c = ia.Id);
    E_Signature__c es2 = new E_Signature__c(Action_Plan__c = ia.Id);
    insert es1;
    insert es2;

      // Create a Signature PNG file to represent the CPStaffSignature
    ContentVersion contentVersion1 = new ContentVersion(
            Title = 'CPStaffSignature.png',
            PathOnClient = 'Signature.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
      
    insert contentVersion1;
    ContentDocument CD1 = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument order by createdDate DESC limit 1];
    
      // Add the CPStaffSignature PNG file to the 1st E-Signature record
    ContentDocumentLink cdl1 = New ContentDocumentLink();
    cdl1.LinkedEntityId = es1.id;
    cdl1.ContentDocumentId = CD1.Id;
    cdl1.shareType = 'V';
    insert cdl1;

    ContentVersion contentVersion2 = new ContentVersion(
            Title = 'YPSignature.png',
            PathOnClient = 'Signatue.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
    insert contentVersion2;
    ContentDocument CD2 = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument order by createdDate DESC limit 1];

      // Add the YPSignature PNG file to the 2nd E-Signature record
    ContentDocumentLink cdl2 = New ContentDocumentLink();
    cdl2.LinkedEntityId = es2.id;
    cdl2.ContentDocumentId = CD2.Id;
    cdl2.shareType = 'V';
    insert cdl2;

    Test.startTest();
      // Set the test to run on the ActionPlanOnlyPrintAsPDF Visualforce page as this is
      // the one which ActionPlanOnlyPrintAsPDFController (the class that this script tests) 
      // runs on
    PageReference pageRef = Page.ActionPlanOnlyPrintAsPDF;
    pageRef.getParameters().put('Id', String.valueOf(ia.Id));
    System.Test.setCurrentPage(pageRef);
    ApexPages.currentPage().getParameters().put('Id', ia.Id);

    ApexPages.StandardController sc = new ApexPages.StandardController(ia);
      // Assign the ActionPlanOnlyPrintAsPDFController as the Controller
    ActionPlanOnlyPrintAsPDFController con = new ActionPlanOnlyPrintAsPDFController(sc);
    con.getTitle();
    con.getSMSigUrl();
    con.getYPSigUrl();
    Test.stopTest();
  }
}