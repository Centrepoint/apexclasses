global class KyperaPropertyBatchProcessor implements Database.Batchable<sObject>, Database.AllowsCallouts {

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([SELECT ID, Name FROM Kypera_Metadata__c LIMIT 1]);
    }

    global void execute(Database.BatchableContext BC, List<Kypera_Metadata__c> records) {       

       // import the data
       List<Kypera_Properties__c> prs = [SELECT ID, Name FROM Kypera_Properties__c];
       KyperaPropertyDataProcessor kpdp = new KyperaPropertyDataProcessor();
       kpdp.doImport(prs);

    }   

    global void finish(Database.BatchableContext BC){
        // post-processing  
    }
    
}