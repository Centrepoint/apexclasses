@IsTest
global class KyperaNonNumeric_Test {

    global static testMethod void testNonNumeric() {
        
        Test.StartTest();
        
        // 1. Setup
    
        // turn on integration in the custom setting
        Trigger_Settings__c ts = new Trigger_Settings__c();
        ts.Sync_With_Kypera__c = true;
        insert ts;
                
        // Get record type Ids
        List<RecordType> rTypeList = [SELECT Id, Name, SobjectType FROM RecordType WHERE SobjectType IN ('Account', 'Contact')];
        Map<String, Id> accTypeMap = new Map<String, Id>();
        Map<String, Id> conTypeMap = new Map<String, Id>();
        for (RecordType rt : rTypeList) {
            if (rt.SobjectType == 'Contact') {
                conTypeMap.put(rt.Name, rt.Id);
            }
            if (rt.SobjectType == 'Account') {
                accTypeMap.put(rt.Name, rt.Id);
            }
        }
        
        // Create Kypera Contact Fields data      
        List<Kypera_Contact_Fields__c> fields = new List<Kypera_Contact_Fields__c>();
        fields.add(new Kypera_Contact_Fields__c(Name = 'Salutation', Do_Not_Sync__c = false, Element_Name__c = 'Title', Kypera_Data_Type__c = 'String', Order__c = 10, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = 'FirstName', Do_Not_Sync__c = false, Element_Name__c = 'Forename', Kypera_Data_Type__c = 'String', Order__c = 20, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = 'LastName', Do_Not_Sync__c = false, Element_Name__c = 'Surname', Kypera_Data_Type__c = 'String', Order__c = 25, Operation__c = 'Create; Update'));        
        fields.add(new Kypera_Contact_Fields__c(Name = 'Name', Do_Not_Sync__c = false, Element_Name__c = 'Salutation', Kypera_Data_Type__c = 'String', Order__c = 30, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = 'infrm__Gender__c', Do_Not_Sync__c = false, Element_Name__c = 'Gender', Kypera_Data_Type__c = 'String', Order__c = 40, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = 'infrm__Ethnic_Origin__c', Do_Not_Sync__c = false, Element_Name__c = 'EthnicOrigin', Kypera_Data_Type__c = 'String', Order__c = 50, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = 'Id', Do_Not_Sync__c = false, Element_Name__c = 'ExternalApplicationReference', Kypera_Data_Type__c = 'String', Order__c = 60, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = 'Kypera_Reference__c', Do_Not_Sync__c = false, Element_Name__c = 'KyperaReference', Kypera_Data_Type__c = 'String', Order__c = 65, Operation__c = 'Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = '-', Do_Not_Sync__c = true, Element_Name__c = 'OtherName', Kypera_Data_Type__c = 'String', Order__c = 70, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = 'Birthdate', Do_Not_Sync__c = false, Element_Name__c = 'DateOfBirth', Kypera_Data_Type__c = 'String', Order__c = 80, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = 'infrm__client_status__c', Do_Not_Sync__c = false, Element_Name__c = 'MaritalStatus', Kypera_Data_Type__c = 'String', Order__c = 90, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = 'infrm__Religion__c', Do_Not_Sync__c = false, Element_Name__c = 'ReligionBelief', Kypera_Data_Type__c = 'String', Order__c = 100, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = 'infrm__physical_health_issues__c', Do_Not_Sync__c = false, Element_Name__c = 'MedicalDetails', Kypera_Data_Type__c = 'String', Order__c = 110, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = 'infrm__Disabled_person__c', Do_Not_Sync__c = false, Element_Name__c = 'RegisteredDisabled', Kypera_Data_Type__c = 'Boolean', Order__c = 120, Operation__c = 'Create; Update'));
        fields.add(new Kypera_Contact_Fields__c(Name = '-', Do_Not_Sync__c = true, Element_Name__c = 'SpecialNeeds', Kypera_Data_Type__c = 'String', Order__c = 130, Operation__c = 'Create; Update'));
        insert fields;
        
        fields = [SELECT Id, Name FROM Kypera_Contact_Fields__c];
        Map<String, Id> fieldmap = new Map<String, Id>();
        for (Kypera_Contact_Fields__c kcf : fields) {
            fieldmap.put(kcf.Name, kcf.Id);
        }
        
        // Create Kypera Contact Field Mappings data
        List<Kypera_Contact_Field_Mappings__c> mappings = new List<Kypera_Contact_Field_Mappings__c>();
        mappings.add(new Kypera_Contact_Field_Mappings__c(Kypera_Contact_Field__c = fieldmap.get('infrm__Disabled_person__c'), In_Form_Value__c = 'Yes', Kypera_Value__c = 'true'));
        
        insert mappings;

        // Create Kypera Metadata in the Custom Setting
        List<Kypera_Metadata__c> kms = new List<Kypera_Metadata__c>();
        kms.add(new Kypera_Metadata__c(Name = 'createPersonBegin', Metadata__c = '<CreatePerson xmlns="http://mobile.kypera.com/InformWebService/">'));
        kms.add(new Kypera_Metadata__c(Name = 'createPersonEnd', Metadata__c = '</CreatePerson>'));
        kms.add(new Kypera_Metadata__c(Name = 'soapBodyBegin', Metadata__c = '<soap:Body>'));
        kms.add(new Kypera_Metadata__c(Name = 'soapBodyEnd', Metadata__c = '</soap:Body>'));
        kms.add(new Kypera_Metadata__c(Name = 'soapEnvelopeBegin', Metadata__c = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'));
        kms.add(new Kypera_Metadata__c(Name = 'soapEnvelopeEnd', Metadata__c = '</soap:Envelope>'));
        kms.add(new Kypera_Metadata__c(Name = 'xmlTag', Metadata__c = '<?xml version="1.0" encoding="utf-8"?>'));
        kms.add(new Kypera_Metadata__c(Name = 'ServiceHandler',  Metadata__c = 'http://mobile.kypera.com/InformWebService/ServiceHandler.asmx'));
        kms.add(new Kypera_Metadata__c(Name = 'CreatePersonEndPoint',  Metadata__c = 'http://mobile.kypera.com/InformWebService/CreatePerson'));
        kms.add(new Kypera_Metadata__c(Name = 'UpdatePersonEndPoint',  Metadata__c = 'http://mobile.kypera.com/InformWebService/UpdatePerson'));
        kms.add(new Kypera_Metadata__c(Name = 'updatePersonBegin',  Metadata__c = '<UpdatePerson xmlns="http://mobile.kypera.com/InformWebService/">'));
        kms.add(new Kypera_Metadata__c(Name = 'updatePersonEnd',  Metadata__c = '</UpdatePerson>'));
        insert kms;
        // put these in a map for easy retrieval later on
        Map<String, String> kyperaMetaData = new Map<String, String>();
        for (Kypera_Metadata__c kmsMapItem: kms) {
            kyperaMetaData.put(kmsMapItem.Name, kmsMapItem.Metadata__c);
        }        

        KyperaProcessor.ResponseStub = '<CreatePersonResponse xmlns="http://mobile.kypera.com/InformWebService/"><CreatePersonResult>&lt;?xml version=\'1.0\'?&gt;&lt;Kypera&gt;&lt;Successful&gt;&lt;![CDATA[True]]&gt;&lt;/Successful&gt;&lt;KyperaReference&gt;&lt;![CDATA[XXXXX]]&gt;&lt;/KyperaReference&gt;&lt;/Kypera&gt;</CreatePersonResult></CreatePersonResponse>';


        // Create an Account
        Account acc = new Account(Name = 'Test Account 1', RecordTypeId = accTypeMap.get('Project'));
        insert acc;
                                
        // 2. Test

        /***/
        /*
        Feature: contact data is sent to Kypera and the Kypera Id that is returned is non-numeric
        Scenario: a contact is sent to Kypera; on its return, a bad Id is sent back and a log entry is created
        
        Given that the controller is invoked 
        And the 'sync with Kypera' custom setting is checked 
        And the 'Kypera Contact Fields' custom object has a valid list of fields to be synced
        And the contact is a client
        And there is no Kypera Reference for the client
        
        When the controller is invoked
        
        Then the client is not updated with the Kypera Reference
        And a Client_Information_Sent_to_Kypera__c record is created
        */
        
        
        // Create a Client with the correct Record Type
        Contact con = new Contact();
        con.put('FirstName',  'Test');
        con.put('LastName', 'Contact 1');
        con.put('infrm__Gender__c', 'Male');
        con.put('Salutation','Mr');
        con.put('RecordTypeId', conTypeMap.get('Client'));
        con.put('infrm__NI_number_known__c', 'No');
        con.put('infrm__Client_Group_P__c', 'Rough sleeper');
        con.put('infrm__Sexuality__c', 'Does not wish to disclose');
        con.put('infrm__ex_Armed_forces_personnel_SP__c', 'No');
        con.put('infrm__Source_of_income__c', 'Don\'t know');
        con.put('infrm__Qualify_for_HB__c', 'don\'t know');
        con.put('infrm__Immigration_Status__c', 'Not known');
        insert con;        
        
        // set up the page and its controller
        PageReference pageRef = Page.Sync_With_Kypera;
        Test.setCurrentPage(pageRef);        
        ApexPages.currentPage().getParameters().put('id', con.id);      
        System.debug('con.id: ' + con.id);
        ApexPages.StandardController stdController = new ApexPages.StandardController(con);
        stdController.getRecord();
        KyperaController controller = new KyperaController(stdController);
        controller.CallKypera(); 
        

        // test the contact has not got a Kypera reference
       String query = 'SELECT Id, Kypera_Reference__c FROM Contact WHERE ';
        query += 'FirstName = \'Test\' AND LastName = \'Contact 1\' ';
        query += 'AND infrm__Gender__c = \'Male\' '; 
        query += 'AND Salutation = \'Mr\' '; 
        query += 'AND RecordTypeId = \'' + conTypeMap.get('Client') + '\''; 
        system.debug(query);
        con = database.query(query);
        
        System.Assert(String.Isblank(con.Kypera_Reference__c));
                
        
        // test there is a log entry for the client marked as an error
        Client_Information_Sent_to_Kypera__c cisk = [SELECT Id, Status__c, Response__c FROM Client_Information_Sent_to_Kypera__c WHERE Client__c =: con.Id LIMIT 1];
        System.AssertEquals('Error', cisk.Status__c);
        
        // test the log entry has the response XML
        String expectedResponse = kyperaMetaData.get('xmlTag') + kyperaMetaData.get('soapEnvelopeBegin') + kyperaMetaData.get('soapBodyBegin') + KyperaProcessor.ResponseStub + kyperaMetaData.get('soapBodyEnd') + kyperaMetaData.get('soapEnvelopeEnd');
        System.AssertEquals(expectedResponse, cisk.Response__c);    
        
        Test.StopTest();    
    
    }
    
}