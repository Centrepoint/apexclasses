/*
    Takes Kypera Scheme information previously imported into Kypera_Properties__c and uses it to 
    update Bedspaces with modified Unit information using records in 
    Kypera_Properties__c with a Unit_Reference__c that matches the Bedspace's Kypera_Unit_ID__c
*/
global with sharing class KyperaPropertyToBedspace {

    global static Kypera_Bedspaces_Update_Log__c log { get; set; }
    global static Kypera_Bedspaces_Update_Log_Detail__c detailLog { get; set; }

    global void doUpdate() {
        // Log: 1 - Update started
        instantiateLog();
        
        // Check if there has been a successful import of properties in the last 24 hours and exist if there hasn't, exit
        DateTime LastSuccessfulBatchDate = [SELECT Batch_End_Date__c  
                                            FROM Kypera_Property_Log__c
                                            WHERE Import_Stage__c =: '5 - Success'
                                            ORDER BY Batch_End_Date__c DESC
                                            LIMIT 1].Batch_End_Date__c;
        DateTime ThirtyTwoHoursAgo = DateTime.Now()-1.5;
        
        if (LastSuccessfulBatchDate < ThirtyTwoHoursAgo) {
            updateLog('Update_Stage__c', '0 - Failed - No Recent Properties');
            return;
        }
        
    
        // Construct a 'map' of Unit References to Properties

        Map<String, Kypera_Properties__c> mainMap = new Map<String, Kypera_Properties__c>();
        for (Kypera_Properties__c k : [SELECT Id, 
                                       Unit_Name__c, Unit_Reference__c, Block_Name__c, 
                                       H_Core_Rent__c, Personal_Service_Charge_CORE__c, Service_Charge_CORE__c,
                                       Bedspace_Type__c, Type_of_tenancy__c
                                       FROM Kypera_Properties__c LIMIT 10000]) {
            mainMap.put(k.Unit_Reference__c, k);
        }
        // Log: 2 - Processed properties
        updateLog('Update_Stage__c', '2 - Processed Properties');
        
        /*
            Loop over every Bedspace and if you find a matching 
            Property ( Bedspace.Kypera_Unit_ID__c == Kypera_Properties__c.Kypera_Unit_Reference__c )
            add this to a list of Bedspaces to update
        */
        
        List<infrm__Bedspace__c> bedspacesToRead = new List<infrm__Bedspace__c>(); // These are the Bedspaces we are reading from the database
        for (infrm__Bedspace__c bstr :  [SELECT Id, 
                                         Name, Kypera_Unit_ID__c, infrm__Basic_Rent__c, 
                                         infrm__Personal_Service_Charge__c, infrm__Service_Charge__c,
                                         infrm__Bedspace_type__c, infrm__Type_of_Tenancy__c
                                         FROM infrm__Bedspace__c 
                                         WHERE infrm__Project__r.Kypera_Scheme_Code__c !=: NULL LIMIT 10000]) {
            bedspacesToRead.add(bstr);
        }
        // Log: 3 - Processed Bedspaces
        updateLog('Update_Stage__c', '3 - Processed Bedspaces');
        List<infrm__Bedspace__c> bedspacesToUpdate = new List<infrm__Bedspace__c>(); // These are the Bedspaces we are writing to the database;  
        
        String bsName, propertyType, defaultOccupancyType; 
        Decimal coreRent, personalServiceCharge, serviceCharge;
        Boolean hasChanged;
        try {
            Kypera_Properties__c KyperaProperty = new Kypera_Properties__c();
            for (infrm__Bedspace__c a : bedspacesToRead) {       
                // Within this loop, check any of the fields have changed. If they have, add the record to the list.
                hasChanged = false;
                KyperaProperty = mainMap.get(a.Kypera_Unit_ID__c); // retrieve the Kypera Property record using the Unit Reference on the Bedspace
                if (KyperaProperty != null) { // if a property has been found, perform the update
                    bsName = '';
                    bsName = KyperaProperty.Unit_Name__c;
                    if (a.Name != bsName) { // if the Scheme Name has changed, perform the update
                        a.Name = bsName;
                        hasChanged = true;
                    }
                    propertyType = '';
                    propertyType = KyperaProperty.Bedspace_Type__c;
                    if (a.infrm__Bedspace_type__c != propertyType) {
                        a.infrm__Bedspace_type__c = propertyType;
                        hasChanged = true;
                    }
                    defaultOccupancyType = '';
                    defaultOccupancyType = KyperaProperty.Type_of_tenancy__c;
                    if (a.infrm__Type_of_Tenancy__c != defaultOccupancyType) {
                        a.infrm__Type_of_Tenancy__c = defaultOccupancyType;
                        hasChanged = true;
                    }
                    coreRent = 0;
                    if (KyperaProperty.H_Core_Rent__c != null) {
                        coreRent = KyperaProperty.H_Core_Rent__c;
                    }
                    if (nullToZero(a.infrm__Basic_Rent__c) != nullToZero(coreRent)) {
                        a.infrm__Basic_Rent__c = coreRent;
                        hasChanged = true;
                    }
                    personalServiceCharge = 0;
                    if (KyperaProperty.Personal_Service_Charge_CORE__c != null) {
                        personalServiceCharge = KyperaProperty.Personal_Service_Charge_CORE__c;
                    }
                    if (nullToZero(a.infrm__Personal_Service_Charge__c) != nullToZero(personalServiceCharge)) {
                        a.infrm__Personal_Service_Charge__c = personalServiceCharge;
                        hasChanged = true;
                    }
                    serviceCharge = 0;
                    if (KyperaProperty.Service_Charge_CORE__c != null) {
                        serviceCharge = KyperaProperty.Service_Charge_CORE__c;
                    }
                    if (nullToZero(a.infrm__Service_Charge__c) != nullToZero(serviceCharge)) {
                        a.infrm__Service_Charge__c = serviceCharge;
                        hasChanged = true;
                    }
                    // The following block helps avoid a "System.ListException: Duplicate id in list" when more than one field has changed
                    if (hasChanged) { 
                        bedspacesToUpdate.add(a);
                    }
                }
            }
        } catch(Exception e) {
            updateLog('Update_Stage__c', '3 - Processed Services');
            updateLog('Exception__c', e.getMessage() + ' at line ' + e.getLineNumber() + ' in KyperaPropertyToBedspace.doUpdate().');
            return;            
        }
        updateLog('Update_Stage__c', '4 - Prepared Data for Updates');
        
        List<Database.SaveResult> result = new List<Database.SaveResult>();
        List<Kypera_Bedspaces_Update_Log_Detail__c> logDetails = new List<Kypera_Bedspaces_Update_Log_Detail__c>();
        try {            
            if (bedspacesToUpdate.size() > 0) {
                result = Database.update(bedspacesToUpdate, false);
                updateLog('Update_Stage__c', '5 - Updated services');
            } else {
                updateLog('Update_Stage__c', '6 - No updates performed');
            }   
        } catch (Exception e) {
            updateLog('Update_Stage__c', '9 - Error');
        }
        if (result.size() > 0) {
            integer i = 0;
            String RecordLevelErrors = '';
            for (Database.SaveResult dsr : result) {
                if (!dsr.IsSuccess()) {
                    logDetails.add(new Kypera_Bedspaces_Update_Log_Detail__c(
                        Kypera_Bedspaces_Update_Log__c = log.Id, 
                        Record_level_errors__c = dsr.getErrors().get(0).getMessage(),
                        Bedspace__c = bedspacesToUpdate.get(i).Id
                    ));
                    i++;
                }
            } 
            if (i > 0) {
                RecordlevelErrors = 'Number of attempted updates = ' + bedspacesToUpdate.size() + '; Number of errors = ' + i;
                updateLog('Record_level_errors__c', RecordLevelErrors);
                updateLog('Update_Stage__c', '8 - Success with errors');
                updateLog('End_Date__c', datetime.now());
            } else {
                updateLog('Update_Stage__c', '7 - Success with no errors');
                updateLog('End_Date__c', datetime.now());
            }
            insert logDetails;
        }   
    }

    // method to instatiate the log
    global void instantiateLog() {
        log = new Kypera_Bedspaces_Update_Log__c();
        log.Start_Date__c = datetime.now();
        log.Update_Stage__c = '1 - Update started';
        insert log;
    }
      
    // method to update the log - string fields
    global void updateLog(String field, string value) {
        log.put(field, value);
        update log;
    }
    
    // method to update the log - date time fields
    global void updateLog(String field, Datetime value) {
        log.put(field, value);
        update log;
    }

    global decimal nullToZero(Decimal d) {
        if (d == null) {
            return 0;
        }
        return d;
    }

}