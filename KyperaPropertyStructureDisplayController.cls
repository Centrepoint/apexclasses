global with sharing class KyperaPropertyStructureDisplayController {

    public KyperaPropertyStructureDisplayController() {
        kp = new KyperaProcessor();
        retrievePropertyData();    
    }


    private KyperaProcessor kp;
    global Map<String, String> kpContactData;
    global string request { get; set; } 
    global string response { get; set; }
    global Map<String,String> kpFields { get; set; }
    global Dom.Document domDoc;
    global static String responseStub { get; set; }
    
    // The constructor initialises the contact variable by using the getRecord method from the standard controller.
    /*global KyperaPropertyStructureDisplayController(ApexPages.StandardController stdController) {
        kp = new KyperaProcessor();
        retrievePropertyData();    
    }*/

    global void retrievePropertyData() {
        kp = new KyperaProcessor();
        request = '';
        response = '';
        // populate the request variable with a call to Kypera  
        request += kp.xmlStart(KyperaProcessor.metadata);
        request += KyperaProcessor.metadata.get('GetPropertyStructure').MetaData__c;
        request += kp.xmlEnd(KyperaProcessor.metadata);  
        system.debug(request);        
        response = callWebServiceEndPointString(request, 'GetPropertyStructureXMLEndPoint').replace('\'', '\\\'').replace('\r',' ').replace('\n',' ');
        system.debug(response);       
        return;
    }
    
    // calls the Kypera webservice's endpoint and returns the response as a string. The serviceEndPointName must be a valid Kypera metadata custom setting
    global string callWebServiceEndPointString(String xmlToSend, String serviceEndPointName)
    {
        String xmlRequest = xmlToSend;
        HttpRequest req = new HttpRequest();
        HTTPResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setTimeout(40000);
        req.setEndpoint(KyperaProcessor.metadata.get('ServiceHandler').MetaData__c); 
        system.debug('*** KyperaProcessor.metadata.get(\'ServiceHandler\').MetaData__c (Endpoint) = ' + KyperaProcessor.metadata.get('ServiceHandler').MetaData__c);
        req.setHeader('Content-Type', 'text/xml; charset=utf-8');
        req.setHeader('SOAPAction', KyperaProcessor.metadata.get(serviceEndPointName).MetaData__c); 
        system.debug('*** KyperaProcessor.metadata.get(serviceEndPointName).MetaData__c (SOAPAction) = ' + KyperaProcessor.metadata.get(serviceEndPointName).MetaData__c);
        req.setBody(xmlRequest);
        system.debug('*** req.getBody: (Body) '+ req.getBody());
        Http http = new Http();
        try { 
            if (!Test.isRunningTest()){ 
                res = http.send(req);         
                System.debug('*** res.body: '+ res.getBody());
                System.debug('*** res.getStatus: '+ res.getStatus());
                System.debug('*** res.getStatusCode:'+ res.getStatusCode());   
            } else {
                System.debug('responseStub = ' + responseStub);
                return responseStub;
            }
            return res.getBody();
        } catch(System.CalloutException e) {
            response = e.GetMessage();
            return '';
        }  
       
    }  

}