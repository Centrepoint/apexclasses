public class HousingTimelineEventValidator {
    // This method checks to see the number of timeline events that have the record type name
    // of 'Housing' or 'ORGANISATIONAL LEAD PROJECT' and returns an error message, if there is more than 1.
    public static void validateNumberOfTimelineEvents(List<infrm__TimelineEvents__c> timelineEvents){
        // a map created to map the roll up for number of housing timeline event to the contact Id
        Map<Id, Integer> housingRollUpMap = new Map<Id, Integer>();

        // Set of new timeline event being inserted or updated
        Set<Id> newTE = new Set<Id>();

        // String list of record types for housing timeline
        // To add more record types for the valiation to look for, add below 'housingRecordTypes.add('record type name');'
        Set<String> housingRecordTypes = new Set<String>();
        for(HousingTimelineEventRecordTypes__mdt rtmd : [SELECT MasterLabel, QualifiedApiName FROM HousingTimelineEventRecordTypes__mdt]){
            housingRecordTypes.add(rtmd.MasterLabel);
        }

        for(infrm__TimelineEvents__c te: timelineEvents){
            // setting up the data to query
            if(te.infrm__Client__c != null){
                housingRollUpMap.put(te.infrm__Client__c, 0);
                newTE.add(te.Id);
            }
        }

        // Aggregate result list to aggregate the number of housing timeline event that are current
        List<AggregateResult> aggResult = new List<AggregateResult>();
        
        // Query the count for timeline events that is active and is related to the contact and store it in the aggregate result variable.
        aggResult = [SELECT COUNT(Name)housingCount, infrm__Client__c 
                     FROM infrm__TimelineEvents__c 
                     WHERE Timeline_Status__c =: System.label.HousingTimelineEventStatusValidation
                     AND Id NOT IN :newTE
                     AND infrm__Record_Type_formula__c IN :housingRecordTypes
                     AND infrm__Client__c IN : housingRollUpMap.keySet() Group By infrm__Client__c];
        
        // Go through the aggregate result to get the count for housing timeline event and store it in the housing roll up map
        for(AggregateResult ar : aggResult){
            Id contactId = (ID)ar.get('infrm__Client__c');
            if(housingRollUpMap.containsKey(contactId)){
                // Get the roll up from the aggregate result for a specific contact
                Integer housingTimelineCount = (Integer)ar.get('housingCount');

                // Store the roll up amount with the contact in a map variable
                housingRollUpMap.put(contactId, housingTimelineCount);
            }
        }

        // Go through the new list of timelines that are being created and ensure that an alert is show if there is more than one housing timeline event from the housingRollUpMap.
        for(infrm__TimelineEvents__c te: timelineEvents){
            // Check to see if the timeline event record being created/updated has client populated
            /*
                This is where you would enter the criteria for the validation to fire
            */
            if(te.infrm__Client__c != null && te.infrm__start_date__c != null && te.infrm__end_date__c == null && housingRecordTypes.contains(te.infrm__Record_Type_formula__c)){
                // Check to see if there is a rollUpMap for the timeline event that is being created/edited
                if(housingRollUpMap.containsKey(te.infrm__Client__c)){
                    // If the roll up retrieved any timeline event exluding the current one being creates or updated, then display an error message.
                    if(housingRollUpMap.get(te.infrm__Client__c) >= 1){
                        te.addError(System.Label.HousingTimelineEventValidationErrorMessage);
                        
                    }
                } 
            }
        }
    }
}