@isTest
private class testFamilyStarControllerEnhanced {

    static testMethod void myUnitTest() {
  
      PageReference pageRef = Page.familyStarEnhanced;
      Test.setCurrentPage(pageRef);

      Contact client = new Contact(FirstName = 'Arthur', LastName = 'TestAskey');
      insert client;
      client = [select id from Contact where LastName = 'TestAskey'];
      
      Account project = new Account(name = 'TestStarTest');
      insert project;
      project = [select id from Account where name = 'TestStarTest'];
      
      
      Family_Star__c familystar = new Family_Star__c(Client__c = client.Id, 
            promoting_good_health__c = '6 - Trying',
            meeting_emotional_needs__c = '8 - Finding what works',
            keeping_your_child_safe__c = '6 - Trying',
            social_networks__c = '8 - Finding what works',
            supporting_learning__c = '6 - Trying',
            setting_boundaries__c = '8 - Finding what works',
            keeping_a_family_routine__c = '6 - Trying',
            providing_home_and_money__c = '8 - Finding what works'
          );
      insert familystar;
  
      ApexPages.currentPage().getParameters().put('id', familystar.id);      
      ApexPages.StandardController stdController = new ApexPages.StandardController(familystar);
      stdController.getRecord();
      ApexPages.currentPage().getParameters().put('id', familystar.id);
      familyStarControllerEnhanced controller = new familyStarControllerEnhanced(stdController);
      
      controller.getFilterDates();
      controller.getFamilyStar();
      controller.getFormData();
      controller.getChartTypes();
      controller.action();
      String dd = controller.dateOfPlan;
      String cc = controller.chartType;
      controller.GetAddedPlans();
      ApexPages.currentPage().getParameters().put('addId', familystar.id);
      controller.AddPlan();
      controller.getFormData();
      controller.GetStarDates();
      controller.GetValidDate(System.now());
      //controller.toInteger(supportPlan.MOTIVATION__c);
      controller.formatDate(System.now());
      
      Map<Id, Family_Star__c> filteredPlans = new Map<Id, Family_Star__c>();
      filteredPlans.put(familystar.Id, familystar);
      controller.DefaultStarData(filteredPlans);
      
      ApexPages.currentPage().getParameters().put('removeId', familystar.id);
      controller.RemovePlan();
      controller.GetPlanSize();

    } 
    
}