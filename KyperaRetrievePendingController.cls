global with sharing class KyperaRetrievePendingController {

    private final Kypera_Tenancy__c tenancy { get; set; }
    private KyperaProcessor kp;
    global string request { get; set; } 
    global string response { get; set; } 
    global string displayValue { get; set; }    
    global Map<String,String> kpFields { get; set; }
    global static String responseStub { get; set; }          
    global String kyperaFieldName { get; set; }
    global string inFormFieldName { get; set; }
    
    // The constructor initialises the contact variable by using the getRecord method from the standard controller.
    global KyperaRetrievePendingController(ApexPages.StandardController stdController) {
        this.tenancy = (Kypera_Tenancy__c)stdController.getRecord(); 
        kp = new KyperaProcessor();      
        // call the method that gets contact data from Kypera 
        retrieveTransactionData();
    }
    
    global void retrieveTransactionData() {
        request = '';
        response = '';
        kp = new KyperaProcessor();      
        // build the request to Kypera
        request += kp.xmlStart(KyperaProcessor.metadata);
        request += KyperaProcessor.metadata.get('checkPendingChangeBegin').MetaData__c;                
        request += '<KyperaReference>'+ tenancy.Client__r.Kypera_Reference__c+'</KyperaReference>';
        request += '<UnitReference>' + tenancy.Unit_Reference__c + '</UnitReference>';
        request += '<StartDate>' + tenancy.Kypera_Start_Date__c + '</StartDate>';
        if (!String.IsBlank(tenancy.Kypera_End_Date__c)) {
            request += '<EndDate>' + tenancy.Kypera_End_Date__c + '</EndDate>';        
        }
        request += '<ExternalApplicationReference>' + tenancy.Client__c + '</ExternalApplicationReference>';
        request += KyperaProcessor.metadata.get('checkPendingChangeEnd').MetaData__c;
        request += kp.xmlEnd(KyperaProcessor.metadata);  
        // populate the request variable with a call to Kypera  
        response = callWebServiceEndPoint(request, 'checkPendingChangeEndPoint');   
        displayValue = GetXMLText(response, 'ChangeStatus', 0).replace('<![CDATA[', '').replace(']]>', '');
        return;    
    }
   
    // calls the Kypera webservice's endpoint and returns the response as a string. The serviceEndPointName must be a valid Kypera metadata custom setting
    global string callWebServiceEndPoint(String xmlToSend, String serviceEndPointName)
    {
        String xmlRequest = xmlToSend;
        HttpRequest req = new HttpRequest();
        HTTPResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setEndpoint(KyperaProcessor.metadata.get('ServiceHandler').MetaData__c); 
        system.debug('*** KyperaProcessor.metadata.get(\'ServiceHandler\').MetaData__c = ' + KyperaProcessor.metadata.get('ServiceHandler').MetaData__c);
        req.setHeader('Content-Type', 'text/xml; charset=utf-8');
        req.setHeader('SOAPAction', KyperaProcessor.metadata.get(serviceEndPointName).MetaData__c); 
        system.debug('*** KyperaProcessor.metadata.get(serviceEndPointName).MetaData__c = ' + KyperaProcessor.metadata.get(serviceEndPointName).MetaData__c);
        system.debug('*** serviceEndPointName = ' + serviceEndPointName);
        req.setBody(xmlRequest);
        system.debug('*** req.getBody: '+ req.getBody());
        Http http = new Http();
        try { 
            if (!Test.isRunningTest()){ 
                res = http.send(req);        
                System.debug('*** res.body: '+ res.getBody());
                System.debug('*** res.getStatus: '+ res.getStatus());
                System.debug('*** res.getStatusCode:'+ res.getStatusCode());   
            } else {
                system.debug('*** responseStub ' + responseStub);
                return responseStub;
            }
            return res.getBody();
        } catch(System.CalloutException e) {
            response = e.GetMessage();
            return '';
        }     
    }  
    
    // gets the text from a given XML tag. It is needed because the response from create person is badly formed and returns encoded versions of < and > 
    global string getXMLText(string xmlString, string tagName, integer position) {
        string retVal = '';
        if(xmlString.contains('<' + tagName + '>') && xmlString.contains('</' + tagName + '>')){
          try{
              retVal = xmlString.substring(xmlString.indexOf('<' + tagName + '>', position) + tagName.length() + 2,
                xmlString.indexOf('</' + tagName + '>', position));   
            }catch (exception e){
                system.debug('*** Exception in method getXMLText ' + e.getMessage() + ' tagName = ' + tagName + ' xmlString = ' + xmlString);
            } 
        }
        return retVal;
    }

}