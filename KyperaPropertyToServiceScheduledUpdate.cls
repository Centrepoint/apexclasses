global class KyperaPropertyToServiceScheduledUpdate implements Schedulable {

   global void execute(SchedulableContext SC) {
        KyperaPropertyToServiceBatchProcessor batchApex = new KyperaPropertyToServiceBatchProcessor();
        ID batchprocessid = Database.executeBatch(batchApex);
   }
   
}