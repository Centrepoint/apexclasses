public class KyperaEndTenancyController {

    private final Kypera_Tenancy__c tenancy { get; set; }
    public String unitReference { get; set; }
    public String unitName { get; set; }
    public String KyperaReference { get; set; }
    public Id ExternalApplicationReference { get; set; }
    public String request { get; set; }
    public String response { get; set; }
    public String responseStub { get; set; }
    public String tenancy_end_date_alt { get; set; } 
    public Tenancy_Information_Sent_to_Kypera__c log {get; set; }
    public String userMessage { get; set; } 
    public Boolean displaySuccessIcon { get; set; } 
    public String messageColour { get; set; }
    private Map<String, Kypera_Metadata__c> metadata;

    /*
        constructor
    */
    public KyperaEndTenancyController(ApexPages.StandardController stdController) {
        this.tenancy = (Kypera_Tenancy__c)stdController.getRecord(); 
        KyperaReference = tenancy.Client__r.Kypera_Reference__c;
        ExternalApplicationReference = tenancy.Client__r.Id;
        metadata = new Map<String, Kypera_Metadata__c>();
        metadata = Kypera_Metadata__c.getAll();
    }    

    public PageReference endTenancy() {
        PageReference pageRef;
        displaySuccessIcon = false;
        messageColour = 'red';
        userMessage = '';
        unitReference = tenancy.Unit_Reference__c;
        tenancy_end_date_alt = ApexPages.currentPage().getParameters().get('tenancy_end_date_alt');
        if (tenancy_end_date_alt == null || tenancy_end_date_alt == '' || unitReference == '' || unitReference == null || tenancy.Client__r.Kypera_Reference__c == '' || tenancy.Client__r.Kypera_Reference__c == null) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.Kypera_Tenancy_End_Required_Fields_Message)); // 'The Start Date and Unit Reference must be entered and the client must have a Kypera Reference.'
            System.Debug(' tenancy_end_date_alt = ' + tenancy_end_date_alt + '; unitReference = ' + unitReference + ' tenancy.Client__r.Kypera_Reference__c = ' + tenancy.Client__r.Kypera_Reference__c);
            return null;
        }
        Date startDateToValidate;
        Date endDateToValidate;
        //Date tenancy_start_date = this.tenancy.Start_Date__c;
        Date tenancy_start_date = [SELECT Start_Date__c FROM Kypera_Tenancy__c WHERE Id =: tenancy.Id LIMIT 1].get(0).Start_Date__c;
        String tenancy_end_date = ApexPages.currentPage().getParameters().get('tenancy_end_date');        
        if (!String.IsBlank(tenancy_end_date)) {
            startDateToValidate = tenancy_start_date;// date.parse(tenancy_start_date.replace('-','/'));
            endDateToValidate = date.parse(tenancy_end_date.replace('-','/'));
            if (startDateToValidate > endDateToValidate) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.Kypera_Tenancy_Start_Before_End_Message)); // 'The tenancy start date must be before the end date.'
                System.Debug('*** start date before end date:  tenancy_start_date = ' + tenancy_start_date + '; tenancy_end_date = ' + tenancy_end_date);
                return null;
            }
        }
       
        try {            
            // construct the XML from the data the user entered
            constructEndTenancyXML();
            
            // call the webservice 
            
            response = callWebServiceEndPoint(request, 'EndTenancyEndPoint');    
            
            // start logging
            instantiateLog(); 

            // update the log
            updateLog('Request__c', request);
            updateLog('Response__c', response);
            updateLog('User_Message__c', userMessage);
         
            
            // process results and set the userMessage
            processResults();
            updateLog('Success__c', displaySuccessIcon);
            
            if (userMessage == '' || userMessage == null) {
                userMessage = 'An error has occurred.';
            }
            
            if (userMessage == Label.Kypera_Has_Ended_Tenancy) {
                tenancy.Kypera_End_Date__c = tenancy_end_date_alt;
                update tenancy;
                if (tenancy.Kypera_End_Date__c != null) {
                    pageRef = new PageReference('/'+tenancy.Id);
                    return pageRef;
                }
            }
        } catch (Exception e) {
            userMessage += e.getMessage() + ' An unhandled exception has occurred in KyperaEndTenancyController.endTenancy. ';
            userMessage += 'Please raise a case with In-Form Support with this error message, the date and time the error occurred ';
            userMessage += 'and the technical information below.';
            system.debug('*** exception in KyperaEndTenancyController.endTenancy() = ' + e);
            if (log == null) {
                instantiateLog();
            }            
            updateLog('Error_Message__c', e.getMessage());
            updateLog('User_Message__c', userMessage);
        }
        return null;        
    }

    // 
    public void constructEndTenancyXML() {
        system.debug('*** entering constructEndTenancyXML() ');
        request = '';
        // build the request to Kypera
        system.debug('*** about to set metadata in constructEndTenancyXML() with ' + metadata); 
        request += xmlStart(metadata);
        system.debug('*** set metadata in constructEndTenancyXML() ');
        request += metadata.get('EndTenancyBegin').MetaData__c;
        request += '<KyperaReference>'+tenancy.Client__r.Kypera_Reference__c+'</KyperaReference>';
        system.debug('*** about to set end date in constructEndTenancyXML() ');
        request += '<EndDate>'+tenancy_end_date_alt+'</EndDate>';
        request += '<UnitReference>'+tenancy.Unit_Reference__c+'</UnitReference>';
        request += '<ExternalApplicationReference>'+tenancy.Client__r.Id+'</ExternalApplicationReference>';
        request += metadata.get('EndTenancyEnd').MetaData__c;
        request += xmlEnd(metadata);
        system.debug('*** exiting constructEndTenancyXML() ');
    }    

    // calls the Kypera webservice's endpoint and returns the response as a string. The serviceEndPointName must be a valid Kypera metadata custom setting
    public string callWebServiceEndPoint(String xmlToSend, String serviceEndPointName) {
        String xmlRequest = xmlToSend;
        HttpRequest req = new HttpRequest();
        HTTPResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setEndpoint(metadata.get('ServiceHandler').MetaData__c); 
        system.debug('*** metadata.get(\'ServiceHandler\').MetaData__c = ' + metadata.get('ServiceHandler').MetaData__c);
        req.setHeader('Content-Type', 'text/xml; charset=utf-8');
        req.setHeader('SOAPAction', metadata.get(serviceEndPointName).MetaData__c); 
        system.debug('*** metadata.get(serviceEndPointName).MetaData__c = ' + metadata.get(serviceEndPointName).MetaData__c);
        system.debug('*** serviceEndPointName = ' + serviceEndPointName);
        req.setBody(xmlRequest);
        system.debug('*** req.getBody: '+ req.getBody());
        Http http = new Http();
        try { 
            if (!Test.isRunningTest()){ 
                res = http.send(req); 
                // the next if statement is for debugging
                /*
                if (serviceEndPointName == 'EndTenancyEndPoint') {                
                    responseStub = '';
                    responseStub+= '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"';
                    responseStub+= ' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"';
                    responseStub+= ' xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body>';
                    responseStub+= '<EndTenancyResponse xmlns="http://mobile.kypera.com/InformWebService/"><EndTenancyResult>';
                    responseStub+= '<Kypera xmlns=""><Successful><![CDATA[True]]></Successful></Kypera></EndTenancyResult></EndTenancyResponse></soap:Body></soap:Envelope>';
                    return responseStub;
                }*/
                System.debug('*** res.body: '+ res.getBody());
                System.debug('*** res.getStatus: '+ res.getStatus());
                System.debug('*** res.getStatusCode:'+ res.getStatusCode());   
            } else {
                if (serviceEndPointName == 'EndTenancyEndPoint') {                
                    return responseStub;
                }
            }
            return res.getBody();
        } catch(System.CalloutException e) {
            response = e.GetMessage();
            userMessage += ' ' + e.GetMessage();
            return '';
        }     
    }  

    // create the start of the outerXML 
    public string xmlStart(Map<String, Kypera_Metadata__c> metaDataList) {
        string retVal = '';
        retVal += metaDataList.get('xmlTag').MetaData__c;
        retVal += metaDataList.get('soapEnvelopeBegin').MetaData__c;
        retVal += metaDataList.get('soapBodyBegin').MetaData__c;        
        return retVal;
    }

    // create the end of the outerXML 
    public string xmlEnd(Map<String, Kypera_Metadata__c> metaDataList) {
        string retVal = '';
        retVal += metaDataList.get('soapBodyEnd').MetaData__c;        
        retVal += metaDataList.get('soapEnvelopeEnd').MetaData__c;
        return retVal;
    }

    // gets the text from a XML tag
    public static string getXMLText(string xmlString, string tagName, integer position) {
        string retVal = '';
        if(xmlString.contains('<' + tagName + '>') && xmlString.contains('</' + tagName + '>')){
          try{
              retVal = xmlString.substring(xmlString.indexOf('<' + tagName + '>', position) + tagName.length() + 2,
                xmlString.indexOf('</' + tagName + '>', position));   
            }catch (exception e){
                system.debug('*** Exception in method getXMLText ' + e.getMessage() + ' tagName = ' + tagName + ' xmlString = ' + xmlString);
            } 
        }
        return retVal;
    }

    // process results and sets the userMessage
    public void processResults() {
   
        if (response.contains('<faultstring>')) {
            userMessage = 'An error has occurred. Please contact your In-Form administrator. The error is: ' + getXMLText(response, 'faultstring', 0);
            return;
        } else if (response.contains('<Successful><![CDATA[False]]></Successful>')) {
            userMessage = Label.Kypera_Cannot_End_Tenancy;
            return;
        } else if (response.contains('<Successful><![CDATA[True]]></Successful>')) {
            userMessage = Label.Kypera_Has_Ended_Tenancy;
            messageColour = 'green';
            displaySuccessIcon = true;
            return;
        } else {   
            userMessage = userMessage + ' An unhandled exception has occurred in KyperaEndTenancyController.processResults. Please contact your In-Form administrator.';
        }
    }

    // log methods
    
    // method to instatiate the log
    public void instantiateLog() {
        log = new Tenancy_Information_Sent_to_Kypera__c();
        log.Client__c = tenancy.Client__r.Id;
        insert log;
    }
    
    // method to update the log - string fields
    public void updateLog(String field, string value) {
        log.put(field, value);
        update log;
    }
    
    // method to update the log - boolean fields
    public void updateLog(String field, boolean value) {
        log.put(field, value);
        update log;
    }
    
    // method to update the log - date time fields
    public void updateLog(String field, Datetime value) {
        log.put(field, value);
        update log;
    }        
    

}