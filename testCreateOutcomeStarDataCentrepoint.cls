/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class testCreateOutcomeStarDataCentrepoint {

    static testMethod void myUnitTest() {
    Contact client = new Contact(FirstName = 'Arthur', LastName = 'TestAskey');
    insert client;
    client = [select id from Contact where LastName = 'TestAskey'];
    
    Account project = new Account(name = 'TestStarTest');
    insert project;
    project = [select id from Account where name = 'TestStarTest'];
    
    Outcome_Star__c outcomeStar = new Outcome_Star__c(Client__c = client.Id);
    insert outcomeStar;//test trigger with null values
    outcomeStar = [Select outcomeStarData__c from Outcome_Star__c where Client__c =: client.Id];
    System.assertEquals(outcomeStar.outcomeStarData__c, '0,0,0,0,0,0,0,0,0,0,0|0,0,0,0,0,0,0,0,0,0,0');
    
    outcomeStar.X4_SOCIAL_NETWORKS_RELATIONSHIPS_staff__c = '4';
    update outcomeStar;//test trigger with a value
    outcomeStar = [Select outcomeStarData__c from Outcome_Star__c where Client__c =: client.Id];
    System.assertEquals(outcomeStar.outcomeStarData__c, '0,0,0,4,0,0,0,0,0,0,0|0,0,0,0,0,0,0,0,0,0,0');
    }
}