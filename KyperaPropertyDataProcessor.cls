global class KyperaPropertyDataProcessor {

    private KyperaProcessor kp;
    global string request { get; set; } 
    global string response { get; set; }
    global integer statusCode {get; set; }
    global static String responseStub { get; set; }
    global static List<Kypera_Properties__c> kProperties { get; set; }
    global static Kypera_Property_Log__c log {get; set; }

    // main method to import data
    global void doImport(List<Kypera_Properties__c> records) {
        // ask Kypera for the data
        try {
            retrievePropertyData(); // makes the callout
            instantiateLog(); // this must happen after the log webservice callout has been made
            updateLog('Import_Stage__c', '1 - Retrieve Property Data');
            updateLog('Status_Code__c', statusCode);
            updateLog('Request__c', request);
        } catch (Exception e) {
            if (log == null) {
                instantiateLog();
            }        
            updateLog('Exception__c', e.getMessage());
            return;
        }
        // clean the data, i.e. convert CDATA to raw data
        system.debug('data before cleaning: '+ response);
        try {
            updateLog('Import_Stage__c', '2 - Clean Data');
            cleanCData();
        } catch (Exception e) {
            if (log == null) {
                instantiateLog();
            }        
            updateLog('Exception__c', e.getMessage()); 
            return;      
        }
        system.debug('data after cleaning: '+response);        
        // process the data
        try {
            if (log == null) {
                instantiateLog();
            }        
            updateLog('Import_Stage__c', '3 - Process Data');
            processData();
        } catch (Exception e) {
            if (log == null) {
                instantiateLog();
            }
            updateLog('Exception__c', e.getMessage());    
            return;   
        }
        // delete and insert the property data
        try {
            system.debug('record count before'+records.size());
            updateLog('Import_Stage__c', '4 - Delete and Insert Data');
            deleteAndInsertData(records);
            system.debug('record count after'+records.size());
        } catch (Exception e) {
            if (log == null) {
                instantiateLog();
            }
                updateLog('Exception__c', e.getMessage());   
            return;
        }
        try {
            // if we got this far, log it as a success if the status code is 200
            if (statusCode == 200) {
                updateLog('Import_Stage__c', '5 - Success');
                updateLog('Batch_End_Date__c', datetime.now());
            } else {
                updateLog('Import_Stage__c', '1 - Retrieve Property Data');
                if (response.length() > 32760) {
                    response = response.substring(0, 32760);
                }
                updateLog('Exception__c', response);
                updateLog('Batch_End_Date__c', datetime.now());
            }
        } catch (Exception e) {
                updateLog('Exception__c', e.getMessage());   
            return;
        }
        // set up scheme codes
        try {
            if (deleteAndInsertSchemeCodes()) {
                updateLog('Successfully_Processed_Scheme_Codes__c', true);
            }
        } catch (Exception e) {
            // do nothing - Successfully_Processed_Scheme_Codes__c is false by default
        }
                     
    }    
    
    // method to instatiate the log
    global void instantiateLog() {
        log = new Kypera_Property_Log__c();
        log.Batch_Start_Date__c = datetime.now();
        insert log;
    }
    
    // method to update the log - string fields
    global void updateLog(String field, string value) {
        log.put(field, value);
        update log;
    }
    
    // method to update the log - integer fields
    global void updateLog(String field, integer value) {
        log.put(field, value);
        update log;
    }
    
    // method to update the log - date time fields
    global void updateLog(String field, Datetime value) {
        log.put(field, value);
        update log;
    }    

    // method to update the log - boolean fields
    global void updateLog(String field, Boolean value) {
        log.put(field, value);
        update log;
    } 

    // make the callout
    global void retrievePropertyData() {
        kp = new KyperaProcessor();
        request = '';
        response = '';
        // populate the request variable with a call to Kypera  
        request += kp.xmlStart(KyperaProcessor.metadata);
        request += KyperaProcessor.metadata.get('GetPropertyStructureXML').MetaData__c;
        request += kp.xmlEnd(KyperaProcessor.metadata);  
        system.debug(request);
        response = callWebServiceEndPointString(request, 'GetPropertyStructureXMLEndPoint');
        system.debug(response);   
    }
    
    // delete the records in the list if the kProperties variable has records, 
    global void deleteAndInsertData(List<Kypera_Properties__c> recordsToDelete) {  
        system.debug('*** kProperties.size() = '+kProperties.size() + ' before');
        if (kProperties.size() > 0) {
            delete recordsToDelete;
            insert kProperties;
            system.debug('*** kProperties.size() = '+kProperties.size() + ' after');            
        }
    }
    
    // delete the Kypera Scheme Codes and re-insert them
    global boolean deleteAndInsertSchemeCodes() {
        boolean retVal = false;
        try {
            List<Kypera_Properties__c> kp = new List<Kypera_Properties__c>();
            kp = [SELECT Scheme_Reference__c 
                  FROM Kypera_Properties__c 
                  ORDER BY Scheme_Reference__c
                  LIMIT 10000];
            
            Set<String> schemeCodes = new Set<String>();
            
            for (Kypera_Properties__c k : kp) {
                schemeCodes.add(k.Scheme_Reference__c);
            }    
            
            List<Kypera_Scheme_Codes__c> ksc = new List< Kypera_Scheme_Codes__c>();
            ksc = [SELECT Id FROM Kypera_Scheme_Codes__c LIMIT 10000];
            delete ksc;
            ksc.clear();
            
            Integer i = 1;
            for (String s : schemeCodes) {
                ksc.add(new Kypera_Scheme_Codes__c(Name = s, batchRecordNumber__c = i));
                i++;
            }
            insert ksc;
            retVal = true;
        } catch (Exception e) {
            retVal = false;
            return retVal;
        }
        return retVal;
    }

    
    // Clean up the data by converting CData into raw data
    global void cleanCData() {    
        String encoded = EncodingUtil.urlEncode(response,'UTF-8');
        encoded = encoded.replaceAll('%3C%21%5BCDATA%5B',''); // i.e. '<![CDATA['
        encoded = encoded.replaceAll('%5D%5D%3E',''); // i.e. ']]>'
        encoded = EncodingUtil.urlDecode(encoded,'UTF-8');
        response = encoded;
    }


    // process the data and put it into a list of Kypera_Properties__c
    global void processData() { 
        XmlStreamReader reader = new XmlStreamReader(response);
        System.debug('======response'+response);
        kProperties = new List<Kypera_Properties__c>();
        String SchemeReference;
        String SchemeName;
        String BlockReference;
        String BlockName;
        String UnitReference;
        String UnitName;
        
        boolean isSafeToGetNextXmlElement = true;
        
        while(isSafeToGetNextXmlElement) {
            // Start at the beginning 
            if (reader.getEventType() == XmlTag.START_ELEMENT) {
            
                if ('SchemeReference' == reader.getLocalName()) {
                    if (reader.hasNext()) {
                        reader.next();            
                        SchemeReference = reader.getText();            
                    }
                }
                if ('SchemeName' == reader.getLocalName()) {
                    if (reader.hasNext()) {
                        reader.next();            
                        SchemeName = reader.getText();            
                    }
                }        
                if ('BlockReference' == reader.getLocalName()) {
                    if (reader.hasNext()) {
                        reader.next();            
                        BlockReference = reader.getText();            
                    }                
                }
                if ('BlockName' == reader.getLocalName()) {
                    if (reader.hasNext()) {
                        reader.next();            
                        BlockName = reader.getText();            
                    }                
                }    
                if ('UnitReference' == reader.getLocalName()) {
                    if (reader.hasNext()) {
                        reader.next();            
                        UnitReference = reader.getText();
                    }                       
                }
                if ('UnitName' == reader.getLocalName()) {
                    if (reader.hasNext()) {
                        reader.next();
                        UnitName = reader.getText();
                    }
                    // create the property record
                    Kypera_Properties__c kProperty = new Kypera_Properties__c();
                    kProperty.Scheme_Reference__c = SchemeReference;
                    kProperty.Scheme_Name__c = SchemeName;
                    kProperty.Block_Reference__c = BlockReference;
                    kProperty.Block_Name__c = BlockName;
                    kProperty.Unit_Reference__c = UnitReference;
                    kProperty.Unit_Name__c = UnitName;
                    kProperties.add(kProperty);
                }
            }
            // move to the next element
            if (reader.hasNext()) {
                reader.next();
            } else {
                isSafeToGetNextXmlElement = false;
                break;
            }
        }
    }

    // calls the Kypera webservice's endpoint and returns the response as a string. The serviceEndPointName must be a valid Kypera metadata custom setting
    global string callWebServiceEndPointString(String xmlToSend, String serviceEndPointName)
    {
        String xmlRequest = xmlToSend;
        HttpRequest req = new HttpRequest();
        HTTPResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setTimeout(40000);
        req.setEndpoint(KyperaProcessor.metadata.get('ServiceHandler').MetaData__c); 
        system.debug('*** KyperaProcessor.metadata.get(\'ServiceHandler\').MetaData__c = ' + KyperaProcessor.metadata.get('ServiceHandler').MetaData__c);
        req.setHeader('Content-Type', 'text/xml; charset=utf-8');
        req.setHeader('SOAPAction', KyperaProcessor.metadata.get(serviceEndPointName).MetaData__c); 
        system.debug('*** KyperaProcessor.metadata.get(serviceEndPointName).MetaData__c = ' + KyperaProcessor.metadata.get(serviceEndPointName).MetaData__c);
        system.debug('*** serviceEndPointName = ' + serviceEndPointName);
        req.setBody(xmlRequest);
        system.debug('*** req.getBody: '+ req.getBody());
        Http http = new Http();
        try { 
            if (!Test.isRunningTest()){ 
                res = http.send(req);
                statusCode = res.getStatusCode();         
                System.debug('*** res.body: '+ res.getBody());
                System.debug('*** res.getStatus: '+ res.getStatus());
                System.debug('*** res.getStatusCode:'+ res.getStatusCode());   
            } else {
                System.debug('*** responseStub = ' + responseStub);
                return responseStub;
            }
            return res.getBody();
            //return res.getBodyDocument().toxmlstring();
        } catch(System.CalloutException e) {
            response = e.GetMessage();
            return '';
        }  
    }
}