@IsTest
global class KyperaPropertyToBedspace_test {

    @testSetup static void setup() {
        // create test Kypera Properties
        List<Kypera_Properties__c> KyperaProperties = new List<Kypera_Properties__c>();
        for(Integer i = 0; i < 1000; i++) {
            KyperaProperties.add(new Kypera_Properties__c
                                    (
                                    Scheme_Name__c = 'Property ' + i, 
                                    Scheme_Reference__c = String.valueOf(i),
                                    Unit_Name__c = 'Unit ' + i,
                                    Unit_Reference__c = String.valueOf(i),
                                    H_Core_Rent__c = 100,
                                    P_Water__c = 10,
                                    P_Food__c = 10,
                                    P_Gas__c = 10,
                                    P_Electricity__c = 10,
                                    P_TV_Licence__c = 10,
                                    P_Other_including_toiletries__c = 10,
                                    H_Council_tax__c = 7.5,
                                    H_Cleaning_of_communal_areas_and__c = 7.5,
                                    H_Service_contracts__c = 7.5,
                                    H_Heat_light__c = 7.5,
                                    H_Renewals_and_replacements__c = 7.5,
                                    H_Laundry_facilities__c = 7.5,
                                    Type_of_tenancy__c = 'T Type ' + i,
                                    Bedspace_Type__c = 'B Type ' + i                                   
                                    )
                                );
        }
        
        insert KyperaProperties;
        KyperaProperties = [SELECT Id, Scheme_Name__c, Scheme_Reference__c FROM Kypera_Properties__c WHERE Unit_Name__c LIKE 'Unit%' LIMIT 10000];
        System.AssertEquals(1000, KyperaProperties.size());
        
        // create test Services
        List<Account> accountServices = new List<Account>();
        for(Integer i = 0; i < 1500; i++) {
            if (i < 1001) {
                accountServices.add(new Account(Name = 'Service ' + i, Kypera_Scheme_Code__c = String.ValueOf(i)));  
            } else {
                accountServices.add(new Account(Name = 'Service ' + i));            
            }
        }
        insert accountServices;
        
        // create test Bedspaces
        List<infrm__Bedspace__c> bedspaces = new List<infrm__Bedspace__c>();
        for(Integer i = 0; i < 1500; i++) {
            bedspaces.add(new infrm__Bedspace__c
                             (
                                Name = 'Bedspace ' + i, 
                                Kypera_Unit_ID__c = String.ValueOf(i),
                                infrm__Project__c = accountServices.get(i).Id,
                                infrm__Basic_Rent__c = 100,
                                CORE_Shared_or_Self_Contained__c = 'Shared',
                                CORE_Bedspace_In_Property__c = 'Yes'                                
                             )
                          );
        }
        insert bedspaces;
        
        // Create a Property Log within 24 hours
        Kypera_Property_Log__c  kpl = new Kypera_Property_Log__c(Import_Stage__c ='5 - Success', Batch_End_Date__c = DateTime.now());
        insert kpl;
        
    }

    // testing bad records with errors and only partial updates
    static testmethod void m1() {
        Test.StartTest();

        List<Account> accountServices = new List<Account>();
        List<Kypera_Properties__c> KyperaProperties = new List<Kypera_Properties__c>();
        List<infrm__Bedspace__c> bedspaces = new List<infrm__Bedspace__c>();
        Kypera_Property_Log__c kpl = [SELECT Id, Import_Stage__c, Batch_End_Date__c FROM Kypera_Property_Log__c LIMIT 1];

        // add an Service that will later be updated with no name therefore raising an error
        Account badAccount = new Account(Name = 'XYZ Account', Kypera_Scheme_Code__c = 'xyz');
        insert badAccount;
        accountServices = [SELECT Id, Name, Kypera_Scheme_Code__c FROM Account LIMIT 10000];

        // add a Bedspace that will later be updated with no name therefore raising an error
        infrm__Bedspace__c badBedspace = new infrm__Bedspace__c(
            Name = 'XYZ Bedspace', 
            Kypera_Unit_ID__c = 'abc123#',
            infrm__Project__c = badAccount.Id,
            infrm__Basic_Rent__c = 100,
            CORE_Shared_or_Self_Contained__c = 'Shared',
            CORE_Bedspace_In_Property__c = 'Yes'       
        );
        insert badBedspace;
        bedspaces = [SELECT Name, Kypera_Unit_ID__c FROM infrm__Bedspace__c LIMIT 10000];
        
        // add a Property with a Reference but no name
        Kypera_Properties__c badProperty = new Kypera_Properties__c(
                Scheme_Name__c = 'XYZABC##', 
                Scheme_Reference__c = 'xyz', 
                Unit_Name__c = 'XYZ Bedspace', 
                Unit_Reference__c = 'abc123#',
                H_Core_Rent__c = 0
                );
        insert badProperty;
        KyperaProperties = [SELECT Id, Scheme_Name__c, Scheme_Reference__c, Unit_Name__c, Unit_Reference__c FROM Kypera_Properties__c LIMIT 10000];
               
        // test how many records have 'Unit' as the start of their name before the update - it should be zero
        Integer rows = [SELECT COUNT() FROM infrm__Bedspace__c WHERE Name LIKE 'Unit%'];
        System.AssertEquals(0, rows);
        
        // run the update
        KyperaPropertyToBedspace kptb = new KyperaPropertyToBedspace();
        kptb.doUpdate();
        
        // verify the update worked - there should be 1000 records with 'Unit' at the start of their name
        rows = [SELECT COUNT() FROM infrm__Bedspace__c WHERE Name LIKE 'Unit%'];
        System.AssertEquals(1000, rows);       
        
        Test.StopTest();
    }
    
    // testing the standard case
    static testmethod void m2() {
        Test.StartTest();

        List<Account> accountServices = new List<Account>();
        List<Kypera_Properties__c> KyperaProperties = new List<Kypera_Properties__c>();
        List<infrm__Bedspace__c> bedspaces = new List<infrm__Bedspace__c>();
        Kypera_Property_Log__c kpl = [SELECT Id, Import_Stage__c, Batch_End_Date__c FROM Kypera_Property_Log__c LIMIT 1];

        bedspaces = [SELECT Name, Kypera_Unit_ID__c 
                    FROM infrm__Bedspace__c LIMIT 10000];
        
        KyperaProperties = [SELECT Id, Scheme_Name__c, Scheme_Reference__c, Unit_Name__c, Unit_Reference__c 
                            FROM Kypera_Properties__c LIMIT 10000];
               
        // test how many records have 'Unit' as the start of their name before the update - it should be zero
        Integer rows = [SELECT COUNT() FROM infrm__Bedspace__c WHERE Name LIKE 'Unit%'];
        System.AssertEquals(0, rows);
        
        // run the update
        KyperaPropertyToBedspace kptb = new KyperaPropertyToBedspace();
        kptb.doUpdate();
        
        // verify the update worked - there should be 1000 records with 'Unit' at the start of their name
        rows = [SELECT COUNT() FROM infrm__Bedspace__c WHERE Name LIKE 'Unit%'];
        System.AssertEquals(1000, rows);       
        
        Test.StopTest();
    }

    // testing when no changes need to be made
    static testmethod void m3() {
        Test.StartTest();

        List<Account> accountServices = new List<Account>();
        
        List<Kypera_Properties__c> KyperaPropertiesToDelete = new List<Kypera_Properties__c>();
        KyperaPropertiesToDelete = [SELECT Id, Scheme_Name__c, Scheme_Reference__c FROM Kypera_Properties__c LIMIT 10000];
        delete KyperaPropertiesToDelete;
        
        List<Kypera_Properties__c> KyperaProperties = new List<Kypera_Properties__c>();
        
        
        // create properties with the same name
        for(Integer i = 0; i < 1000; i++) {
            KyperaProperties.add(new Kypera_Properties__c
                (
                    Scheme_Name__c = 'Service ' + i, Scheme_Reference__c = String.valueOf(i),
                    Unit_Name__c = 'Bedspace ' + i,
                    Unit_Reference__c = String.valueOf(i)
                )
            
            );
        }
        insert KyperaProperties;        
           
        Kypera_Property_Log__c kpl = [SELECT Id, Import_Stage__c, Batch_End_Date__c FROM Kypera_Property_Log__c LIMIT 1]; 
        kpl.Batch_End_Date__c = DateTime.Now() - 10;
        update kpl;
        
        // test how many records have 'Unit' as the start of their name before the update - it should be zero
        Integer rows = [SELECT COUNT() FROM infrm__Bedspace__c WHERE Name LIKE 'Unit%'];
        System.AssertEquals(0, rows);
        
        // run the update
        KyperaPropertyToBedspace kptb = new KyperaPropertyToBedspace();
        kptb.doUpdate();
        
        // verify the update worked - there should be 0 records with 'Unit' at the start of their name
        rows = [SELECT COUNT() FROM infrm__Bedspace__c WHERE Name LIKE 'Unit%'];
        System.AssertEquals(0, rows);       
        
        Test.StopTest();
    }

}