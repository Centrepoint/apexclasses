// Declare that this is a test class
@isTest
// Declare the test class name. This is for the MyGoalsPrintAsPDFController class
private class MyGoalsPrintAsPDFControllerTest {
    // Declare a method to create a test record
  private static testMethod void MyGoalsPrintAsPDFControllerTest() {
      // Fetch the RecordTypeID 'My Goals' from My_Goal__c Custom Object
    Id devRecordTypeId = Schema.SObjectType.My_Goal__c.getRecordTypeInfosByName().get('My Goals').getRecordTypeId();
      // Create a new Record for My_Goal__c Custom Object and Assign test values to the fields
      My_Goal__c mg = new My_Goal__c(Completed_date__c = Date.Today().addDays(-10),
                                    Young_Persons_Name__c = 'Test Young Person',
                                    Staff_Members_Name__c = 'Test Staff Member Name',
                                    Date_Young_Person_Signed__c = Date.Today().addDays(-10),
                                    Date_Staff_Member_Signed__c = Date.Today().addDays(-10),
                                    Signed_Electronically__c = 'Yes',

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                              
// Populate this field with a specific Client Id and Timeline Event Id as it is required on the RecordType
// This record ID needs to be updated to link to a record that exists when this class is deployed to a new instance
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
									Client__c = '0031w00000B47tDAAR',
                                    Timeline_Event__c = 'a0j1w000000Ghz9AAC',
                                     
                              		// Use the RecordTypeId sourced above
                                    RecordTypeId = devRecordTypeId);
    insert mg;
      // Create 2 new E-Signature Records and link them to the My Goal test record
    E_Signature__c es1 = new E_Signature__c(My_Goal_Record__c = mg.Id);
    E_Signature__c es2 = new E_Signature__c(My_Goal_Record__c = mg.Id);
    insert es1;
    insert es2;

      // Create a Signature PNG file to represent the CPStaffSignature
    ContentVersion contentVersion1 = new ContentVersion(
            Title = 'CPStaffSignature.png',
            PathOnClient = 'Signature.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
     
    insert contentVersion1;
    ContentDocument CD1 = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument order by createdDate DESC limit 1];
   
      // Add the CPStaffSignature PNG file to the 1st E-Signature record
    ContentDocumentLink cdl1 = New ContentDocumentLink();
    cdl1.LinkedEntityId = es1.id;
    cdl1.ContentDocumentId = CD1.Id;
    cdl1.shareType = 'V';
    insert cdl1;

    ContentVersion contentVersion2 = new ContentVersion(
            Title = 'YPSignature.png',
            PathOnClient = 'Signatue.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
    insert contentVersion2;
    ContentDocument CD2 = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument order by createdDate DESC limit 1];

      // Add the YPSignature PNG file to the 2nd E-Signature record
    ContentDocumentLink cdl2 = New ContentDocumentLink();
    cdl2.LinkedEntityId = es2.id;
    cdl2.ContentDocumentId = CD2.Id;
    cdl2.shareType = 'V';
    insert cdl2;

    Test.startTest();
      // Set the test to run on the MyGoalsPrintAsPDF Visualforce page as this is
      // the one which MyGoalsPrintAsPDFController (the class that this script tests)
      // runs on
    PageReference pageRef = Page.MyGoalsPrintAsPDF;
    pageRef.getParameters().put('Id', String.valueOf(mg.Id));
    System.Test.setCurrentPage(pageRef);
    ApexPages.currentPage().getParameters().put('Id', mg.Id);

    ApexPages.StandardController sc = new ApexPages.StandardController(mg);
      // Assign the MyGoalsPrintAsPDFController as the Controller
    MyGoalsPrintAsPDFController con = new MyGoalsPrintAsPDFController(sc);
    con.getTitle();
    con.getSMSigUrl();
    con.getYPSigUrl();
    Test.stopTest();
  }
}