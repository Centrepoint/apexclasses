global with sharing class KyperaRetrieveController {

    private final Contact con { get; set; }
    private KyperaProcessor kp;
    global string request { get; set; } 
    global string response { get; set; } 
    global string transactions { get; set; }    
    global Map<String,String> kpFields { get; set; }
    global static string responseStub { get; set; }          
    global String kyperaFieldName { get; set; }
    global string inFormFieldName { get; set; }
    
    // The constructor initialises the contact variable by using the getRecord method from the standard controller.
    global KyperaRetrieveController(ApexPages.StandardController stdController) {
        this.con = (Contact)stdController.getRecord(); 
        kp = new KyperaProcessor();      
        // call the method that gets contact data from Kypera 
        retrieveTransactionData();
    }
    
    global void retrieveTransactionData() {
        request = '';
        response = '';
        kp = new KyperaProcessor();      
        // build the request to Kypera
        request += kp.xmlStart(KyperaProcessor.metadata);
        request += KyperaProcessor.metadata.get('GetCurrentTransactionsBegin').MetaData__c;
        request += '<ExternalApplicationReference></ExternalApplicationReference>';
        request += '<KyperaReference>'+con.Kypera_Reference__c+'</KyperaReference>';
        request += '<StartDate>2000-Jan-01</StartDate>'; // hard-coding arbitrarily long start and end dates
        request += '<EndDate>2025-Dec-31</EndDate>';   
        request += KyperaProcessor.metadata.get('GetCurrentTransactionsEnd').MetaData__c;
        request += kp.xmlEnd(KyperaProcessor.metadata);  
        // populate the request variable with a call to Kypera  
        response = callWebServiceEndPoint(request, 'GetCurrentTransactionsEndPoint');   
        transactions = response.replace('\'', '\\\'').replace('\r',' ').replace('\n',' ');
        return;    
    }

    /*
    global void retrievePropertyData() {
        kp = new KyperaProcessor();
        request = '';
        response = '';
        // populate the request variable with a call to Kypera  
        request += kp.xmlStart(KyperaProcessor.metadata);
        request += '<GetPropertyStructureXml xmlns="http://mobile.kypera.com/InformWebService/" />';
        request += kp.xmlEnd(KyperaProcessor.metadata);  
        system.debug(request);
        response = callWebServiceEndPoint(request, 'GetPropertyStructureXML');
        system.debug(response);       
        return;
    }
    */
   
    // calls the Kypera webservice's endpoint and returns the response as a string. The serviceEndPointName must be a valid Kypera metadata custom setting
    global string callWebServiceEndPoint(String xmlToSend, String serviceEndPointName)
    {
        String xmlRequest = xmlToSend;
        HttpRequest req = new HttpRequest();
        HTTPResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setEndpoint(KyperaProcessor.metadata.get('ServiceHandler').MetaData__c); 
        system.debug('*** KyperaProcessor.metadata.get(\'ServiceHandler\').MetaData__c = ' + KyperaProcessor.metadata.get('ServiceHandler').MetaData__c);
        req.setHeader('Content-Type', 'text/xml; charset=utf-8');
        req.setHeader('SOAPAction', KyperaProcessor.metadata.get(serviceEndPointName).MetaData__c); 
        system.debug('*** KyperaProcessor.metadata.get(serviceEndPointName).MetaData__c = ' + KyperaProcessor.metadata.get(serviceEndPointName).MetaData__c);
        system.debug('*** serviceEndPointName = ' + serviceEndPointName);
        req.setBody(xmlRequest);
        system.debug('*** req.getBody: '+ req.getBody());
        Http http = new Http();
        try { 
            if (!Test.isRunningTest()){ 
                res = http.send(req);        
                System.debug('*** res.body: '+ res.getBody());
                System.debug('*** res.getStatus: '+ res.getStatus());
                System.debug('*** res.getStatusCode:'+ res.getStatusCode());   
            } else {
                system.debug('*** ' + responseStub);
                return responseStub;
            }
            return res.getBody();
        } catch(System.CalloutException e) {
            response = e.GetMessage();
            return '';
        }     
    }  
    


}