// Declare that this is a test class
@isTest
// Declare the test class name. This is for the ActionPlanOutcomesStarPDFController class
private class ActionPlanOutcomesStarPrintAsPDFTest {
    // Declare a method to create a test record
  private static testMethod void testActionPlanOutcomesStarPrintAsPDFTest() {
      // Fetch the RecordTypeID 'Action Plan & Outcomes Star' from Homelessness_Outcomes_Star__c Custom Object
    Id devRecordTypeId = Schema.SObjectType.Homelessness_Outcomes_Star__c.getRecordTypeInfosByName().get('Action Plan & Outcomes Star').getRecordTypeId();
    	// Create a new Record for Homelessness_Outcomes_Star__c Custom Object and Assign test values to the fields 
      Homelessness_Outcomes_Star__c apos = new Homelessness_Outcomes_Star__c(Date_Staff_Member_Signed__c = Date.Today().addDays(-10),
                                                        Date_Young_Person_Signed__c = Date.Today().addDays(-10),
                                                        Staff_Members_Name__c = 'Test Staff Member Name',
                                                        Young_Persons_Name__c = 'Test Young Person Name',
                                               			// Populate these field with a specific recordId as it is required on the Page Layout
                                                 		Client__c = '0031w00000B47tDAAR',
                                                        Timeline_Event__c = 'a0j1w000000Ghz9AAC',
                                                        Date__c = Date.Today(),
                                                        // Populate these fields as they are required on the Page Layout                     
                                                        Motivation_and_taking_responsibility__c = '1 - Not interested in talking to any workers or in making change',
                                                        Self_care_and_living_skills__c = '4 - If others can help me look after myself better, I will go along with it',
                                                        Physical_health__c = '3 - Will get help when in pain or discomfort',
                                                        Managing_money_and_personal_admin__c = '2 - Money things are a mess but nothing can be done about it',
                                                        Emotional_and_mental_health__c = '4 - Going along with treatment and feel OK some of the time',
                                                        Social_networks_and_relationships__c = '5 - Found someone I can talk to & trust. Want to address family issues',
                                                        Meaningful_use_of_time__c = '7 - Using my time in a more meaningful and satisfying way',
                                                        Drug_and_alcohol_misuse__c = '3 - Need some help with alcohol and/or drug issues',
                                                        Managing_tenancy_and_accommodation__c = '6 - Starting to do things to keep or get the kind of home I want',   
                                                        Offending__c = '8 - Changing my life to keep within the law',
                                                        Completed_by__c = 'Worker and client together - agreed',
                                                        // Use the RecordTypeId 'Action Plan & Outcomems Star' which is 
                                                 		// fetched above
                                                        RecordTypeId = devRecordTypeId);
    insert apos;
      // Create 2 new E-Signature Records and link them to the Action Plan test record
    E_Signature__c es1 = new E_Signature__c(Action_Plan_and_Outcomes_Star_Record__c = apos.Id);
    E_Signature__c es2 = new E_Signature__c(Action_Plan_and_Outcomes_Star_Record__c = apos.Id);
    insert es1;
    insert es2;

      // Create a Signature PNG file to represent the CPStaffSignature
    ContentVersion contentVersion1 = new ContentVersion(
            Title = 'CPStaffSignature.png',
            PathOnClient = 'Signature.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
      
    insert contentVersion1;
    ContentDocument CD1 = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument order by createdDate DESC limit 1];
    
      // Add the CPStaffSignature PNG file to the 1st E-Signature record
    ContentDocumentLink cdl1 = New ContentDocumentLink();
    cdl1.LinkedEntityId = es1.id;
    cdl1.ContentDocumentId = CD1.Id;
    cdl1.shareType = 'V';
    insert cdl1;

    ContentVersion contentVersion2 = new ContentVersion(
            Title = 'YPSignature.png',
            PathOnClient = 'Signatue.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
    insert contentVersion2;
    ContentDocument CD2 = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument order by createdDate DESC limit 1];

      // Add the YPSignature PNG file to the 2nd E-Signature record
    ContentDocumentLink cdl2 = New ContentDocumentLink();
    cdl2.LinkedEntityId = es2.id;
    cdl2.ContentDocumentId = CD2.Id;
    cdl2.shareType = 'V';
    insert cdl2;

    Test.startTest();
      // Set the test to run on the ActionPlanOnlyPrintAsPDF Visualforce page as this is
      // the one which ActionPlanOutcomesStarPrintAsPDF (the class that this script tests) 
      // runs on
    PageReference pageRef = Page.ActionPlanOnlyPrintAsPDF;
    pageRef.getParameters().put('Id', String.valueOf(apos.Id));
    System.Test.setCurrentPage(pageRef);
    ApexPages.currentPage().getParameters().put('Id', apos.Id);

    ApexPages.StandardController sc = new ApexPages.StandardController(apos);
      // Assign the ActionPlanOutcomesStarPDFController as the Controller
    ActionPlanOutcomesStarPDFController con = new ActionPlanOutcomesStarPDFController(sc);
    con.getTitle();
    con.getSMSigUrl();
    con.getYPSigUrl();
    Test.stopTest();
  }
}