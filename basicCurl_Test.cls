@isTest
public class basicCurl_Test {

    public static testmethod void t1() {
        Test.StartTest();
        BasicCurl bc = new BasicCurl();
        bc.urlToSend = 'https://www.example.com';
        bc.wGet();      
        system.assertequals('<html>test</html>', bc.responseBody);
        system.assertequals(200, bc.responseStatusCode);
        system.assertequals('OK', bc.responseStatus);        
        
        Test.StopTest();
    }

}