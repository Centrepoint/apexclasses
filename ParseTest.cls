public class ParseTest {

    // This holds the text we should parse
    public String textToParse {get; set;}

    // This holds the result of any parsing
    public String parsedText {get; set;}
        
    // This is a list to hold data
    public List<Map<String, String>> datalist { get; set; }
    
    public List<Kypera_Properties__c> kPropertiesToUpdate { get; set; }
    public List<Kypera_Properties__c> kPropertiesToSelect = new List<Kypera_Properties__c>();
    Map<String, Kypera_Properties__c> kPropertiesToSelectMap = new Map<String, Kypera_Properties__c>();
    
    
    // The main method that's called when you click the button
    public PageReference parse() {
        kPropertiesToUpdate = new List<Kypera_Properties__c>();
       for (Kypera_Properties__c k : [SELECT Id, Name, Unit_Reference__c FROM Kypera_Properties__c WHERE Unit_Reference__c !=: null LIMIT 10000]) {
           kPropertiesToSelect.add(k);
           kPropertiesToSelectMap.put(k.Unit_Reference__c, k);
       }
       
       dataList = new List<Map<String, String>>();
       if (textToParse == null) {
         parsedText = 'Nothing to parse';
       } else {
         parsedText = parse(textToParse);
       }
        return null;
    }
    
    // Just checking that it's actually XML
    private String parse(String toParse) {
      DOM.Document doc = new DOM.Document();
      try {
        // add "markers" to identify "end of units"
        toParse = toParse.replace('</unit>', '</unit><endOfUnit>EOF</endOfUnit>');
        
        //
        doc.load(toParse);    
        DOM.XMLNode root = doc.getRootElement();
        parseXMLForSchemes(root);
        processDataList();
        addIds();
        removeNoIds(); 
        return walkThrough(root);
        
      } catch (System.XMLException e) {  // invalid XML
        return e.getMessage();
      }
    }

    // Recursively walk through the XML
    private String walkThrough(DOM.XMLNode node) {
      String result = '\n'; 
      String schemeCode;
      Map<String, String> keyValue = new Map<String, String>();
      if (node.getNodeType() == DOM.XMLNodeType.COMMENT) {
        return 'Comment (' +  node.getText() + ')';
      }
      if (node.getNodeType() == DOM.XMLNodeType.TEXT) {
        return 'Text (' + node.getText() + ')';
      }
      if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
        result += 'Element: ' + node.getName();
        if (node.getText().trim() != '') {
          result += ', text=' + node.getText().trim();
        }     
        
        if (node.getName() == 'housingServiceCharge') {
            result += ' - h';           
        }
        if (node.getName() == 'personalServiceCharge') {
           result += ' - p';
        }
        if (node.getAttributeCount() == 2 && node.getName() == 'charge') {           
          for (Integer i = 0; i< node.getAttributeCount(); i++ ) {
            result += ', attribute #' + i + ':' + node.getAttributeKeyAt(i) + '=' + node.getAttributeValue(node.getAttributeKeyAt(i), node.getAttributeKeyNsAt(i));
          }
        }
        for (Dom.XMLNode child: node.getChildElements()) {
          result += walkThrough(child);
        }
        if (node.getName() == 'endOfUnit') {
          result += '\n';
          System.Debug('*** found 1');
        }
        return result;
      }
      return '';  //should never reach here      
    }
 

    public void parseXMLForSchemes(DOM.XMLNode node) {

      Map<String, String> keyValue = new Map<String, String>();

      if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
        if (node.getText().trim() != '') {
          keyValue.clear();
          keyValue.put(node.getName(), node.getText().trim());
          dataList.add(keyValue);
        }     
        
        if (node.getName() == 'housingServiceCharge') {
            keyValue.clear();
            keyValue.put('housingServiceCharge', String.ValueOf(node.getChildElements().size()));
            dataList.add(keyValue);    
        }
        if (node.getName() == 'personalServiceCharge') {
            keyValue.clear();
            keyValue.put('personalServiceCharge', String.ValueOf(node.getChildElements().size()));
            dataList.add(keyValue);
        }
        if (node.getAttributeCount() == 2 && node.getName() == 'charge') { 
          keyValue.clear();
          keyValue.put(node.getAttributeValue(node.getAttributeKeyAt(0), node.getAttributeKeyNsAt(0)), node.getAttributeValue(node.getAttributeKeyAt(1), node.getAttributeKeyNsAt(1)));
          dataList.add(keyValue);        
        }
        for (Dom.XMLNode child: node.getChildElements()) {
          //System.Debug('### recursing over ' + node.getName());
          parseXMLForSchemes(child);
        }
        if (node.getName() == 'endOfUnit') {
          keyValue.clear();
          keyValue.put('EOF', 'true');
          System.Debug('*** found 2');
        }
        // end 
      }
      //should never reach here      
    }

    public void processDataList() {
        // convert the data into a Kypera Property record
        String unitName, unitReference, propertyType, defaultOccupancyType, ownerShip, propertySubType;
        Decimal hCoreRent, hCleaningOfCommunalAreas, hCouncilTax, hElectricity, hFood, hGas, hHeatAndLight;
        Decimal hLaundryFacilities, hOtherIncludingToiletries, hRenewalsAndReplacements, hServiceContracts, hTVLicence, hWater;
        Decimal pCoreRent, pCleaningOfCommunalAreas, pCouncilTax, pElectricity, pFood, pGas, pHeatAndLight;
        Decimal pLaundryFacilities, pOtherIncludingToiletries, pRenewalsAndReplacements, pServiceContracts, pTVLicence, pWater;
        Boolean hCoreRentProcessed = false, hCleaningOfCommunalAreasProcessed = false, hCouncilTaxProcessed = false;
        Boolean hElectricityProcessed = false, hFoodProcessed = false, hGasProcessed = false, hHeatAndLightProcessed = false;
        Boolean hLaundryFacilitiesProcessed = false, hOtherIncludingToiletriesProcessed = false, hRenewalsAndReplacementsProcessed = false;
        Boolean hServiceContractsProcessed = false, hTVLicenceProcessed = false, hWaterProcessed = false;
        Boolean pCoreRentProcessed = false, pCleaningOfCommunalAreasProcessed = false, pCouncilTaxProcessed = false;
        Boolean pElectricityProcessed = false, pFoodProcessed = false, pGasProcessed = false, pHeatAndLightProcessed = false;
        Boolean pLaundryFacilitiesProcessed = false, pOtherIncludingToiletriesProcessed = false, pRenewalsAndReplacementsProcessed = false;
        Boolean pServiceContractsProcessed = false, pTVLicenceProcessed = false, pWaterProcessed = false;        
        boolean hasHousingServiceCharge = false;
        boolean hasPersonalServiceCharge = false;
        for (Map<String, String> s : dataList) {
            if (s.containsKey('code')) {
                unitReference = s.get('code');
            }
            // Housing Service Charge fields
            if (s.containsKey('housingServiceCharge')) {
                if (s.get('housingServiceCharge') != null && Integer.valueOf(s.get('housingServiceCharge')) > 0) {
                    hasHousingServiceCharge = true;
                }
            }
            if (hasHousingServiceCharge && s.containsKey('Core Rent') && !hCoreRentProcessed) {
                hCoreRent = Decimal.valueOf(s.get('Core Rent'));
                hCoreRentProcessed = true;
            }
            if (hasHousingServiceCharge && s.containsKey('Cleaning of communal areas and') && !hCleaningOfCommunalAreasProcessed) {
                hCleaningOfCommunalAreas = Decimal.valueOf(s.get('Cleaning of communal areas and'));
                hCleaningOfCommunalAreasProcessed = true;
            }
            if (hasHousingServiceCharge && s.containsKey('Council tax') && !hCouncilTaxProcessed) {
                hCouncilTax = Decimal.valueOf(s.get('Council tax'));
                hCouncilTaxProcessed = true;
            }
            if (hasHousingServiceCharge && s.containsKey('Electricity') && !hElectricityProcessed) {
                hElectricity = Decimal.valueOf(s.get('Electricity'));
                hElectricityProcessed = true;
            }
            if (hasHousingServiceCharge && s.containsKey('Food') && !hFoodProcessed) {
                hFood = Decimal.valueOf(s.get('Food'));
                hFoodProcessed = true;
            }
            if (hasHousingServiceCharge && s.containsKey('Gas') && !hGasProcessed) {
                hGas = Decimal.valueOf(s.get('Gas'));
                hGasProcessed = true;
            }
            if (hasHousingServiceCharge && s.containsKey('Heat & light') && !hHeatAndLightProcessed) {
                hHeatAndLight = Decimal.valueOf(s.get('Heat & light'));
                hHeatAndLightProcessed = true;
            }
            if (hasHousingServiceCharge && s.containsKey('Laundry facilities') && !hLaundryFacilitiesProcessed) {
                hLaundryFacilities = Decimal.valueOf(s.get('Laundry facilities'));
                hLaundryFacilitiesProcessed = true;
            }
            if (hasHousingServiceCharge && s.containsKey('Other (including toiletries)') && !hOtherIncludingToiletriesProcessed) {
                hOtherIncludingToiletries = Decimal.valueOf(s.get('Other (including toiletries)'));
                hOtherIncludingToiletriesProcessed = true;
            }
            if (hasHousingServiceCharge && s.containsKey('Renewals and replacements') && !hRenewalsAndReplacementsProcessed) {
                hRenewalsAndReplacements = Decimal.valueOf(s.get('Renewals and replacements'));
                hRenewalsAndReplacementsProcessed = true;
            }
            if (hasHousingServiceCharge && s.containsKey('Service contracts ') && !hServiceContractsProcessed) {
                hServiceContracts = Decimal.valueOf(s.get('Service contracts '));
                hServiceContractsProcessed = true;
            }
            if (hasHousingServiceCharge && s.containsKey('TV Licence') && !hTVLicenceProcessed) {
                hTVLicence = Decimal.valueOf(s.get('TV Licence'));
                hTVLicenceProcessed = true;
            } 
            if (hasHousingServiceCharge && s.containsKey('Water') && !hWaterProcessed) {
                hWater = Decimal.valueOf(s.get('Water'));
                hWaterProcessed = true;
            }              
            
            // Personal Service Charge fields
            if (s.containsKey('personalServiceCharge')) {
                if (s.get('personalServiceCharge') != null && Integer.valueOf(s.get('personalServiceCharge')) > 0) {
                    hasPersonalServiceCharge = true;
                }
            }
            if (hasPersonalServiceCharge && s.containsKey('Core Rent') && !pCoreRentProcessed) {
                pCoreRent = Decimal.valueOf(s.get('Core Rent'));
                pCoreRentProcessed = true;
            }
            if (hasPersonalServiceCharge && s.containsKey('Cleaning of communal areas and') && !pCleaningOfCommunalAreasProcessed) {
                pCleaningOfCommunalAreas = Decimal.valueOf(s.get('Cleaning of communal areas and'));
                pCleaningOfCommunalAreasProcessed = true;
            }  
            if (hasPersonalServiceCharge && s.containsKey('Council tax') && !pCouncilTaxProcessed) {
                pCouncilTax = Decimal.valueOf(s.get('Council tax'));
                pCouncilTaxProcessed = true;
            }
            if (hasPersonalServiceCharge && s.containsKey('Electricity') && !pElectricityProcessed) {
                pElectricity = Decimal.valueOf(s.get('Electricity'));
                pElectricityProcessed = true;
            }
            if (hasPersonalServiceCharge && s.containsKey('Food') && !pFoodProcessed) {
                pFood = Decimal.valueOf(s.get('Food'));
                pFoodProcessed = true;
            }
            if (hasPersonalServiceCharge && s.containsKey('Gas') && !pGasProcessed) {
                pGas = Decimal.valueOf(s.get('Gas'));
                pGasProcessed = true;
            }
            if (hasPersonalServiceCharge && s.containsKey('Heat & light') && !pHeatAndLightProcessed) {
                pHeatAndLight = Decimal.valueOf(s.get('Heat & light'));
                pHeatAndLightProcessed = true;
            }
            if (hasPersonalServiceCharge && s.containsKey('Laundry facilities') && !pLaundryFacilitiesProcessed) {
                pLaundryFacilities = Decimal.valueOf(s.get('Laundry facilities'));
                pLaundryFacilitiesProcessed = true;
            }
            if (hasPersonalServiceCharge && s.containsKey('Other (including toiletries)') && !pOtherIncludingToiletriesProcessed) {
                pOtherIncludingToiletries = Decimal.valueOf(s.get('Other (including toiletries)'));
                pOtherIncludingToiletriesProcessed = true;
            }
            if (hasPersonalServiceCharge && s.containsKey('Renewals and replacements') && !pRenewalsAndReplacementsProcessed) {
                pRenewalsAndReplacements = Decimal.valueOf(s.get('Renewals and replacements'));
                pRenewalsAndReplacementsProcessed = true;
            }
            if (hasPersonalServiceCharge && s.containsKey('Service contracts ') && !pServiceContractsProcessed) {
                pServiceContracts = Decimal.valueOf(s.get('Service contracts '));
                pServiceContractsProcessed = true;
            }  
            if (hasPersonalServiceCharge && s.containsKey('TV Licence') && !pTVLicenceProcessed) {
                pTVLicence = Decimal.valueOf(s.get('TV Licence'));
                pTVLicenceProcessed = true;
            } 
            if (hasPersonalServiceCharge && s.containsKey('Water') && !pWaterProcessed) {
                pWater = Decimal.valueOf(s.get('Water'));
                pWaterProcessed = true;
            }    
            if (s.containsKey('propertyType')) {
                propertyType = s.get('propertyType');
            }
            if (s.containsKey('defaultOccupancyType')) {
                defaultOccupancyType = s.get('defaultOccupancyType');
            }            
            if (s.containsKey('EOF')) { // we have reached the end of a unit so...
                // add it to a list
                kPropertiesToUpdate.add(
                    new Kypera_Properties__c(
                        Unit_Reference__c = unitReference,
                        H_Core_Rent__c = hCoreRent,
                        P_Core_Rent__c = pCoreRent,
                        H_Cleaning_of_communal_areas_and__c = hCleaningOfCommunalAreas,
                        P_Cleaning_of_communal_areas_and__c = pCleaningOfCommunalAreas,
                        H_Council_Tax__c = hCouncilTax,
                        P_Council_Tax__c = pCouncilTax,
                        H_Electricity__c = hElectricity,
                        P_Electricity__c = pElectricity,
                        H_Food__c = hFood,
                        P_Food__c = pFood,
                        H_Gas__c = hGas,
                        P_Gas__c = pGas,
                        H_Heat_light__c = hHeatAndLight,
                        P_Heat_light__c = pHeatAndLight,
                        H_Laundry_facilities__c = hLaundryFacilities,
                        P_Laundry_facilities__c = pLaundryFacilities,
                        H_Other_including_toiletries__c = hOtherIncludingToiletries,
                        P_Other_including_toiletries__c = pOtherIncludingToiletries,
                        H_Renewals_and_replacements__c = hRenewalsAndReplacements,
                        P_Renewals_and_replacements__c = pRenewalsAndReplacements,
                        H_Service_Contracts__c = hServiceContracts,
                        P_Service_Contracts__c = pServiceContracts,
                        H_TV_Licence__c = hTVLicence,
                        P_TV_Licence__c = pTVLicence,
                        H_Water__c = hWater,
                        P_Water__c = pWater,
                        Bedspace_Type__c = propertyType,
                        Type_of_tenancy__c = defaultOccupancyType
                    )
                );
                // reset the variables for the next iteration
                hasHousingServiceCharge = false;
                hasPersonalServiceCharge = false;
                hCoreRent = null;
                pCoreRent = null;
                hCoreRentProcessed = false;
                pCoreRentProcessed = false;
                hCleaningOfCommunalAreas = null;
                pCleaningOfCommunalAreas = null;
                hCleaningOfCommunalAreasProcessed = false;
                pCleaningOfCommunalAreasProcessed = false;
                hCouncilTax = null;
                pCouncilTax = null;
                hCouncilTaxProcessed = false;
                pCouncilTaxProcessed = false;
                hElectricity = null;
                pElectricity = null;
                hElectricityProcessed = false;
                pElectricityProcessed = false;
                hFood = null;
                pFood = null;
                hFoodProcessed = false;
                pFoodProcessed = false;
                hGas = null;
                pGas = null;
                hGasProcessed = false;
                pGasProcessed = false;
                hHeatAndLight = null;
                pHeatAndLight = null;
                hHeatAndLightProcessed = false;
                pHeatAndLightProcessed = false;
                hLaundryFacilities = null;
                pLaundryFacilities = null;
                hLaundryFacilitiesProcessed = false;
                pLaundryFacilitiesProcessed = false;
                hOtherIncludingToiletries = null;
                pOtherIncludingToiletries = null;
                hOtherIncludingToiletriesProcessed = false;
                pOtherIncludingToiletriesProcessed = false;
                hRenewalsAndReplacements = null;
                pRenewalsAndReplacements = null;
                hRenewalsAndReplacementsProcessed = false;
                pRenewalsAndReplacementsProcessed = false;
                hServiceContracts = null;
                pServiceContracts = null;
                hServiceContractsProcessed = false;
                pServiceContractsProcessed = false;
                hTVLicence = null;
                pTVLicence = null;
                hTVLicenceProcessed = false;
                pTVLicenceProcessed = false;
                hWater = null;
                pWater = null;
                hWaterProcessed = false;
                pWaterProcessed = false;
                propertyType = null;
                defaultOccupancyType = null;
            } // if (s.containsKey('EOF'))
        }// end for loop  
    }
    
    public void addIds() {
        for (Kypera_Properties__c kx: kPropertiesToUpdate) {
            kx.Id = kPropertiesToSelectMap.get(kx.Unit_Reference__c).Id;
        }
    }
    
    public void removeNoIds() {
        Integer i = 0;
        List<Integer> idsToRemove = new List<Integer>();
        for (Kypera_Properties__c kx: kPropertiesToUpdate) {
            if (kx.Id == null) {
                idsToRemove.add(i);
            }
            i++;
        }
        for (Integer n : idsToRemove) {
            kPropertiesToUpdate.remove(n);
        }      
    }
    
    public string unitXML(String xml) {
        String returnValue = '';
        returnValue = xml.substringBetween('<Units>','</Units>');
        returnValue = '<Units>' + returnValue + '</Units>';
        return returnValue;
    }
     
}