public class KyperaCreateTenancyStagedController {

    private final Contact con { get; set; }
    public String schemeReference { get; set; }
    public String unitReference { get; set; }
    public String unitName { get; set; }
    public Id ExternalApplicationReference { get; set; }
    public List<Kypera_Properties__c> kProperties { get; set; }
    public String request { get; set; }
    public String response { get; set; }
    public String responseStub { get; set; }
    public String tenancy_start_date_alt { get; set; }
    public String tenancy_end_date_alt { get; set; } 
    public String tenancy_start_date_display { get; set; }
    public Tenancy_Information_Sent_to_Kypera__c log {get; set; }
    public String userMessage { get; set; } 
    public String messageColour { get; set; }
    public Boolean displaySuccessIcon { get; set; } 
    public String KyperaReference { get; set; }
    private Map<String, Kypera_Metadata__c> metadata;
    public Kypera_Tenancy__c tenancy { get; set; }
    public List<te> teList { get; set; }
    public Id orgLeadRecordTypeId { get; set; }
    public String orgLeadRecordTypeName { get; set; }
    Public KyperaProcessor kp;
    Public String timelineEventId { get; set; }
    Public Integer timelineTenancyCount { get; set; }
    Public boolean displayStage1 { get; set; }
    Public boolean displayStage2 { get; set; }
    Public boolean displayStage3 { get; set; }
    Set<String> timelineEventRecordTypeName = new Set<String>();
    

    public KyperaCreateTenancyStagedController(ApexPages.StandardController stdController) {
        toggleDisplay(false, false, false);
        this.con = (Contact) stdController.getRecord(); 
        kp = new KyperaProcessor();
        KyperaReference = con.Kypera_Reference__c;
        ExternalApplicationReference = con.Id; // get the client id
        metadata = new Map<String, Kypera_Metadata__c>();
        metadata = Kypera_Metadata__c.getAll();
        /*
            12/06/2019 -- Removed 'ORGANISATION LEAD PROJECT' filter from timeline event query, thereforce the record type name and id variable is not required.
            orgLeadRecordTypeName = 'ORGANISATION LEAD PROJECT';
            orgLeadRecordTypeId = setupOrgLeadRecordType();
        */

        /*
            18/06/2019 - Update
            Added a set of string with the record type names that are being pulled from the custom metadata.
            This is because, if there needs to be a change of record type in the timeline event query, this change can be done outside of code.
        */
        for(HousingTimelineEventRecordTypes__mdt rtmd : [SELECT MasterLabel, QualifiedApiName FROM HousingTimelineEventRecordTypes__mdt]){
            timelineEventRecordTypeName.add(rtmd.MasterLabel);
        }
        
        // set up the 'list' of timeline events. There should only ever be one current OLP timeline event
        this.teList = setupTeList(); 
        // if the client has no current housing timelines, display an error and exit
        if (teList.size() == 0) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'There are no Current Timeline Events linked to a Castleton Service'));
            return;
        }
        // as there is only one current OLP timeline, the start date can be retrieved via the first timeline event on the list. (If there is more than one the VF page won't show the list of Timeline Events).
        timelineEventId = teList.get(0).recordId;
        // set Tenancy Start Date
        if (setUpTenancyStartDate()) {
            displayStage1 = true;
        }
    }    
    
    // retrieves the curent OLP Timeline Events for the client 
    // should only be 1 as a client can only ever have one current OLP timeline
    
    /*
        12/06/2019 removed the record type filter from the timeline events query below in order for the visual force page to return time line events of any record type.
    */
    /*
        18/06/2019 - Update
        Adding the set of record type names in the timeline event filter to query by. This set is used as it sits on the custom metadata settings and can be changed without needing to change the code.
    */
    public List<te> setupTeList() {
        teList = new List<te>();
        for (infrm__TimelineEvents__c t : [SELECT Id, Name, infrm__start_date__c, infrm__Lead_Project2__r.Name, infrm__status__c,Timeline_Status__c, RecordType.Name, infrm__Lead_Project2__r.Kypera_Scheme_Code__c
                                           FROM infrm__TimelineEvents__c 
                                           WHERE infrm__Client__c =: ExternalApplicationReference 
                                           AND infrm__end_date__c =: NULL
                                           AND Timeline_Status__c =: System.label.HousingTimelineEventStatusValidation
                                           AND RecordType.Name IN :timelineEventRecordTypeName
                                           AND infrm__Lead_Project2__r.Service_Type__c NOT IN 
                                           (
                                               'Subcontracted – Floating Support',
                                               'Subcontracted – Hostel', 
                                               'Subcontracted – Housing Support', 
                                               'Support only – non-accommodation based'
                                           )
                                           AND infrm__Lead_Project2__r.Kypera_Scheme_Code__c !=: NULL
                                           ORDER BY Name 
                                           LIMIT 1
                                          ]) {
            teList.Add(new te(t.Id, t.Name, kp.nullToBlank(string.ValueOf((t.infrm__start_date__c))), t.infrm__Lead_Project2__r.Name, t.Timeline_Status__c, t.RecordType.Name, t.infrm__Lead_Project2__r.Kypera_Scheme_Code__c));
        }
        system.debug(teList.size());
        return teList;
    }
    
    // return the record type id for the 'ORGANISATION LEAD PROJECT'
    /*
    12/06/2019 -- removed organisation lead project record type filter from the timeline event query, therforce it is not required to retrieve the record type Id. 
    public Id setupOrgLeadRecordType() {
        Id recordTypeId;
        recordTypeId = [SELECT Id FROM RecordType WHERE SObjectType =: 'infrm__TimelineEvents__c' AND Name =: orgLeadRecordTypeName LIMIT 1].Id;
        return recordTypeId;
    }
    */
    
    // sets up the Tenancy Start Date and returns true if it could be set up; otherwise it displays an error
    public boolean setUpTenancyStartDate() {
        // get the number of tenancies at the current OLP Timeline Event
        timelineTenancyCount = 0;
        timelineTenancyCount = [SELECT COUNT() FROM Kypera_Tenancy__c WHERE Timeline_Event__c =: timelineEventId];    
        // If the Client has any Tenancies without an End Date, you will not be able to navigate to this page but, just in case, we add an error.
        List<Kypera_Tenancy__c> openTenancies = [SELECT Name FROM Kypera_Tenancy__c WHERE Timeline_Event__c =: timelineEventId AND End_Date__c =: null ORDER BY Start_Date__c DESC LIMIT 1];
        if (openTenancies.size() > 0) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Tenancy ' + openTenancies.get(0).Name + ' is open.'));
            return false;        
        }
        // If the Timeline Event has no Tenancies, the date will be the Start Date of the Timeline Event. 
        if (timelineTenancyCount == 0) {
            tenancy_start_date_alt = teList.get(0).timelineEventStartDate;
            tenancy_start_date_display = tenancy_start_date_alt;
            return true;
        }
        // If the Timeline Event has Tenancies, it will be the day after the end date of the last Tenancy. 
        if (timelineTenancyCount > 0) {
            AggregateResult[] groupedResults  = [SELECT Max(End_Date__c) h FROM Kypera_Tenancy__c WHERE Timeline_Event__c =: timelineEventId];
            Object maxStartDate = groupedResults[0].get('h');
            Date maximumStartDate = (Date) maxStartDate;
            tenancy_start_date_alt = string.ValueOf(maximumStartDate.addDays(1));
            tenancy_start_date_display = tenancy_start_date_alt;
            return true;
        }
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Something went wrong while retrieving the Tenancy Start Date. Please contact your In-Form System Administrator. This error happened in KyperaCreateTenancyStagedController.setUpTenancyStartDate()'));          
        return false;        
    }
    
    // toggle the variables used to decide whether to display parts of the interface
    public void toggleDisplay(boolean state1, boolean state2, boolean state3) {
        displayStage1 = state1;
        displayStage2 = state2;
        displayStage3 = state3;    
    }


    /*
        Check whether properties are available by calling GetVacantProperties
    */
    public PageReference checkAvailability() {
        try {
            // 1. Check that the Tenancy Start Date and Scheme Reference is set
            if (String.IsBlank(tenancy_start_date_alt) && String.IsBlank(schemeReference)) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select a Timeline Event with a Tenancy Start Date and Kypera Scheme Code.'));
                return null;
            }
            // 2. Prepare the XML
            constructCheckAvailablePropertiesXML();
            
            // 3. Make the callout
            response = callWebServiceEndPoint(request, 'GetVacantPropertiesEndPoint');    
            
            // 4. Process the response
            processAvailablePropertyData();
            
            
        } catch (Exception e) {
            String userMessage = e.getMessage() + ' An unhandled exception has occurred in KyperaCreateTenancyStagedController.CheckAvailability. ';
            userMessage += 'Please contact your In-Form System Administrator with this error message, the date and time the error occurred ';
            userMessage += 'and the technical information below.';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,userMessage));
            system.debug('*** exception in KyperaCreateTenancyStagedController.checkAvailability() = ' + e);
            if (log == null) {
                instantiateLog();
            }
            updateLog('Error_Message__c', e.getMessage());
            updateLog('User_Message__c', userMessage);

        }        
        
        return null;
    }

    
    // process the data and put it into a list of properties to display
    public void processAvailablePropertyData() { 
        System.debug('*** entering processAvailablePropertyData');
        System.debug('*** response = ' + response);
        XmlStreamReader reader = new XmlStreamReader(response);
        kProperties = new List<Kypera_Properties__c>();
        String SchemeReference;
        String SchemeName;
        String BlockReference;
        String BlockName;
        String UnitReference;
        String UnitName;

        boolean isSafeToGetNextXmlElement = true;
        
        while(isSafeToGetNextXmlElement) {
            // Start at the beginning 
            if (reader.getEventType() == XmlTag.START_ELEMENT) {
            
                if ('SchemeReference' == reader.getLocalName()) {
                    if (reader.hasNext()) {
                        reader.next();            
                        SchemeReference = reader.getText();            
                    }
                }
                if ('SchemeName' == reader.getLocalName()) {
                    if (reader.hasNext()) {
                        reader.next();            
                        SchemeName = reader.getText();            
                    }
                }        
                if ('BlockReference' == reader.getLocalName()) {
                    if (reader.hasNext()) {
                        reader.next();            
                        BlockReference = reader.getText();            
                    }                
                }
                if ('BlockName' == reader.getLocalName()) {
                    if (reader.hasNext()) {
                        reader.next();            
                        BlockName = reader.getText();            
                    }                
                }    
                if ('UnitReference' == reader.getLocalName()) {
                    if (reader.hasNext()) {
                        reader.next();            
                        UnitReference = reader.getText();
                    }                       
                }
                if ('UnitName' == reader.getLocalName()) {
                    if (reader.hasNext()) {
                        reader.next();
                        UnitName = reader.getText();
                    }
                    // create the property record
                    Kypera_Properties__c kProperty = new Kypera_Properties__c();
                    kProperty.Scheme_Reference__c = SchemeReference;
                    kProperty.Scheme_Name__c = SchemeName;
                    kProperty.Block_Reference__c = BlockReference;
                    kProperty.Block_Name__c = BlockName;
                    kProperty.Unit_Reference__c = UnitReference;
                    kProperty.Unit_Name__c = UnitName;
                    kProperties.add(kProperty);
                }
            }
            // move to the next element
            if (reader.hasNext()) {
                reader.next();
            } else {
                isSafeToGetNextXmlElement = false;
                break;
            }
        }
        displayStage2 = true;
        System.debug('*** exiting processAvailablePropertyData');
        System.Debug('*** ' + kProperties);    
    }

    public PageReference createTenancy() {
        PageReference pageRef;
        displaySuccessIcon = false;
        messageColour = 'red';
        userMessage = '';
        tenancy_end_date_alt = ApexPages.currentPage().getParameters().get('tenancy_end_date_alt');
        Date startDateToValidate;
        Date endDateToValidate;
        String tenancy_start_date = tenancy_start_date_alt.substring(8, 10) + '/' + tenancy_start_date_alt.substring(5, 7) + '/' + tenancy_start_date_alt.substring(0, 4);
        String tenancy_end_date = ApexPages.currentPage().getParameters().get('tenancy_end_date');
             
        if (tenancy_start_date_alt == null || tenancy_start_date_alt == '' || unitReference == '' || unitReference == null || con.Kypera_Reference__c == '' || con.Kypera_Reference__c == null) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.Kypera_Tenancy_Required_Fields_Message)); // 'The Start Date and Unit Reference must be entered and the client must have a Kypera Reference.'
            System.Debug(' tenancy_start_date_alt = ' + tenancy_start_date_alt + '; unitReference = ' + unitReference + ' con.Kypera_Reference__c = ' + con.Kypera_Reference__c);
            return null;
        }
        if (!String.IsBlank(tenancy_end_date) && !String.IsBlank(tenancy_end_date)) {
            startDateToValidate = date.parse(tenancy_start_date.replace('-','/'));
            endDateToValidate = date.parse(tenancy_end_date.replace('-','/'));
            if (startDateToValidate > endDateToValidate) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.Kypera_Tenancy_Start_Before_End_Message)); // 'The tenancy start date must be before the end date.'
                System.Debug('*** start date before end date:  tenancy_start_date_alt = ' + tenancy_start_date_alt + '; unitReference = ' + unitReference + ' con.Kypera_Reference__c = ' + con.Kypera_Reference__c);
                return null;
            }
        }
           
        try {            
            // construct the XML from the data the user entered
            constructCreateTenancyXML();
            // call the webservice 
            response = callWebServiceEndPoint(request, 'StartTenancyEndPoint');    
            
            // start logging
            instantiateLog(); 

            // update the log
            updateLog('Request__c', request);
            updateLog('Response__c', response);
            updateLog('User_Message__c', userMessage);
            
            // process results and set the userMessage
            processResults();
            updateLog('Success__c', displaySuccessIcon);
            
            if (userMessage == '' || userMessage == null) {
                userMessage = 'An error has occurred.';
            }
            
            if (userMessage == Label.Kypera_Has_Created_Tenancy) {
                tenancy = new Kypera_Tenancy__c(
                    Client__c = con.Id,
                    Kypera_Start_Date__c = tenancy_start_date_alt, 
                    Kypera_End_Date__c = tenancy_end_date_alt,
                    Unit_Name__c = unitName,
                    Unit_Reference__c = unitReference,
                    Request__c = request,
                    Response__c = response,
                    Timeline_Event__c = timelineEventId);
                insert tenancy;
                if (tenancy.Id != null) {
                    pageRef = new PageReference('/'+tenancy.Id);
                    return pageRef;
                }
            }
        } catch (Exception e) {
            userMessage += e.getMessage() + ' An unhandled exception has occurred in KyperaCreateTenancyStagedController.createTenancy. ';
            userMessage += 'Please raise a case with In-Form Support with this error message, the date and time the error occurred ';
            userMessage += 'and the technical information below.';
            system.debug('*** exception in KyperaCreateTenancyStagedController.createTenancy() = ' + e);
            if (log == null) {
                instantiateLog();
            }
            updateLog('Error_Message__c', e.getMessage());
            updateLog('User_Message__c', userMessage);

        }
        return null;        
    }

    public void constructCreateTenancyXML() {
        system.debug('*** entering constructCreateTenancyXML() ');
        request = '';
        // build the request to Kypera
        system.debug('*** about to set metadata in constructCreateTenancyXML() with ' + metadata); 
        request += xmlStart(metadata);
        system.debug('*** set metadata in constructCreateTenancyXML() ');
        request += metadata.get('StartTenancyBegin').MetaData__c;
        request += '<KyperaReference>'+con.Kypera_Reference__c+'</KyperaReference>';
        system.debug('*** about to set start date in constructCreateTenancyXML() ');
        request += '<StartDate>'+tenancy_start_date_alt+'</StartDate>';
        system.debug('*** strEndDate pre = ' + tenancy_end_date_alt);
        if (tenancy_end_date_alt != null && tenancy_end_date_alt != '') {
            system.debug('*** tenancy_end_date_alt inside = ' + tenancy_end_date_alt);
           request += '<EndDate>'+tenancy_end_date_alt+'</EndDate>';
          
        }
        request += '<UnitReference>'+unitReference+'</UnitReference>';
        request += '<ExternalApplicationReference>'+con.Id+'</ExternalApplicationReference>';
        request += metadata.get('StartTenancyEnd').MetaData__c;
        request += xmlEnd(metadata);
        
        system.debug('*** exiting constructCreateTenancyXML() ');
    }    

    // process results and sets the userMessage
    public void processResults() {
   
        if (response.contains('<faultstring>')) {
            userMessage = 'An error has occurred. The error is: ' + getXMLText(response, 'faultstring', 0);
            return;
        } else if (response.contains('<Successful><![CDATA[False]]></Successful>')) {
            userMessage = Label.Kypera_Cannot_Create_Tenancy;
            return;
        } else if (response.contains('<Successful><![CDATA[True]]></Successful>')) {
            userMessage = Label.Kypera_Has_Created_Tenancy;
            messageColour = 'green';
            displaySuccessIcon = true;
            return;
        } else {   
            userMessage = userMessage + ' An unhandled exception has occurred in KyperaCreateTenancyStagedController.processResults. Please raise a case with In-Form Support with this error message.';
        }
    }

    // calls the Kypera webservice's endpoint and returns the response as a string. The serviceEndPointName must be a valid Kypera metadata custom setting
    public string callWebServiceEndPoint(String xmlToSend, String serviceEndPointName) {
        String xmlRequest = xmlToSend;
        HttpRequest req = new HttpRequest();
        HTTPResponse res = new HttpResponse();
        req.setMethod('POST');
        req.setEndpoint(metadata.get('ServiceHandler').MetaData__c); 
        system.debug('*** metadata.get(\'ServiceHandler\').MetaData__c = ' + metadata.get('ServiceHandler').MetaData__c);
        req.setHeader('Content-Type', 'text/xml; charset=utf-8');
        req.setHeader('SOAPAction', metadata.get(serviceEndPointName).MetaData__c); 
        system.debug('*** metadata.get(serviceEndPointName).MetaData__c = ' + metadata.get(serviceEndPointName).MetaData__c);
        system.debug('*** serviceEndPointName = ' + serviceEndPointName);
        req.setBody(xmlRequest);
        system.debug('*** req.getBody: '+ req.getBody());
        Http http = new Http();
        try { 
            if (!Test.isRunningTest()){ 
                res = http.send(req); 
                // the next 2 if statements are for debugging & testing
                /*
                if (serviceEndPointName == 'GetVacantPropertiesEndPoint') {
                    responseStub =  '';
                    responseStub += '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ';
                    responseStub += 'xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><GetVacantPropertiesResponse xmlns="https://kypera.changing-lives.org.uk/InformWebService/">';
                    responseStub += '<GetVacantPropertiesResult><Kypera xmlns=""><PropertyStructure><Districts><District><Scheme><Block><Unit><UnitReference>';
                    responseStub += '<![CDATA[TEST]]></UnitReference><UnitName><![CDATA[Test Unit]]></UnitName></Unit></Block></Scheme></District><District>';
                    responseStub += '<DistrictReference><![CDATA[N01]]></DistrictReference><DistrictName><![CDATA[Newcastle]]></DistrictName>';
                    responseStub += '<Scheme><SchemeReference><![CDATA[017]]></SchemeReference><SchemeName><![CDATA[36 Bentinck Terrace]]></SchemeName>';
                    responseStub += '<Block><BlockReference><![CDATA[BT1]]></BlockReference><BlockName><![CDATA[36 Bentinck Terrace]]></BlockName>';
                    responseStub += '<Unit><UnitReference><![CDATA[BT002]]></UnitReference><UnitName><![CDATA[Bentinck2]]></UnitName></Unit>';
                    responseStub += '<Unit><UnitReference><![CDATA[BT003]]></UnitReference><UnitName><![CDATA[Bentinck3]]></UnitName></Unit>';
                    responseStub += '</Block></Scheme><Scheme><SchemeReference><![CDATA[022]]></SchemeReference><SchemeName><![CDATA[Abbott House]]></SchemeName>';
                    responseStub += '<Block><BlockReference><![CDATA[AH1]]></BlockReference><BlockName><![CDATA[Abbott House]]></BlockName>';
                    responseStub += '<Unit><UnitReference><![CDATA[AB004]]></UnitReference><UnitName><![CDATA[Abbott4]]></UnitName></Unit></Block>';
                    responseStub += '</Scheme></District></Districts></PropertyStructure></Kypera></GetVacantPropertiesResult></GetVacantPropertiesResponse>';
                    responseStub += '</soap:Body></soap:Envelope>';
                    return responseStub;
                }
                if (serviceEndPointName == 'StartTenancyEndPoint') {                
                    responseStub = '';
                    responseStub+= '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"';
                    responseStub+= ' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"';
                    responseStub+= ' xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body>';
                    responseStub+= ' <StartTenancyResponse xmlns="http://mobile.kypera.com/InformWebService/"><StartTenancyResult>';
                    responseStub+= ' <Kypera xmlns=""><Successful><![CDATA[True]]></Successful></Kypera></StartTenancyResult></StartTenancyResponse></soap:Body></soap:Envelope>';
                    return responseStub;
                }
                */
                System.debug('*** res.body: '+ res.getBody());
                System.debug('*** res.getStatus: '+ res.getStatus());
                System.debug('*** res.getStatusCode:'+ res.getStatusCode());   
            } else {
                if (serviceEndPointName == 'GetVacantPropertiesEndPoint') {
                    return response;
                }
                if (serviceEndPointName == 'StartTenancyEndPoint') {                
                    return response;
                }
            }
            return res.getBody();
        } catch(System.CalloutException e) {
            response = e.GetMessage();
            userMessage += ' ' + e.GetMessage();
            return '';
        }     
    }  

    // construct the XML request for available properties
    public void constructCheckAvailablePropertiesXML() {
        system.debug('*** entering constructCheckAvailablePropertiesXML() ');
        request = '';
        // build the request to Kypera
        system.debug('*** about to set metadata in constructCheckAvailablePropertiesXML() with ' + metadata); 
        request += xmlStart(metadata);
        system.debug('*** set metadata in constructCheckAvailablePropertiesXML() ');
        request += metadata.get('GetVacantPropertiesBegin').MetaData__c;
        if (!String.IsBlank(tenancy_start_date_alt)) {
          //  request += '<EndDate>' + tenancy_start_date_alt + '</EndDate>';
          request += '<EndDate>' + '1900-01-01'+ '</EndDate>';
        }
        if (!String.IsBlank(schemeReference)) {
            request += '<Scheme>' + schemeReference + '</Scheme>';
        }      
        request += metadata.get('GetVacantPropertiesEnd').MetaData__c;
        request += xmlEnd(metadata);
        system.debug('*** exiting constructCheckAvailablePropertiesXML() ');
    }   

   // create the start of the outerXML 
    public string xmlStart(Map<String, Kypera_Metadata__c> metaDataList) {
        string retVal = '';
        retVal += metaDataList.get('xmlTag').MetaData__c;
        retVal += metaDataList.get('soapEnvelopeBegin').MetaData__c;
        retVal += metaDataList.get('soapBodyBegin').MetaData__c;        
        return retVal;
    }

    // create the end of the outerXML 
    public string xmlEnd(Map<String, Kypera_Metadata__c> metaDataList) {
        string retVal = '';
        retVal += metaDataList.get('soapBodyEnd').MetaData__c;        
        retVal += metaDataList.get('soapEnvelopeEnd').MetaData__c;
        return retVal;
    }

    // gets the text from a XML tag
    public static string getXMLText(string xmlString, string tagName, integer position) {
        string retVal = '';
        if(xmlString.contains('<' + tagName + '>') && xmlString.contains('</' + tagName + '>')){
          try{
              retVal = xmlString.substring(xmlString.indexOf('<' + tagName + '>', position) + tagName.length() + 2,
                xmlString.indexOf('</' + tagName + '>', position));   
            }catch (exception e){
                system.debug('*** Exception in method getXMLText ' + e.getMessage() + ' tagName = ' + tagName + ' xmlString = ' + xmlString);
            } 
        }
        return retVal;
    }
    
    // log methods
    
    // method to instatiate the log
    public void instantiateLog() {
        log = new Tenancy_Information_Sent_to_Kypera__c();
        log.Client__c = con.Id;
        insert log;
    }
    
    // method to update the log - string fields
    public void updateLog(String field, string value) {
        log.put(field, value);
        update log;
    }
    
    // method to update the log - boolean fields
    public void updateLog(String field, boolean value) {
        log.put(field, value);
        update log;
    }
    
    // method to update the log - date time fields
    public void updateLog(String field, Datetime value) {
        log.put(field, value);
        update log;
    }     
    

    // Wrapper class for displaying Timeline Events
    public class te {
        Public Id recordId { get; set; }
        Public String teName { get; set; }
        Public String timelineEventStartDate { get; set; }
        Public String LeadProjectName { get; set; }
        Public String teStatus { get; set; }
        Public String recordTypeName { get; set; }
        Public String schemeCode { get; set; }
        
        public te(Id recId, String tName, String tEventStartDate, String projectName, String status, String rtName, String KyperaSchemeCode) {
            recordId = recId;
            teName = tName;
            timelineEventStartDate = tEventStartDate;
            LeadProjectName = projectName;  
            teStatus = status;
            recordTypeName = rtName;  
            schemeCode = KyperaSchemeCode;
        }
    }   
    
}// end class