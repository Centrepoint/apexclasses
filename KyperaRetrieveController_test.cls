@IsTest
global class KyperaRetrieveController_test {

    global static testMethod void testKyperaRetrieveController() {
        
        Test.StartTest();
        
        // set up the test response variable
        String responseStub = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body>';
        responseStub += '<GetCurrentTransactionsResponse xmlns="http://mobile.kypera.com/InformWebService/"><GetCurrentTransactionsResult><Kypera xmlns=""><Transactions><Transaction><UnitReference><![CDATA[0011]]></UnitReference><Address><![CDATA[Flat 3 11 Conway Place Dudley West Midlands WS23 8TD ]]>';
        responseStub += '</Address><OccupancyStart><![CDATA[01/06/2013]]></OccupancyStart><OccupancyEnd><![CDATA[00:00:00]]></OccupancyEnd><Year><![CDATA[2014]]></Year><Period><![CDATA[2]]></Period><ID><![CDATA[906992]]></ID><TransactionType><![CDATA[Debit Run]]></TransactionType>';
        responseStub += '<DateEntered><![CDATA[21/12/2014 00:06:46]]></DateEntered><Reference1><![CDATA[Debit 15/07/2013 to 29/07/2013]]></Reference1><Source><![CDATA[-1]]></Source><BFwd><![CDATA[502.8600]]></BFwd><Debit><![CDATA[]]></Debit><Credit><![CDATA[160.0000]]></Credit><CFwd>';
        responseStub += '<![CDATA[662.8600]]></CFwd><Outstanding><![CDATA[160.0000]]></Outstanding><Allocated><![CDATA[0.0000]]></Allocated></Transaction><Transaction><UnitReference><![CDATA[0011]]></UnitReference><Address><![CDATA[Flat 3 11 Conway Place Dudley West Midlands WS23 8TD ]]>';
        responseStub += '</Address><OccupancyStart><![CDATA[01/06/2013]]></OccupancyStart><OccupancyEnd><![CDATA[00:00:00]]></OccupancyEnd><Year><![CDATA[2014]]></Year><Period><![CDATA[1]]></Period><ID><![CDATA[906361]]></ID><TransactionType><![CDATA[Debit Run]]></TransactionType>';
        responseStub += '<DateEntered><![CDATA[21/12/2014 00:06:46]]></DateEntered><Reference1><![CDATA[Debit 01/07/2013 to 15/07/2013]]></Reference1><Source><![CDATA[-1]]></Source>';
        responseStub += '<BFwd><![CDATA[342.8600]]></BFwd><Debit><![CDATA[]]></Debit><Credit><![CDATA[160.0000]]></Credit><CFwd><![CDATA[502.8600]]></CFwd><Outstanding><![CDATA[160.0000]]></Outstanding><Allocated><![CDATA[0.0000]]></Allocated></Transaction>';
        responseStub += '<Transaction><UnitReference><![CDATA[0011]]></UnitReference><Address><![CDATA[Flat 3 11 Conway Place Dudley West Midlands WS23 8TD ]]></Address><OccupancyStart><![CDATA[01/06/2013]]></OccupancyStart><OccupancyEnd><![CDATA[00:00:00]]>';
        responseStub += '</OccupancyEnd><Year><![CDATA[2013]]></Year><Period><![CDATA[26]]></Period><ID><![CDATA[905730]]></ID><TransactionType><![CDATA[Debit Run]]></TransactionType><DateEntered><![CDATA[21/12/2014 00:06:46]]></DateEntered><Reference1><![CDATA[Debit 17/06/2013 to 01/07/2013]]>';
        responseStub += '</Reference1><Source><![CDATA[-1]]>';
        responseStub += '</Source><BFwd><![CDATA[182.8600]]></BFwd><Debit><![CDATA[]]></Debit><Credit><![CDATA[160.0000]]></Credit><CFwd><![CDATA[342.8600]]></CFwd><Outstanding><![CDATA[160.0000]]></Outstanding><Allocated><![CDATA[0.0000]]></Allocated>'; 
        responseStub += '</Transaction><Transaction><UnitReference><![CDATA[0011]]></UnitReference><Address><![CDATA[Flat 3 11 Conway Place Dudley West Midlands WS23 8TD ]]></Address><OccupancyStart>';
        responseStub += '<![CDATA[01/06/2013]]></OccupancyStart><OccupancyEnd><![CDATA[00:00:00]]></OccupancyEnd><Year><![CDATA[2013]]></Year><Period><![CDATA[26]]></Period><ID><![CDATA[905657]]></ID><TransactionType><![CDATA[Start of Tenancy Adjustment]]></TransactionType><DateEntered><![CDATA[21/12/2014 00:06:46]]>';
        responseStub += '</DateEntered><Reference1><![CDATA[Adjustment 01/06/2013 - 16/06/2013]]></Reference1><Source><![CDATA[-1]]></Source><BFwd><![CDATA[0.0000]]></BFwd><Debit><![CDATA[]]></Debit><Credit><![CDATA[160.0000]]></Credit><CFwd><![CDATA[182.8600]]></CFwd><Outstanding><![CDATA[182.8600]]>';
        responseStub += '</Outstanding><Allocated><![CDATA[0.0000]]></Allocated></Transaction></Transactions></Kypera></GetCurrentTransactionsResult></GetCurrentTransactionsResponse></soap:Body></soap:Envelope>';
    
        // Get record type Ids
        List<RecordType> rTypeList = [SELECT Id, Name, SobjectType FROM RecordType WHERE SobjectType IN ('Account', 'Contact')];
        Map<String, Id> accTypeMap = new Map<String, Id>();
        Map<String, Id> conTypeMap = new Map<String, Id>();
        for (RecordType rt : rTypeList) {
            if (rt.SobjectType == 'Contact') {
                conTypeMap.put(rt.Name, rt.Id);
            }
            if (rt.SobjectType == 'Account') {
                accTypeMap.put(rt.Name, rt.Id);
            }
        }

     
        // Create Kypera Metadata in the Custom Setting
        List<Kypera_Metadata__c> kms = new List<Kypera_Metadata__c>();
        
        kms.add(new Kypera_Metadata__c(Name = 'GetCurrentTransactionsBegin', Metadata__c = '<GetCurrentTransactions xmlns="http://mobile.kypera.com/InformWebService/">'));
        kms.add(new Kypera_Metadata__c(Name = 'GetCurrentTransactionsEnd', Metadata__c = '</GetCurrentTransactions>'));
        kms.add(new Kypera_Metadata__c(Name = 'GetCurrentTransactionsEndPoint', Metadata__c = 'http://mobile.kypera.com/InformWebService/GetCurrentTransactions'));
        
        kms.add(new Kypera_Metadata__c(Name = 'createPersonBegin', Metadata__c = '<CreatePerson xmlns="http://mobile.kypera.com/InformWebService/">'));
        kms.add(new Kypera_Metadata__c(Name = 'createPersonEnd', Metadata__c = '</CreatePerson>'));
        kms.add(new Kypera_Metadata__c(Name = 'soapBodyBegin', Metadata__c = '<soap:Body>'));
        kms.add(new Kypera_Metadata__c(Name = 'soapBodyEnd', Metadata__c = '</soap:Body>'));
        kms.add(new Kypera_Metadata__c(Name = 'soapEnvelopeBegin', Metadata__c = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'));
        kms.add(new Kypera_Metadata__c(Name = 'soapEnvelopeEnd', Metadata__c = '</soap:Envelope>'));
        kms.add(new Kypera_Metadata__c(Name = 'xmlTag', Metadata__c = '<?xml version="1.0" encoding="utf-8"?>'));
        kms.add(new Kypera_Metadata__c(Name = 'ServiceHandler',  Metadata__c = 'http://mobile.kypera.com/InformWebService/ServiceHandler.asmx'));
        kms.add(new Kypera_Metadata__c(Name = 'CreatePersonEndPoint',  Metadata__c = 'http://mobile.kypera.com/InformWebService/CreatePerson'));
        kms.add(new Kypera_Metadata__c(Name = 'UpdatePersonEndPoint',  Metadata__c = 'http://mobile.kypera.com/InformWebService/UpdatePerson'));
        kms.add(new Kypera_Metadata__c(Name = 'updatePersonBegin',  Metadata__c = '<UpdatePerson xmlns="http://mobile.kypera.com/InformWebService/">'));
        kms.add(new Kypera_Metadata__c(Name = 'updatePersonEnd',  Metadata__c = '</UpdatePerson>'));
        insert kms;

        
        // put these in a map for easy retrieval later on
        Map<String, String> kyperaMetaData = new Map<String, String>();
        for (Kypera_Metadata__c kmsMapItem: kms) {
            kyperaMetaData.put(kmsMapItem.Name, kmsMapItem.Metadata__c);
        }
        
    
        // Create an Account
        Account acc = new Account(Name = 'Test Account 1', RecordTypeId = accTypeMap.get('Project'));
        insert acc;        

        // Create a Client with the correct Record Type
        Contact con = new Contact(FirstName = 'Test', LastName = 'Contact 1', infrm__Gender__c = 'Male', Salutation = 'Mr', RecordTypeId = conTypeMap.get('Client'), infrm__Disabled_person__c = 'Yes', BirthDate = Date.newInstance(1980, 10, 10));
        con.put('infrm__NI_number_known__c', 'No');
        con.put('infrm__Client_Group_P__c', 'Rough sleeper');
        con.put('infrm__Sexuality__c', 'Does not wish to disclose');
        con.put('infrm__ex_Armed_forces_personnel_SP__c', 'No');
        con.put('infrm__Source_of_income__c', 'Don\'t know');
        con.put('infrm__Qualify_for_HB__c', 'don\'t know');
        con.put('infrm__Immigration_Status__c', 'Not known');
        insert con;
        
        
       // set up the page and its controller
        PageReference pageRef = Page.KyperaTransactions;
        Test.setCurrentPage(pageRef);        
        ApexPages.currentPage().getParameters().put('id', con.id);      
        System.debug('con.id: ' + con.id);
        ApexPages.StandardController stdController = new ApexPages.StandardController(con);
        stdController.getRecord();
        KyperaRetrieveController.responseStub = responseStub;
        KyperaRetrieveController controller = new KyperaRetrieveController(stdController);
//        controller.responseStub = responseStub;
        controller.retrieveTransactionData();   
        //controller.retrievePropertyData();
        system.assertEquals(responseStub, KyperaRetrieveController.responseStub);      

        Test.StopTest();
    
    
    }
}