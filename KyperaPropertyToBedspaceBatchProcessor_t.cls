@IsTest
global class KyperaPropertyToBedspaceBatchProcessor_t {

    @testSetup static void setup() {
        // create test Kypera Properties
        List<Kypera_Properties__c> KyperaProperties = new List<Kypera_Properties__c>();
        for(Integer i = 0; i < 1000; i++) {
            KyperaProperties.add(new Kypera_Properties__c
                                    (
                                    Scheme_Name__c = 'Property ' + i, 
                                    Scheme_Reference__c = String.valueOf(i),
                                    Unit_Name__c = 'Unit ' + i,
                                    Unit_Reference__c = String.valueOf(i),
                                    H_Core_Rent__c = 100
                                    )
                                );
        }
        
        insert KyperaProperties;
        KyperaProperties = [SELECT Id, Scheme_Name__c, Scheme_Reference__c FROM Kypera_Properties__c WHERE Unit_Name__c LIKE 'Unit%' LIMIT 10000];
        System.AssertEquals(1000, KyperaProperties.size());
        
        // create test Services
        List<Account> accountServices = new List<Account>();
        for(Integer i = 0; i < 1500; i++) {
            if (i < 1001) {
                accountServices.add(new Account(Name = 'Service ' + i, Kypera_Scheme_Code__c = String.ValueOf(i)));  
            } else {
                accountServices.add(new Account(Name = 'Service ' + i));            
            }
        }
        insert accountServices;
        
        // create test Bedspaces
        List<infrm__Bedspace__c> bedspaces = new List<infrm__Bedspace__c>();
        for(Integer i = 0; i < 1500; i++) {
            bedspaces.add(new infrm__Bedspace__c
                             (
                                Name = 'Bedspace ' + i, 
                                Kypera_Unit_ID__c = String.ValueOf(i),
                                infrm__Project__c = accountServices.get(i).Id,
                                infrm__Basic_Rent__c = 100,
                                CORE_Shared_or_Self_Contained__c = 'Shared',
                                CORE_Bedspace_In_Property__c = 'Yes'                                
                             )
                          );
        }
        insert bedspaces;
        
        // Create a Property Log within 24 hours
        Kypera_Property_Log__c  kpl = new Kypera_Property_Log__c(Import_Stage__c ='5 - Success', Batch_End_Date__c = DateTime.now());
        insert kpl;
        
    }

    static testmethod void m1() {
        Test.StartTest();

        List<Account> accountServices = new List<Account>();
        List<Kypera_Properties__c> KyperaProperties = new List<Kypera_Properties__c>();
        List<infrm__Bedspace__c> bedspaces = new List<infrm__Bedspace__c>();
        Kypera_Property_Log__c kpl = [SELECT Id, Import_Stage__c, Batch_End_Date__c FROM Kypera_Property_Log__c LIMIT 1];
        
        // Create Kypera Metadata in the Custom Setting
        List<Kypera_Metadata__c> kms = new List<Kypera_Metadata__c>();
        
        kms.add(new Kypera_Metadata__c(Name = 'GetCurrentTransactionsBegin', Metadata__c = '<GetCurrentTransactions xmlns="http://mobile.kypera.com/InformWebService/">'));
        kms.add(new Kypera_Metadata__c(Name = 'GetCurrentTransactionsEnd', Metadata__c = '</GetCurrentTransactions>'));
        kms.add(new Kypera_Metadata__c(Name = 'GetCurrentTransactionsEndPoint', Metadata__c = 'http://mobile.kypera.com/InformWebService/GetCurrentTransactions'));
        
        kms.add(new Kypera_Metadata__c(Name = 'createPersonBegin', Metadata__c = '<CreatePerson xmlns="http://mobile.kypera.com/InformWebService/">'));
        kms.add(new Kypera_Metadata__c(Name = 'createPersonEnd', Metadata__c = '</CreatePerson>'));
        kms.add(new Kypera_Metadata__c(Name = 'soapBodyBegin', Metadata__c = '<soap:Body>'));
        kms.add(new Kypera_Metadata__c(Name = 'soapBodyEnd', Metadata__c = '</soap:Body>'));
        kms.add(new Kypera_Metadata__c(Name = 'soapEnvelopeBegin', Metadata__c = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'));
        kms.add(new Kypera_Metadata__c(Name = 'soapEnvelopeEnd', Metadata__c = '</soap:Envelope>'));
        kms.add(new Kypera_Metadata__c(Name = 'xmlTag', Metadata__c = '<?xml version="1.0" encoding="utf-8"?>'));
        kms.add(new Kypera_Metadata__c(Name = 'ServiceHandler',  Metadata__c = 'http://mobile.kypera.com/InformWebService/ServiceHandler.asmx'));
        kms.add(new Kypera_Metadata__c(Name = 'CreatePersonEndPoint',  Metadata__c = 'http://mobile.kypera.com/InformWebService/CreatePerson'));
        kms.add(new Kypera_Metadata__c(Name = 'UpdatePersonEndPoint',  Metadata__c = 'http://mobile.kypera.com/InformWebService/UpdatePerson'));
        kms.add(new Kypera_Metadata__c(Name = 'updatePersonBegin',  Metadata__c = '<UpdatePerson xmlns="http://mobile.kypera.com/InformWebService/">'));
        kms.add(new Kypera_Metadata__c(Name = 'updatePersonEnd',  Metadata__c = '</UpdatePerson>'));
        kms.add(new Kypera_Metadata__c(Name = 'GetPropertyStructureXML', Metadata__c = 'http://mobile.kypera.com/InformWebService/GetPropertyStructureXml'));
        insert kms;        
        KyperaPropertyToBedspaceBatchProcessor sync = new KyperaPropertyToBedspaceBatchProcessor();
        ID batchprocessid = Database.executeBatch(sync);
                
        Test.StopTest();
        
    }
    
}